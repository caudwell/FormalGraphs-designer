#!/usr/bin/env python3
# _*_ coding: utf-8 _*_

version = '1.6'

"""
Dessine_ Graphe.py écrit en Python 3.6
E.Vieil 10/01/2018 
Evolutions par rapport à la version 1.5 :
Choix des unités SI ou SI-J (SI avec explicitation du joule, radian et corpuscule)
Fenêtre "A propos"
Commentaire de version dans les enregistrements
Nouvelle taille bitmap 450x300 pixels
Ajustement de la taille des graphes en SVG selon la hauteur bitmap
Bouton d'ajustement de la taille des symboles algébriques dans les noeuds
Visionneuse de code SVG, XML et LaTeX. Le code peut être modifié avant exportation.

Evolutions par rapport à la version 1.4 :
Lecture et mémorisation complète de fichiers XML (y compris statique/dynamique).
Enregistrement/Lecture de description en XML dans les fichiers bitmap.
Représentation des singletons inductif et capacitif avec leurs paires conjuguées.
Inclusion des singletons dans les sorties XML et SVG. 
Centrage des labels (symboles et indices) dans les noeuds et les légendes.
Visionneuse SVG et Bitmap. Carte des variétés d'énergie. 
Fenêtre de description des variétés d'énergie.
"""
import sys,  os,  csv, struct,  binascii

from PyQt5. QtCore import Qt,  pyqtSlot, QPointF,  QRectF,  QEvent,  QByteArray, QBuffer, QIODevice
from PyQt5. QtGui import QBrush, QPen, QColor,  QPainter, QRadialGradient, QPolygonF,\
QPainterPath,  QFont,  QIcon, QPixmap
from PyQt5. QtWidgets import QMainWindow,  QApplication,  QGraphicsScene,  \
QGraphicsRectItem,  QGraphicsTextItem, QGraphicsLineItem, \
QGraphicsItem, QGraphicsPolygonItem,  QGraphicsPathItem,  QFileDialog, QMessageBox, \
QGraphicsItemGroup,  QTextEdit, QWidget, QHBoxLayout,  QFontDialog,  QGraphicsView,  QLabel
from Ui_Dessinateur_GF import Ui_MainWindow # Dessine_Graphe_Ressources_rc importé au passage
from GF_Data_SVG3 import Definitions_SVG, Dico_Elements_SVG
import xml.etree.ElementTree as ET
from PyQt5.QtSvg import QSvgWidget


compteur_appel = 0 # Pour éviter rebond dans les appels par menu
can_exit = True
Langue = 'F' # défaut
dynamique = 1 # défaut
operateurs = True # défaut
linearite = False # défaut
legendes = True # défaut
unites = 3 #  SI + SI_J par défaut
coul_elem = {}
coul_elem['qte_base'] =  '#00FF00'
coul_elem['unite_q'] = "#00C000"
coul_elem['effort'] =  "#FF0000"
coul_elem['unite_e'] =  "#D00000"
coul_elem['impulsion'] =  "#00FFFF"
coul_elem['unite_p'] =  "#00C0C0"
coul_elem['flot'] =  "#FF00FF"
coul_elem['unite_f'] =  "#800080"
coul_elem['compteur'] = "#FFFFFF"
coul_elem['capacitance'] = "#339933"  #008000
coul_elem['conductance'] = "#3333FF" 
coul_elem['inductance'] = "#805030"
coul_elem['temps'] = "#F06010"
coul_elem['barriere'] = "#FF0000"
coul_elem['synapse'] = "#C0C0C0"

# Dimensions du cadre élémentaire du graphe
arete = 100.  # cadre schéma carré
d = arete / 3. #32. # Longueur étalon des noeuds
facteur = 0.275  # Echelle entre scene et vue en pixel
delta_x = 256. # fond 
delta_y = 190.
x0 = (delta_x - arete)/2.
y0 = (delta_y - arete)/2.
Dimension_bitmap = "800x600" # Défaut
Plein_ecran = False
Signification_on = False # Fenêtre description des variétés
police_legendes = 'Myanmar Text' # Police monospace
taille_legendes = 5.0 * 0.01 *arete
style_legendes =''
police_unites = 'Helvetica'
taille_unites = 5.0 * 0.01 *arete
taille_symbole = 13.



n = 0 # Numero de groupe représentant un objet formel (pour le cas de plusieurs objets formels, NON IMPLEMENTE)
graphe_existe = False
Variete = [n]
Capa_Elastance = [n]
Indu_Reluctance = [n]
Condu_Resistance = [n]
Evol_Involution_C = [n]
Evol_Involution_L = [n]
Multiplication_C = [n]
Multiplication_L = [n]
Capa_Elast_Sing = [n]
Indu_Reluct_Sing = [n]

Enum_proprietes = '' # Pour attribuer un nom de fichier représentatif
ordre_cles = [] # liste pour indexer les cledu dictionnaire "Etiquette"
ObjetFormel = [n] #  par défaut
NomObjetFormel = 'singleton_c'
Repertoire = ''
Code_SVG = ''

Liste_elements = []
Suffix_prop = {}
Suffix_prop['Capacitance'] = 'C'
Suffix_prop['Elastance'] = 'C-1'
Suffix_prop['Conductance'] = 'G'
Suffix_prop['Resistance'] = 'R'
Suffix_prop['Inductance'] = 'L'
Suffix_prop['Reluctance'] = 'L-1'
Suffix_prop['Evolution'] = 'T'
Suffix_prop['Involution'] = 'T-1'
Suffix_prop['Barriere'] = 'X'
Suffix_prop['Multiplication_C'] = 'N'  # Les multiplications par N sont toujours par paire
Suffix_prop['Multiplication_L'] = 'N'  # (mais construites individuellement)

var = [{}] #  var[n] : Dictionnaire (cle, liste des variables) pour la variété choisie
ind = [{}]   # ind[n] : Dictionnaire (cle, liste des indices) pour la variété choisie

def Lecture_parametres(self):
    global ordre_cles
# Lecture des paramètres de graphe
    nomFichier = 'GF_Data_varietes2.csv'
    self.Etiquette = {}
    cle_var = ''
    cle_ind = ''
    with open(nomFichier,'r' , encoding="Latin-1") as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        i=0
        ordre_cles = [] # liste pour indexer les cledu dictionnaire "Etiquette"
        for row in reader: # row = liste[]
            cle = row[0]
            if len(cle) > 0 :
                i += 1
                if cle == 'indice' :  # = indices
                    cle_ind = "@" + cle_var
                    self.Etiquette[cle_ind] = row[1:]
                    #print(self.Etiquette[cle_ind])
                else: # =  variables
                    if i > 1 : ordre_cles.append(cle) # Titre de la colonne ('Nom_court') non pris en compte
                    cle_var = cle
                    self.Etiquette[cle_var] = row[1:]
                #print('cle ',i, '=', cle)
    csvfile.close()
    #print("ordre_cles =", ordre_cles)
    nomFichier = 'GF_Data_proprietes2.csv'
    self.Etiquette_op = {}
    cle_var = ''
    cle_ind = ''
    with open(nomFichier,'r' , encoding="Latin-1") as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader: # row = liste[]
            cle = row[0]
            #print(cle)
            if len(cle) > 0 :
                if cle == 'indice' :  # = indices
                    cle_ind = "@" + cle_var
                    self.Etiquette_op[cle_ind] = row[1:]
                else: # =  variables
                    cle_var = cle
                    self.Etiquette_op[cle_var] = row[1:]
    csvfile.close()
  
def construction_listes_varietes(self, Langue):  # Procédure
    # Construction des deux liste de paires (variété, clé)
    global liste_varietesF
    global liste_varietesE
    global liste_attributs_op # Liste
    liste_varietesF = [] # Liste de paires (variété, clé) principale pour une variété désignée par une cle
    liste_varietesE = [] # Liste de paires (variété, clé) secondaire
    for cle in self.Etiquette : # Balayage des variétés dans les cles du dictionnaire
        if cle != 'Nom court': # On évite les attributs (en-têtes de colonnes dans un tableau)
            liste = self.Etiquette[cle] # liste des variables de la variété désignée par la cle
            element = liste[0]
            if cle[0] != "@" : # Si cle commence par @ alors liste secondaire
                liste_varietesF.append((element,  cle)) # liste  principale (noms en français) de paires
            else:
                liste_varietesE.append((element,  cle)) # liste des indices et des noms en anglais de paires
        """print ('-----------------------------------------------')
        print ("Paires (variété, clé) en français = ",  liste_varietesF)      
        print ('-----------------------------------------------')
        print ("Paires (variété, clé) en anglais = ", liste_varietesE) """   
    # Remplissage de la liste de choix avec les noms de variétés de liste_varietesF ou liste_varietesE
    if Langue == 'F' : self.liste_varietes = liste_varietesF
    if Langue == 'E' : self.liste_varietes = liste_varietesE
    #Extraction de la liste des attributs (noms en-tête de colonnes)
    self.liste_attributs =self.Etiquette['Nom court'] #Extraction de la liste des attributs de variables
    #print(self.liste_attributs)
    liste_attributs_op = self.Etiquette_op['Nom'] #Extraction de la liste des attributs de propriétés

def construction_dicos_var(self, cle):  # Procédure
    if cle[0] != "@" :
        cle_var = cle
        cle_ind = "@" + cle
    else:
        cle_var = cle[1:]
        cle_ind = cle
    liste = self.Etiquette[cle_var]  # Liste des variables de la variété choisie
      
    for index in range(len(liste)) : # Parcours de la liste des variables élément par élément
        if len(liste[index]) > 0 :
            element = liste[index]
            var[n][self.liste_attributs[index]] = element
    #print ("Ligne Variables",var[n]['Symb_S_f']) 
    #print('__________________________________________________________________________')
    liste =self.Etiquette[cle_ind]  # Liste des indices de la variété choisie
    for index in range(len(liste)) : # Parcours de la liste des variables élément par élément
        if len(liste[index]) > 0 :
            element = liste[index]
            ind[n][self.liste_attributs[index]] = element
    #print ("Ligne Indices", ind[n]['Unite_S_q']) 
    #print('__________________________________________________________________________')
    
def construction_dicos_prop(self, cle): 
    global liste_attributs_op # Liste
    global oper # Dictionnaire
    global ind_op # Dictionnaire
    if cle[0] != "@" :
        cle_oper = cle
        cle_ind_op = "@" + cle
    else:
        cle_oper = cle[1:]
        cle_ind_op = cle
        
    liste = self.Etiquette_op[cle_oper]  # Liste des propriétés de la variété choisie
    # print(liste)
    oper ={}  # Dictionnaire (cle, liste des propriétés) pour la variété choisie
    for index in range(len(liste)) : # Parcours de la liste des propriétés élément par élément
        if len(liste[index]) > 0 :
            element = liste[index]
            oper[liste_attributs_op[index]] = element
    # print (oper) 
    
    liste = self.Etiquette_op[cle_ind_op]  # Liste des indices des propriétés de la variété choisie
    ind_op ={}  # Dictionnaire (cle, liste des indices) pour la variété choisie
    for index in range(len(liste)) : # Parcours de la liste des propriétés élément par élément
        if len(liste[index]) > 0 :
            element = liste[index]
            ind_op[liste_attributs_op[index]] = element
    # print (ind_op) 

def remplissage_labels(self):     # Procédure
    # Remplissage des labels avec les variables de la variété choisie (panneau de gauche)
    if Langue == 'F' :
        self.lbl_p.setText(var[n]['Nom_p']) 
        self.lbl_f.setText(var[n]['Nom_f']) 
        self.lbl_q.setText(var[n]['Nom_q']) 
        self.lbl_e.setText(var[n]['Nom_e'])
    if Langue == 'E' :
        self.lbl_p.setText(ind[n]['Nom_p']) 
        self.lbl_f.setText(ind[n]['Nom_f']) 
        self.lbl_q.setText(ind[n]['Nom_q']) 
        self.lbl_e.setText(ind[n]['Nom_e'])
    
    # Remplissage des labels "symboles"
    label_text(self.lbl_p_2, var[n]['Symb_p'],  var[n]['Police_p'], \
    var[n]['Taille_p'],  var[n]['Style_p'])
    label_text(self.lbl_f_2, var[n]['Symb_f'],  var[n]['Police_f'], \
    var[n]['Taille_f'],  var[n]['Style_f'])
    label_text(self.lbl_q_2, var[n]['Symb_q'],  var[n]['Police_q'], \
    var[n]['Taille_q'],  var[n]['Style_q'])
    label_text(self.lbl_e_2, var[n]['Symb_e'],  var[n]['Police_e'], \
    var[n]['Taille_e'],  var[n]['Style_e'])

    # Remplissage des labels "indices"
    label_text(self.lbl_p_1, ind[n]['Symb_p'],  ind[n]['Police_p'], \
    ind[n]['Taille_p'],  ind[n]['Style_p'])
    label_text(self.lbl_f_1, ind[n]['Symb_f'],  ind[n]['Police_f'], \
    ind[n]['Taille_f'],  ind[n]['Style_f'])
    label_text(self.lbl_q_1, ind[n]['Symb_q'],  ind[n]['Police_q'], \
    ind[n]['Taille_q'],  ind[n]['Style_q'])
    label_text(self.lbl_e_1, ind[n]['Symb_e'],  ind[n]['Police_e'], \
    ind[n]['Taille_e'],  ind[n]['Style_e'])
    
    # Remplissage des labels "unites"
    unite = var[n]['Unite_p']
    if ind[n]['Unite_p'] != '~' : unite = ind[n]['Unite_p'] +" = " +unite
    label_text(self.lbl_p_3, "/ " + format_html(unite), "Arial","14", "normal")
    unite = var[n]['Unite_f']
    if ind[n]['Unite_f'] != '~' : unite = ind[n]['Unite_f'] +" = " +unite
    label_text(self.lbl_f_3, "/ " + format_html(unite) , "Arial","14", "normal")
    unite = var[n]['Unite_q']
    if ind[n]['Unite_q'] != '~' : unite = ind[n]['Unite_q'] +" = " +unite
    label_text(self.lbl_q_3, "/ " + format_html(unite), "Arial","14", "normal")
    unite = var[n]['Unite_e']
    if ind[n]['Unite_e'] != '~' : unite = ind[n]['Unite_e'] +" = " +unite
    label_text(self.lbl_e_3, "/ " + format_html(unite), "Arial","14", "normal")

def label_text(objet_label, texte,  police,  taille,  style): # Fonction
    if texte == '~' : texte = ' '
    font =QFont(police,  0.8 * float(taille))
    font.setBold(False)
    font.setItalic(False)
    if style == 'bold' or style == 'bolditalic': font.setBold(True)
    if style == 'italic' or style == 'bolditalic': font.setItalic(True)
    if style == 'normal' :
        font.setItalic(False)
        font.setBold(False)
    objet_label .setFont(font)
    if "0x" in texte :
        texte = chr(int(texte,  16))
    objet_label.setText(texte) 

def format_html(texte) : # Fonction gérant les exposants 
    chaine = ''
    if len(texte) > 0 :
        exposant = False
        for c in texte:
            if c == '-' and len(texte) >1 : c ="0"
            if (c.isdigit() ) and not exposant  :
                exposant = True
                chaine = chaine + "<sup>"
            elif  not (c.isdigit() ) and exposant :
                exposant = False
                chaine = chaine + "</sup>"
            if c == '0' : c ="-"
            chaine = chaine + c
        if  exposant  :
            chaine = chaine + "</sup>"
        #print (chaine)
    return chaine        
    
def NoeudQteBase(d, x0,  y0, epaisseur, couleur):

    gradient = QRadialGradient(QPointF(0.,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.335 * d, -0.45 * d))
    points.append(QPointF(0.335 * d, -0.45 * d))
    points.append(QPointF( 0.6 * d,  0.4 * d))
    points.append(QPointF( -0.6 * d,  0.4 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Nombre d\'entités capacitives')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Qte_Base')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if ObjetFormel[n] == 1 : # Singleton
        objet.setPos(x0 + arete, y0)
    else :  
        objet.setPos(x0 + arete, y0 + arete)
    return objet
    
def NoeudEntiteQteBase(d, x0,  y0, epaisseur, couleur):

    gradient = QRadialGradient(QPointF(0.,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.55 * d, 0.))
    points.append(QPointF(0., -0.55 * d))
    points.append(QPointF( +0.55 * d,  0.))
    points.append(QPointF(0., 0.55 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Entité capacitive unité')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_EntiteQte_Base')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 + arete,y0 + arete)
    return objet

def NoeudEffort(d, x0,  y0, epaisseur, couleur):
    gradient = QRadialGradient(QPointF(0.,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.31 * d, -0.45 * d))
    points.append(QPointF(0.31 * d, -0.45 * d))
    points.append(QPointF( 0.5 * d,  0.145 * d))
    points.append(QPointF( 0. * d,  0.5 * d))
    points.append(QPointF( -0.5 * d,  0.145 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Energie-par-entité capacitive')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Effort')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 , y0 + arete)
    return objet
    
def NoeudEffortSingulier(d, x0,  y0, epaisseur, couleur):
    gradient = QRadialGradient(QPointF(0.1 * d,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.455 * d, 0.45 * d))
    points.append(QPointF(0.45 * d, 0.45 * d))
    points.append(QPointF( 0.70 * d,  -0.4 * d))
    points.append(QPointF( -0.19 * d,  -0.4 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Energie-par-entité singulière capacitive')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_EffortSingulier')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 - 0.025*arete , y0)
    return objet
    
def NoeudImpulsion(d, x0,  y0, epaisseur, couleur):
    gradient = QRadialGradient(QPointF(0.,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.335 * d, 0.45 * d))
    points.append(QPointF(0.34 * d, 0.45 * d))
    points.append(QPointF( 0.6 * d,  -0.4 * d))
    points.append(QPointF( -0.6 * d,  -0.4 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Nombre d\'entités inductives')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Impulsion')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if ObjetFormel[n] == 1 : # Singleton
        objet.setPos(x0 , y0 + arete)
    else :
        objet.setPos(x0 , y0)
    return objet
    
def NoeudEntiteImpulsion(d, x0,  y0, epaisseur, couleur):
    gradient = QRadialGradient(QPointF(0.1 * d,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.455 * d, 0.45 * d))
    points.append(QPointF(0.45 * d, 0.45 * d))
    points.append(QPointF( 0.70 * d,  -0.4 * d))
    points.append(QPointF( -0.19 * d,  -0.4 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Entité inductive')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_EntiteImpulsion')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 - .1* d , y0)
    return objet

def NoeudFlot(d, x0,  y0, epaisseur, couleur):    
    gradient = QRadialGradient(QPointF(0.,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.31 * d, 0.45 * d))
    points.append(QPointF(0.31 * d, 0.45 * d))
    points.append(QPointF( 0.5 * d,  -0.145 * d))
    points.append(QPointF( 0. * d,  -0.5 * d))
    points.append(QPointF( -0.5 * d,  -0.145 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Energie-par-entité inductive')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Flot')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 + arete,y0)
    return objet  
    
def NoeudFlotSingulier(d, x0,  y0, epaisseur, couleur):    
    gradient = QRadialGradient(QPointF(0.1 * d,  0.), 0.5*d)
    gradient.setColorAt(0, Qt.white)
    gradient.setColorAt(1, QColor(couleur))
    points = QPolygonF()
    points.append(QPointF(-0.455 * d, 0.45 * d))
    points.append(QPointF(0.45 * d, 0.45 * d))
    points.append(QPointF( 0.70 * d,  -0.4 * d))
    points.append(QPointF( -0.19 * d,  -0.4 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(gradient)
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setToolTip('Variable d\'état : Energie-par-entité singulière inductive')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_FlotSingulier')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 + 0.98*arete,y0+arete)
    return objet    

def CompteurQte_Base(d, x0,  y0, epaisseur, couleur):
    dc = 7. * d / 8.
    path = QPainterPath()
    path.addRoundedRect(QRectF(-0.5*dc, -0.5*dc, dc, dc), dc/4., dc/4.)
    objet = QGraphicsPathItem(path)
    objet.setBrush(QBrush(QColor(couleur)))
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)        
    objet.setToolTip('Variable sans statut d\'état : Compteur d\'entités capacitives')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Compteur_C')
    objet.setPos(x0 + arete, y0+arete)
    return objet

def CompteurImpulsion(d, x0,  y0, epaisseur, couleur):
    dc =  7. * d / 8.
    path = QPainterPath()
    path.addRoundedRect(QRectF(-0.5*dc, -0.5*dc, dc, dc), dc/4., dc/4.)
    objet = QGraphicsPathItem(path)
    objet.setBrush(QBrush(QColor(couleur)))
    objet.setPen(QPen(Qt.black,  epaisseur))
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)        
    objet.setToolTip('Variable sans statut d\'état : Compteur d\'entités inductives')
    objet.setData(0,'Noeud')
    objet.setData(1,'N_Compteur_L')
    objet.setPos(x0 , y0)
    return objet    

def Capacitance(d, x0,  y0, Element, couleur):    
    points = QPolygonF()
    points.append(QPointF(-0.38*arete, 0.04 * d))
    points.append(QPointF(0.28*arete, 0.04 * d))
    points.append(QPointF( 0.28*arete,  0.1 * d))
    points.append(QPointF( 0.34*arete,  0.))
    points.append(QPointF( 0.28*arete,  -0.1 * d))
    points.append(QPointF( 0.28*arete,  -0.04 * d))
    points.append(QPointF( -0.38*arete,  -0.04 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur )))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Capacitance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Capacitance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if Element == 'O_Capacitance' : objet.setPos(x0 + 0.5*arete,y0+arete)
    if Element == 'O_Capacitance_Singuliere' : objet.setPos(x0 + 0.5*arete,y0)
    return objet
    
def Elastance(d, x0,  y0, Element, couleur):    
    points = QPolygonF()
    points.append(QPointF(0.38*arete, 0.04 * d))
    points.append(QPointF(-0.30*arete , 0.04 * d))
    points.append(QPointF( -0.30*arete,  0.1 * d))
    points.append(QPointF( -0.36*arete,  0.))
    points.append(QPointF( -0.30*arete,  -0.1 * d))
    points.append(QPointF( -0.30*arete,  -0.04 * d))
    points.append(QPointF( 0.38*arete,  -0.04 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur)))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Elastance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Elastance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if Element == 'O_Elastance' : objet.setPos(x0 + 0.5*arete,y0+arete)
    if Element == 'O_Elastance_Singuliere' : objet.setPos(x0 + 0.5*arete,y0)
    return objet
    
def Inductance(d, x0,  y0, Element, couleur):    
    points = QPolygonF()
    points.append(QPointF(0.38*arete, 0.04 * d))
    points.append(QPointF(-0.30*arete , 0.04 * d))
    points.append(QPointF( -0.30*arete,  0.1 * d))
    points.append(QPointF( -0.36*arete,  0.))
    points.append(QPointF( -0.30*arete,  -0.1 * d))
    points.append(QPointF( -0.30*arete,  -0.04 * d))
    points.append(QPointF( 0.38*arete,  -0.04 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur)))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Inductance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Inductance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if Element == 'O_Inductance' : objet.setPos(x0 + 0.5*arete,y0)
    if Element == 'O_Inductance_Singuliere' : objet.setPos(x0 + 0.5*arete,y0+arete)
    return objet
    
def Reluctance(d, x0,  y0, Element, couleur):    
    points = QPolygonF()
    points.append(QPointF(-0.38*arete, 0.04 * d))
    points.append(QPointF(0.30*arete , 0.04 * d))
    points.append(QPointF( 0.30*arete,  0.1 * d))
    points.append(QPointF( 0.36*arete,  0.))
    points.append(QPointF( 0.30*arete,  -0.1 * d))
    points.append(QPointF( 0.30*arete,  -0.04 * d))
    points.append(QPointF( -0.38*arete,  -0.04 * d))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur )))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Réluctance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Reluctance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    if Element == 'O_Reluctance' : objet.setPos(x0 + 0.5*arete,y0)
    if Element == 'O_Reluctance_Singuliere' : objet.setPos(x0 + 0.5*arete,y0+arete)
    return objet
    
def Conductance(d, x0,  y0, couleur):    
    dl = d/34.
    x1 =-0.40*arete
    y1 = 0.40*arete
    x2 = 0.35*arete
    y2 = -0.35*arete
    points = QPolygonF()
    points.append(QPointF(x1 - dl, y1 - dl))
    points.append(QPointF(x2 - dl, y2 - dl))
    points.append(QPointF(x2 - 3*dl, y2 - 3*dl))
    points.append(QPointF(x2 + 5*dl, y2 -5*dl))
    points.append(QPointF(x2 + 3*dl, y2 + 3*dl))
    points.append(QPointF(x2 + dl, y2 + dl))
    points.append(QPointF(x1 + dl, y1 + dl))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur)))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Conductance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Conductance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 + 0.5*arete,y0+0.5*arete)
    return objet
    
def Resistance(d, x0,  y0, couleur):    
    dl = d/34.
    x1 =0.40*arete
    y1 = -0.40*arete
    x2 = -0.35*arete
    y2 = 0.35*arete
    points = QPolygonF()
    points.append(QPointF(x1 + dl, y1 + dl))
    points.append(QPointF(x2 + dl, y2 + dl))
    points.append(QPointF(x2 + 3*dl, y2 + 3*dl))
    points.append(QPointF(x2 - 5*dl, y2 + 5*dl))
    points.append(QPointF(x2 - 3*dl, y2 - 3*dl))
    points.append(QPointF(x2 - dl, y2 - dl))
    points.append(QPointF(x1 - dl, y1 - dl))
    objet = QGraphicsPolygonItem(points)
    objet.setBrush(QBrush(QColor(couleur )))
    objet.setPen(QPen(Qt.NoPen))
    objet.setToolTip('Résistance : propriété consitutive du système')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Resistance')
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setPos(x0 + 0.5*arete,y0+0.5*arete)
    return objet
    
def Fleche_Haute(d, dynamique, couleur):    
    df = d
    y1 = 0.38*arete
    y2 = -0.30*arete
    y3 = -0.30*arete
    y4 = -0.36*arete
    if dynamique :
        df =2. * d
        y2 = -0.28*arete
        y3 = -0.25*arete
    points = QPolygonF()
    points.append(QPointF( 0.04 * df, y1))
    points.append(QPointF( 0.04 * df, y2 ))
    points.append(QPointF( 0.1 * df, y3 ))
    points.append(QPointF( 0.,  y4))
    points.append(QPointF( -0.1 * df,  y3 ))
    points.append(QPointF( -0.04 * df,  y2 ))
    points.append(QPointF( -0.04 * df,  y1 ))
    fleche = QGraphicsPolygonItem(points)
    fleche.setBrush(QBrush(QColor(couleur)))
    fleche.setPen(QPen(Qt.NoPen))
    if dynamique :
        interieur = QGraphicsRectItem(0., 0., 0.025 * df, 0.68*arete,  parent = fleche)
        interieur.setBrush(QBrush(Qt.white) )
        interieur.setPen(QPen(Qt.NoPen ) )
        interieur.setPos(-.0125 * df,-0.29*arete)
    return fleche
    
def Fleche_Basse(d, dynamique, couleur):    
    df = d
    y1 = -0.38*arete
    y2 = +0.30*arete
    y3 = +0.30*arete
    y4 = +0.36*arete
    if dynamique :
        df =2. * d
        y2 = +0.28*arete
        y3 = +0.25*arete
    points = QPolygonF()
    points.append(QPointF( 0.04 * df, y1))
    points.append(QPointF( 0.04 * df, y2 ))
    points.append(QPointF( 0.1 * df, y3 ))
    points.append(QPointF( 0.,  y4))
    points.append(QPointF( -0.1 * df,  y3 ))
    points.append(QPointF( -0.04 * df,  y2 ))
    points.append(QPointF( -0.04 * df,  y1 ))
    fleche = QGraphicsPolygonItem(points)
    fleche.setBrush(QBrush(QColor(couleur) ))
    fleche.setPen(QPen(Qt.NoPen))
    if dynamique :
        interieur = QGraphicsRectItem(0., 0., 0.025 * df, 0.68*arete,  parent = fleche)
        interieur.setBrush(QBrush(Qt.white) )
        interieur.setPen(QPen(Qt.NoPen ) )
        interieur.setPos(-.0125 * df, -0.39*arete)
    fleche.setData(0,'Operateur')
    return fleche

def Croix(d,  couleur):    

    dl = d/8.
    ligne = QGraphicsLineItem()
    ligne.setLine(- dl,  - dl, dl, dl) 
    ligne.setPen(QPen(QColor(couleur)))
    group = QGraphicsItemGroup()
    group.addToGroup(ligne)
    ligne = QGraphicsLineItem() 
    ligne.setLine(- dl,  dl, dl, -dl) 
    ligne.setPen(QPen(QColor("#FF0000") ))
    group.addToGroup(ligne)
    return group

def Evolution_C(d, x0,  y0, dynamique, couleur):     
    objet = Fleche_Haute(d, dynamique, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Propriété d\'espace-temps d\'évolution')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Evolution_C')
    objet.setPos(x0 + arete,y0+0.5*arete)
    return objet
    
def Involution_C(d, x0,  y0, dynamique, couleur):    
    objet = Fleche_Basse(d, dynamique, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Propriété d\'espace-temps d\'évolution inverse')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Involution_C')
    objet.setPos(x0 + arete,y0+0.5*arete)
    return objet
    
def Evolution_L(d,  x0,  y0, dynamique, couleur ):    
    objet = Fleche_Basse(d, dynamique, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Propriété d\'espace-temps d\'évolution')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Evolution_L')
    objet.setPos(x0,y0+0.5*arete)
    return objet
    
def Involution_L(d, x0,  y0, dynamique, couleur):    
    objet = Fleche_Haute(d, dynamique, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Propriété d\'espace-temps d\'évolution inverse')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Involution_L')
    objet.setPos(x0 ,y0+0.5*arete)
    return objet

def Croix_D(d, x0,  y0, couleur):    
    objet = Croix(d, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Chemin interdit (pas de lien temporel)')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Croix_C')
    objet.setPos(x0 +arete ,y0+0.5*arete)
    return objet

def Croix_G(d, x0,  y0, couleur):    
    objet = Croix(d, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Chemin interdit (pas de lien temporel)')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Croix_G')
    objet.setPos(x0 ,y0+0.5*arete)
    return objet

def Croix_C(d, x0,  y0, couleur):    
    objet = Croix(d, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Chemin interdit (pas de lien entre les deux noeuds')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Croix_C')
    objet.setPos(x0 +0.5*arete ,y0+arete)
    return objet

def Croix_L(d, x0,  y0, couleur):    
    objet = Croix(d, couleur)
    objet.setFlag(QGraphicsItem.ItemIsMovable)
    objet.setFlag(QGraphicsItem.ItemIsSelectable)
    objet.setToolTip('Chemin interdit (pas de lien entre les deux noeuds')
    objet.setData(0,'Operateur')
    objet.setData(1,'O_Croix_L')
    objet.setPos(x0 +0.5*arete ,y0)
    return objet
 
def Sommation_Effort(d, x0,  y0, epaisseur, couleur):     

    path= QPainterPath(QPointF( -0.29*d, -.48*d) )  # startPoint 
    path.lineTo (  0.34*d, -.48*d)
    path.lineTo (  0.53*d, 0.12*d)
    polyligne =QGraphicsPathItem(path)
    plume = QPen(QColor(couleur),  2 + epaisseur)
    polyligne.setPen(plume)
    group = QGraphicsItemGroup()
    group.addToGroup(polyligne)
    texte = QGraphicsTextItem("+")
    police = QFont("Arial", 8.* 0.01 *arete)
    texte.setFont(police)
    rect = texte.boundingRect()
    dx = rect.width()
    dy = rect.height()
    texte.setPos(+0.1*dx,  -1.60*dy)
    texte.setDefaultTextColor(Qt.black)
    group.addToGroup(texte)
    group.setFlag(QGraphicsItem.ItemIsMovable)
    group.setFlag(QGraphicsItem.ItemIsSelectable)
    group.setToolTip('Addition de tous les chemins arrivant sur cette surépaisseur')
    group.setData(0,'Connecteur')
    group.setData(1,'C_Somme_E')
    group.setPos(x0, y0+arete)
    return group
    
def Sommation_Flot(d, x0,  y0, epaisseur, couleur): 

    path= QPainterPath(QPointF( 0.29*d, .48*d) )  # startPoint 
    path.lineTo (  -0.34*d, .48*d)
    path.lineTo (  -0.53*d, -0.12*d)
    polyligne=QGraphicsPathItem(path)
    plume = QPen(QColor(couleur),  2 + epaisseur)
    polyligne.setPen(plume)
    group = QGraphicsItemGroup()
    group.addToGroup(polyligne)
    texte = QGraphicsTextItem("+")
    police = QFont("Arial", 8.* 0.01 *arete)
    texte.setFont(police)
    rect = texte.boundingRect()
    dx = rect.width()
    dy = rect.height()
    texte.setPos(-1.3*dx,  0.55*dy)
    texte.setDefaultTextColor(Qt.black)
    group.addToGroup(texte)
    group.setFlag(QGraphicsItem.ItemIsMovable)
    group.setFlag(QGraphicsItem.ItemIsSelectable)
    group.setToolTip('Addition de tous les chemins arrivant sur cette surépaisseur')
    group.setData(0,'Connecteur')
    group.setData(1,'C_Somme_F')
    group.setPos(x0+arete, y0)
    return group
    

def Justifie(texte):
    lignes = texte.strip(' ').split('\n')
    if len(lignes) > 1 :
        lignes[0] =  lignes[0].strip(' ')
        lignes[1] =  lignes[1].strip(' ')
        long1 = len(lignes[0])
        long2 = len(lignes[1])
        if long1 < long2 :
            blanc = ' ' * int((long2 - long1)/1.25)
            lignes[0] = blanc + lignes[0] 
        if long1 > long2 :
            blanc = ' ' * (int((long1 - long2)/1.25))
            lignes[1] = blanc + lignes[1]
        texte = lignes[0] + '\n' + lignes[1]
    return texte
    
def TexteLegende(self, d, x0,  y0, Element, id_texte,  texte ) :
    global legendes
    global police_legendes
    global taille_legendes
    global style_legendes
    if legendes and texte != '~' :
        #print("texte =", texte)
        objet_texte = QGraphicsTextItem()
        police = QFont()
        police.setFamily(police_legendes)
        police.setPointSizeF(taille_legendes)
        police.setBold(False)
        police.setItalic(False)
        if style_legendes == 'bold' or style_legendes == 'bolditalic': police.setBold(True)
        if style_legendes == 'italic' or style_legendes == 'bolditalic': police.setItalic(True)
        objet_texte.setFont(police)
        objet_texte.setPlainText(str(texte))
        rect = objet_texte.boundingRect()
        texte2 = ''
        separation = False
        if rect.width() > arete : 
            separation = True
            morceau = texte.split(' ')
            nb = len(morceau)
            if nb == 2 :
                texte = morceau[0]
                texte2 = morceau[1]
            elif nb == 3  :
                texte = morceau[0] + ' ' + morceau[1]
                texte2 = morceau[2]
            elif nb == 4  :
                texte = morceau[0] + ' ' + morceau[1]
                texte2 = morceau[2] + ' ' + morceau[3]
            elif nb == 5  :
                texte = morceau[0] + ' ' + morceau[1] + ' ' + morceau[2]
                texte2 = morceau[3] + ' ' + morceau[4]
            elif nb == 6  :
                texte = morceau[0] + ' ' + morceau[1] + ' ' + morceau[2]
                texte2 = morceau[3] + ' ' + morceau[4] + ' ' + morceau[5]
        #print ( "texte =", texte,  "texte2 =",  texte2)
        objet_texte.setPlainText(str(texte))
        rect = objet_texte.boundingRect()
        if separation:
            objet_texte2 = QGraphicsTextItem(parent = objet_texte)
            objet_texte2.setPlainText(str(texte2))
            objet_texte2.setFont(police)
            objet_texte2.setObjectName("Legende%" + id_texte + "%2")
            objet_texte2.setFlag(QGraphicsItem.ItemIsMovable)
            objet_texte2.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_texte2.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_texte2.installEventFilter(self)
            objet_texte2.setData(0,'Legende')
            objet_texte2.setData(1,'T_Legende')
            rect2 = objet_texte2.boundingRect()
            objet_texte2.setPos(0.5*(rect.width() - rect2.width()),   0.35*rect.height())
        dx = 1.0 * rect.width()/2.
        dy_inf = 0.43*(d)
        dy_sup = 1.1*d 
        if separation : dy_sup = 1.2*d 
        if Element == 'N_Qte_Base':
            if ObjetFormel[n] == 1 :
                objet_texte.setPos(x0 + arete - dx,   y0 - dy_sup)
            else :  
                objet_texte.setPos(x0 + arete - dx , y0 + arete + dy_inf)
        if Element == 'N_EntiteQte_Base' or Element == 'N_Compteur_C':
          objet_texte.setPos(x0 + arete - dx , y0 + arete + dy_inf)
        elif Element == 'N_Effort' :
          objet_texte.setPos(x0 - dx , y0 + arete + dy_inf)
        elif Element == 'N_EffortSingulier' :
          objet_texte.setPos(x0 - dx , y0 - dy_sup)
        elif Element == 'N_Impulsion' :
            if ObjetFormel[n] == 1 :
                objet_texte.setPos(x0  - dx,   y0 +arete  + dy_inf)
            else :  
                objet_texte.setPos(x0 - dx,  y0 - dy_sup)
        if Element == 'N_EntiteImpulsion' or Element == 'N_Compteur_L':
          objet_texte.setPos(x0 - dx,  y0 - dy_sup)
        elif Element == 'N_Flot' :
            objet_texte.setPos(x0 + arete - dx,  y0 - dy_sup )
        elif Element == 'N_FlotSingulier' :
            objet_texte.setPos(x0 + arete - dx,   y0 +arete  + dy_inf)
        objet_texte.setDefaultTextColor(Qt.black)
        objet_texte.setObjectName("Legende%" + id_texte)
        objet_texte.setFlag(QGraphicsItem.ItemIsMovable)
        objet_texte.setFlag(QGraphicsItem.ItemIsSelectable)
        objet_texte.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet_texte.installEventFilter(self)
        objet_texte.setData(0,'Legende')
        objet_texte.setData(1,'T_Legende')
        return objet_texte
    
def TexteLabel(self, d, x0,  y0, centre, Element, id_label, texte, texte_indice) :
    #print (centre)
    objet_texte = QGraphicsTextItem() 
    objet = objet_texte # contiendra l'ensemble symbole et indice
    if texte != '~' :
        if Element == 'N_Qte_Base'or Element == 'N_Compteur_C':
            police1 = font_label( var[n]['Police_q'],  float(var[n]['Taille_q'])* 0.01 *arete,  var[n]['Style_q'])
            police2 = font_label( ind[n]['Police_q'],  float(ind[n]['Taille_q'])* 0.01 *arete,  ind[n]['Style_q'])
        if Element == 'N_EntiteQte_Base' :
            style = var[n]['Style_q']
            if texte =='1' :  style = ''
            police1 = font_label( var[n]['Police_S_q'],  float(var[n]['Taille_q'])* 0.01 *arete, style )
            police2 = font_label( ind[n]['Police_S_q'],  float(ind[n]['Taille_q'])* 0.01 *arete, style)
        elif Element == 'N_Effort':
            police1 = font_label( var[n]['Police_e'],  float(var[n]['Taille_e'])* 0.01 *arete,  var[n]['Style_e'])
            police2 = font_label( ind[n]['Police_e'],  float(ind[n]['Taille_e'])* 0.01 *arete,  ind[n]['Style_e'])
        elif Element == 'N_EffortSingulier':
            police1 = font_label( var[n]['Police_S_e'],  float(var[n]['Taille_e'])* 0.01 *arete,  var[n]['Style_e'])
            police2 = font_label( ind[n]['Police_S_e'],  float(ind[n]['Taille_e'])* 0.01 *arete,  ind[n]['Style_e'])
        elif Element == 'N_Impulsion' or Element == 'N_Compteur_L':
            police1 = font_label( var[n]['Police_p'],  float(var[n]['Taille_p'])* 0.01 *arete,  var[n]['Style_p'])
            police2 = font_label( ind[n]['Police_p'],  float(ind[n]['Taille_p'])* 0.01 *arete,  ind[n]['Style_p'])
        elif Element == 'N_EntiteImpulsion':
            police1 = font_label( var[n]['Police_S_p'],  float(var[n]['Taille_p'])* 0.01 *arete,  var[n]['Style_p'])
            police2 = font_label( ind[n]['Police_S_p'],  float(ind[n]['Taille_p'])* 0.01 *arete,  ind[n]['Style_p'])
        elif Element == 'N_Flot' :
            police1 = font_label( var[n]['Police_f'],  float(var[n]['Taille_f'])* 0.01 *arete,  var[n]['Style_f'])
            police2 = font_label( ind[n]['Police_f'],  float(ind[n]['Taille_f'])* 0.01 *arete,  ind[n]['Style_f'])
        elif Element == 'N_FlotSingulier' :
            police1 = font_label( var[n]['Police_S_f'],  float(var[n]['Taille_f'])* 0.01 *arete,  var[n]['Style_f'])
            police2 = font_label( ind[n]['Police_S_f'],  float(ind[n]['Taille_f'])* 0.01 *arete,  ind[n]['Style_f'])
        objet_texte.setFont(police1)
        P = texte.find("§")
        exposant = ''
        if P > 0 :
            exposant = texte[P+1:]
            texte  = texte[:P]
        if "0x" in texte : texte = chr(int(texte,  16))
        objet_texte.setHtml(texte)
        objet_texte.setDefaultTextColor(Qt.black)
        objet_texte.setObjectName("Symbole%" + id_label)
        objet_texte.setFlag(QGraphicsItem.ItemIsMovable)
        objet_texte.setFlag(QGraphicsItem.ItemIsSelectable)
        objet_texte.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet_texte.installEventFilter(self)
        rect_symb = objet_texte.boundingRect()
        if len(exposant) > 0 :
            objet_exposant = QGraphicsTextItem(parent = objet_texte) 
            objet_exposant.setFont(font_label("Arial", float(var[n]['Taille_q'])* 0.005 *arete, ''))
            objet_exposant.setPlainText(exposant)
            dx = 0.6 * rect_symb.width() # pour positionner l'exposant par rapport au symbole
            dy = +0.0 * d
            objet_exposant.setPos( dx ,  dy)
        ecart_x = 0.
        ecart_y = 0.
        indice = False
        if len(texte_indice.strip()) > 0  and texte_indice != '~' : indice = True
        if indice :
            objet_indice = QGraphicsTextItem(parent = objet_texte) 
            objet_indice.setFont(police2)
            if "0x" in texte_indice : 
                P = texte_indice.find('0x')
                texte_indice = texte_indice[:P] + chr(int(texte_indice[P:P+6],  16)) + texte_indice[P+6:]
            objet_indice.setHtml(texte_indice)
            #rect_indice = objet_indice.boundingRect()
            #print("texte_indice =", texte_indice,"Indice height =", rect_indice.height())
            dx = 0.5 * rect_symb.width() # pour positionner l'indice par rapport au symbole
            dy = 0.2 * d
            if (Element == 'N_Effort' or Element == 'N_EffortSingulier') and var[n]['Police_e']  =='Lucida Calligraphy' : 
                dx += 0.2 * rect_symb.width() # Cas de l'affinité
            if (Element == 'N_Effort' or Element == 'N_EffortSingulier')  and texte =='V': 
                dx -= 0.2 * rect_symb.width() # Cas du potentiel V avec indice
            if (Element == 'N_Flot' or Element == 'N_FlotSingulier')  and texte =='f': 
                dx -= 0.2 * rect_symb.width() # Cas du symbole f avec indice
            objet_indice.setPos( dx ,  dy)
            rect_indice = QRectF()
            rect_indice = objet_indice.boundingRect()
            largeur_label = dx + rect_indice.width()
            hauteur_label = dy + rect_indice.height()
            ecart_x = 0.20 *  rect_symb.width() # pour décaler symbole et indice
            ecart_y = -0.12 *  rect_symb.height() # pour décaler symbole et indice
            ecart_x += 0.1*rect_indice.width()
            objet_indice.setDefaultTextColor(Qt.black)
            objet_indice.setObjectName("Indice%" + id_label)
            objet_indice.setFlag(QGraphicsItem.ItemIsMovable)
            objet_indice.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_indice.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_indice.installEventFilter(self)
        objet = objet_texte # contient l'ensemble symbole et indice
        dx = -0.95 * rect_symb.width()/2. - ecart_x  # pour positionner l'ensemble symbole et indice
        if Element == 'N_Effort' and texte =='V' and texte_indice != '~' : 
            dx += 0.2 * rect_symb.width() # Cas du symbole V avec indice
        if Element == 'N_Qte_Base' and texte =='M' and texte_indice != '~' : 
            dx += 0.15 * rect_symb.width() # Cas du symbole M avec indice
        if Element == 'N_Flot' and (texte =='f' or texte =='E') : 
            dx += 0.2 * rect_symb.width() # Cas du symbole f  ou E avec indice
            ecart_y += 0.1 * rect_symb.height() # Cas du symbole f  ou E avec indice
        if not indice :
            largeur_label = rect_symb.width()
            hauteur_label = rect_symb.height()
        centre_label_x = centre[0]-0.55*largeur_label 
        centre_label_y = centre[1]-0.5*hauteur_label 
        if len(exposant) > 0 : centre_label_x += -0.05*largeur_label 
        if ObjetFormel[0] == 1 : # Singleton
            #objet_texte.setPos(x0 + arete + dx , y0 + dy_inf)
            objet_texte.setPos(centre_label_x , centre_label_y )
        else :
            objet_texte.setPos(centre_label_x , centre_label_y )
        objet.setFlag(QGraphicsItem.ItemIsMovable)
        objet.setFlag(QGraphicsItem.ItemIsSelectable)
        objet.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet.setData(0,'Label')
        objet.setData(1,'T_Label')
    return objet

def font_label( police,  taille,  style): # Fonction
    global taille_symbole    
    taille_effective = float(taille) * (float(taille_symbole)/13.)
    font =QFont(police,  0.5 * float(taille_effective))
    font.setBold(False)
    font.setItalic(False)
    if style == 'bold' or style == 'bolditalic': font.setBold(True)
    if style == 'italic' or style == 'bolditalic': font.setItalic(True)
    return font

def Choix_unite(id_unite):
    unite = var[n][id_unite] # Unité par défaut
    if ind[n][id_unite] != '~' : 
        if unites == 2 : unite = ind[n][id_unite] # unité SI
        if unites == 3 : unite = ind[n][id_unite] +" = " + unite  # unité SI + SI_J
    return unite

def TexteUnites(self, d, x0,  y0, Element, couleur): # Place les unités à côté des noeuds
    id_unite = ''
    objet_unite = QGraphicsTextItem()
    objet_unite.setPlainText('?')
    unite =''
    if unites > 0 :
        if Element == 'N_EntiteQte_Base': 
            unite = Choix_unite('Unite_S_q') 
            objet_unite = unite_texte(unite, couleur)
            objet_unite.setPos(x0 + arete + 0.20*arete , y0  + arete )
        if Element == 'N_Qte_Base' or Element == 'N_Compteur_C': 
            unite = Choix_unite('Unite_q')
            objet_unite = unite_texte(unite, couleur)
            if ObjetFormel[n] == 1 :
                objet_unite.setPos(x0 + arete + 0.20*arete , y0 )
            else :
                objet_unite.setPos(x0 + arete + 0.20*arete , y0 + arete )
        elif Element == 'N_Effort' :
            unite = Choix_unite('Unite_e')
            objet_unite = unite_texte(unite, couleur)
            rect = objet_unite.boundingRect()
            objet_unite.setPos(x0  - 0.20*arete - rect.width(),   y0 + arete )
        elif Element == 'N_EffortSingulier' :
            unite = Choix_unite('Unite_S_e')
            objet_unite = unite_texte(unite, couleur)
            rect = objet_unite.boundingRect()
            objet_unite.setPos(x0  - 0.20*arete - rect.width(),   y0)
        elif Element == 'N_Impulsion' or Element == 'N_Compteur_L':
            unite = Choix_unite('Unite_p')
            objet_unite = unite_texte(unite, couleur)
            rect = objet_unite.boundingRect()
            if ObjetFormel[n] == 1 :
                objet_unite.setPos(x0  - 0.20*arete - rect.width(),   y0  +arete )
            else :
                objet_unite.setPos(x0  - 0.20*arete - rect.width(),   y0 )
        elif Element == 'N_EntiteImpulsion':
            unite = Choix_unite('Unite_S_p')
            objet_unite = unite_texte(unite, couleur)
            rect = objet_unite.boundingRect()
            objet_unite.setPos(x0  - 0.20*arete - rect.width(),   y0 )
        elif Element == 'N_Flot' :
            unite = Choix_unite('Unite_f')
            objet_unite = unite_texte(unite, couleur)
            objet_unite.setPos(x0 + arete + 0.20*arete,  y0 )
        elif Element == 'N_FlotSingulier' :
            unite = Choix_unite('Unite_S_f')
            objet_unite = unite_texte(unite, couleur)
            objet_unite.setPos(x0 + arete + 0.20*arete,  y0 + arete)
    if len (id_unite) >= 0 : 
        objet_unite.setObjectName("Unite%" + id_unite)
        objet_unite.setFlag(QGraphicsItem.ItemIsMovable)
        objet_unite.setFlag(QGraphicsItem.ItemIsSelectable)
        objet_unite.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet_unite.installEventFilter(self)
        objet_unite.setData(0,'Label')
        objet_unite.setData(1,'T_Label')
    return objet_unite

def unite_texte(texte, Couleur):    
    objet = QGraphicsTextItem()
    police = QFont()
    police.setFamily(police_unites)
    police.setPointSizeF(taille_unites) 
    objet.setDefaultTextColor(QColor(Couleur))
    objet.setFont(police )
    objet.setHtml(format_html(texte))
    return objet
    
def TexteOper(self, d, x0,  y0, Element, id_oper,  texte, police_texte, texte_indice, police_indice, Repetition):
   global operateurs
   global linearite
   objet = QGraphicsTextItem()
   if operateurs  and texte != '~' :
        if 'O_Inductance'  in Element or 'O_Reluctance' in  Element  :
          Couleur_op = QColor(coul_elem['inductance'])
        elif 'O_Conductance' in Element or 'O_Resistance' in Element :
          Couleur_op = QColor(coul_elem['conductance'])
        elif 'O_Capacitance' in Element  or 'O_Elastance' in Element :
          Couleur_op = QColor(coul_elem['capacitance'])
        objet_texte = QGraphicsTextItem()
        police = QFont(police_texte, 8.* 0.01 *arete)
        police.setItalic(linearite)
        objet_texte.setFont(police)
        texte = texte.strip(' ')
        P = texte.find('-1')
        if P >= 0 : texte = texte[:P]
        H = texte.find("0x")
        if H>=0 : texte = texte[:H] +chr(int(texte[H+2:H+6],  16)) + texte[H+6:]
        objet_texte.setHtml(texte)
        objet_texte.setDefaultTextColor(Couleur_op)
        objet_texte.setObjectName("Symb_op%" + id_oper)
        objet_texte.setFlag(QGraphicsItem.ItemIsMovable)
        objet_texte.setFlag(QGraphicsItem.ItemIsSelectable)
        objet_texte.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet_texte.installEventFilter(self)
        rect = objet_texte.boundingRect()
        if not linearite :
            objet_chapeau = QGraphicsTextItem(parent = objet_texte) 
            police = QFont(police_texte, 8.* 0.01 *arete)
            police.setItalic(False)
            objet_chapeau.setFont(police)
            objet_chapeau.setDefaultTextColor(Couleur_op)
            objet_chapeau.setPlainText('^')
            objet_chapeau.setFlag(QGraphicsItem.ItemIsMovable)
            objet_chapeau.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_chapeau.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_chapeau.installEventFilter(self)
            dx = 0.05*d
            if texte == 'L' : dx = -0.03 *d
            dy = -0.18*d
            objet_chapeau.setPos(dx, dy)
        if P >= 0 :
          objet_exposant = QGraphicsTextItem(parent = objet_texte) 
          police = QFont('Arial', 5.* 0.01 *arete)
          police.setItalic(False)
          objet_exposant.setFont(police)
          objet_exposant.setDefaultTextColor(Couleur_op)
          objet_exposant.setPlainText('-1')
          objet_exposant.installEventFilter(self)
          dx = 0.3*d
          if texte == 'L' : dx = 0.2 *d
          dy = -0.13*d
          objet_exposant.setPos(dx, dy)
          
        ecart_x = 0.
        # ecart_y = 0.
        if texte_indice != '~' or '_Singuliere' in Element :
            objet_indice = QGraphicsTextItem(parent = objet_texte) 
            police = QFont(police_indice, 6.* 0.01 *arete)
            police.setItalic(True)
            objet_indice.setFont(police)
            objet_indice.setDefaultTextColor(Couleur_op)
            texte_indice = formatte_indice(texte_indice, Repetition)
            if '_Singuliere' in Element : 
                if texte_indice == '~' :
                    texte_indice = 's'
                else :    
                    texte_indice += 's'
            objet_indice.setHtml(texte_indice)
            dx = 0.6 * rect.width() # pour positionner l'indice par rapport au symbole
            dy = 0.2 * rect.height()
            objet_indice.setPos( dx ,  dy)
            ecart_x = 0.20 *  rect.width() # pour décaler symbole et indice
            #ecart_y = -0.15 *  rect.height() # pour décaler symbole et indice
            ecart_x += 0.05*objet_indice.boundingRect().width()
            objet_indice.setObjectName("Indice_op%" + id_oper)
            objet_indice.setFlag(QGraphicsItem.ItemIsMovable)
            objet_indice.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_indice.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_indice.installEventFilter(self)
        objet = objet_texte # contient l'ensemble symbole et indice
        dx = -0.95 * rect.width()/2. - ecart_x  # pour positionner l'ensemble symbole et indice
        #dy_inf = -0.95 * rect.height()/2. + ecart_y 
        #dy_sup =+0.6 * d - ecart_y 
        if Element == 'O_Inductance' or Element == 'O_Reluctance'  :
          objet_texte.setPos(x0 + 0.5*arete + dx , y0 + 0. - 0.70*d)
        if Element == 'O_Inductance_Singuliere' or Element == 'O_Reluctance_Singuliere'  :
          objet_texte.setPos(x0 + 0.5*arete + dx , y0 + arete + 0. + 0.06*d)
        elif Element == 'O_Conductance' or Element == 'O_Resistance' :
          objet_texte.setPos(x0 + 0.42*arete + dx,   y0 + 0.5*arete -0.9*d)
        elif Element == 'O_Capacitance' or Element == 'O_Elastance' :
          objet_texte.setPos(x0 + 0.5*arete + dx,  y0 + arete + 0.06*d)
        elif Element == 'O_Capacitance_Singuliere' or Element == 'O_Elastance_Singuliere' :
          objet_texte.setPos(x0 + 0.5*arete + dx,  y0 - 0.7*d)
        objet.setFlag(QGraphicsItem.ItemIsMovable)
        objet.setFlag(QGraphicsItem.ItemIsSelectable)
        objet.setTextInteractionFlags(Qt.TextEditorInteraction)
        objet.setData(0,'Operateur')
        objet.setData(1,'O_Operateur')
        return objet

def formatte_indice(texte, Repetition):
  P = texte.find("§") # Caractère séparateur Pôle/Dipôle
  Etiquette1 = texte
  if P >= 0 :
    Etiquette2 = texte[:P] # Indice pôle
    Etiquette3 = texte[P + 1:] # Indice Dipôle
    P = Etiquette2.find('0x')
    if P >= 0 : Etiquette2 = "&#x" + Etiquette2[P + 2: P+6] + ";" + Etiquette2[P + 6:]
    P = Etiquette3.find('0x')
    if P >= 0 : Etiquette3 = "&#x" +Etiquette3[P + 2: P+6] + ";" + Etiquette3[P + 6:]
    P = Etiquette3.find('0x')
    if P >= 0 : Etiquette3 = Etiquette3[0: P ] + "&#x" + Etiquette3[P + 2: P+6] + ";" + Etiquette3[P + 6:]
    Etiquette1 = Etiquette2
    if Repetition == 1 : Etiquette1 = Etiquette3
  else :
    H = texte.find("0x")
    if H>=0 : texte = texte[:H] +chr(int(texte[H+2:H+6],  16)) + texte[H+6:]
    Etiquette1 = texte
  return Etiquette1

def Texte_Evolution(self, d, x0,  y0, Element, dynamique, couleur):
    objet = QGraphicsTextItem()
    if operateurs :
        longueur = 0.3*d
        dy = -0.17*d
        if 'O_Inv' in Element :
            longueur = 0.5*d
            dy = -0.1*d
        if dynamique == 1 :
            objet = QGraphicsLineItem(0., 0., longueur, 0.) #Barre de fraction
            objet.setPen(QPen(QColor(couleur)))
            police1 = QFont('Arial', 6. * 0.01*arete)
            police1.setItalic(False)
            #Numérateur
            objet_texte1 = QGraphicsTextItem(parent = objet)
            objet_texte1.setFont(police1)
            objet_texte1.setDefaultTextColor(QColor(coul_elem['temps']))
            objet_texte1.setFlag(QGraphicsItem.ItemIsMovable)
            objet_texte1.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_texte1.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_texte1.installEventFilter(self)
            objet_texte1.setPlainText('d')
            rect = objet_texte1.boundingRect()
            dx = rect.width()
            dy = rect.height()
            objet_texte1.setPos(-0.25*dx , -0.85*dy)

            #Dénominateur
            objet_texte2 = QGraphicsTextItem(parent = objet)
            objet_texte2.setFont(police1)
            objet_texte2.setDefaultTextColor(QColor(couleur))
            objet_texte2.setPlainText('d')
            objet_texte2.setPos(-0.3*dx , -0.18*dy)
            objet_texte2.setFlag(QGraphicsItem.ItemIsMovable)
            objet_texte2.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_texte2.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_texte2.installEventFilter(self)

            police2 = police1
            police2.setItalic(True)
            objet_texte3 = QGraphicsTextItem(parent = objet_texte2)
            objet_texte3.setFont(police2)
            objet_texte3.setDefaultTextColor(QColor(couleur))
            objet_texte3.setPlainText('t')
            objet_texte3.setPos(0.38*dx , 0.)
            objet_texte3.setFlag(QGraphicsItem.ItemIsMovable)
            objet_texte3.setFlag(QGraphicsItem.ItemIsSelectable)
            objet_texte3.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_texte3.installEventFilter(self)
            delta_y = 0.5*arete
            objet.setLine(0.,0.,  0.6*dx, 0.)

            if 'O_Inv' in Element :
                police1.setPointSizeF(4.*0.01*arete)
                police1.setItalic(False)
                objet_exposant1 = QGraphicsTextItem(parent = objet_texte1)
                objet_exposant1.setFont(police1)
                objet_exposant1.setDefaultTextColor(QColor(couleur))
                objet_exposant1.setFlag(QGraphicsItem.ItemIsMovable)
                objet_exposant1.setFlag(QGraphicsItem.ItemIsSelectable)
                objet_exposant1.setTextInteractionFlags(Qt.TextEditorInteraction)
                objet_exposant1.installEventFilter(self)
                objet_exposant1.setPlainText('-1')
                objet_exposant1.setPos(0.45*dx , -0.02*dy)
                objet_exposant2 = QGraphicsTextItem(parent = objet_texte3)
                objet_exposant2.setFont(police1)
                objet_exposant2.setDefaultTextColor(QColor(couleur))
                objet_exposant2.setFlag(QGraphicsItem.ItemIsMovable)
                objet_exposant2.setFlag(QGraphicsItem.ItemIsSelectable)
                objet_exposant2.setTextInteractionFlags(Qt.TextEditorInteraction)
                objet_exposant2.installEventFilter(self)
                objet_exposant2.setPlainText('-1')
                objet_exposant2.setPos(0.35*dx , -0.02*dy)
                objet.setLine(0.,0.,  1.0*dx, 0.)

            if Element == 'O_Evolution_C' or Element == 'O_Involution_C':
                objet.setPos(x0 + arete + 0.2*d,  y0 + delta_y)
            elif Element == 'O_Evolution_L' :
                objet.setPos(x0 - 0.55*d,  y0 + delta_y)
            elif Element == 'O_Involution_L':
                objet.setPos(x0 - 0.7*d,  y0 + delta_y)
                
        else :  # Statique
            objet_texte = QGraphicsTextItem()
            if ObjetFormel[n] == 1 : 
                police = QFont('Times New Roman', 8.*0.01*arete)
                texte = 'N'
            else :    
                police = QFont('Arial', 8.*0.01*arete)
                texte = 'T'
            police.setItalic(True)
            objet_texte.setFont(police)
            objet_texte.setDefaultTextColor(QColor(couleur))
            objet_texte.setTextInteractionFlags(Qt.TextEditorInteraction)
            objet_texte.installEventFilter(self)
            objet_texte.setPlainText(texte)
            delta_y = 40.
            if 'O_Ev' in Element and ObjetFormel[n] > 1 :
                objet_exposant1 = QGraphicsTextItem(parent = objet_texte)
                police.setPointSizeF(4.*0.01*arete)
                police.setItalic(False)
                objet_exposant1.setFont(police)
                objet_exposant1.setDefaultTextColor(QColor(couleur))
                objet_exposant1.setFlag(QGraphicsItem.ItemIsMovable)
                objet_exposant1.setFlag(QGraphicsItem.ItemIsSelectable)
                objet_exposant1.setTextInteractionFlags(Qt.TextEditorInteraction)
                objet_exposant1.installEventFilter(self)
                objet_exposant1.setPlainText('-1')
                objet_exposant1.setPos(0.33*d , -0.02*d)
            objet = objet_texte
            if Element == 'O_Evolution_C' or Element == 'O_Involution_C':
                objet.setPos(x0 + arete + 0.1*d,  y0 + 40.)
            elif Element == 'O_Involution_L' :
                objet.setPos(x0 - 0.50*d,  y0 + 40.)
            elif Element == 'O_Evolution_L':
                objet.setPos(x0 - 0.65*d,  y0 + 40.)
        objet.setFlag(QGraphicsItem.ItemIsMovable)
        objet.setFlag(QGraphicsItem.ItemIsSelectable)
        objet.setData(0,'Operateur')
        objet.setData(1,'O_Operateur')
    return objet  
    
def Implante(self, d, x0,  y0, Element, Prefix_elem, coul_elem):
    global Liste_elements
    global Langue
    global objet_label
    
    Objet_absent = True
    #QMessageBox.information(self, "Info",  "Element = "+Element)              
    objet_operateur = QGraphicsTextItem() 
    if ObjetFormel[n] <= 2 : # Singleton ou Pôle
      Repetition = 0
    else :
      Repetition = 1 # Pour doubler l'indice des propriétés
    for item in self.scene.items(): 
        nom = item.data(1)
        if  nom == Element :
            Objet_absent = False               
    if  Objet_absent :    
        #rectangle = QGraphicsRectItem()

        if Element == 'N_Qte_Base':
            objet = NoeudQteBase(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['qte_base'])
            rect =   objet.boundingRect()
            if ObjetFormel[n] == 1 :
                centre = [x0+arete + rect.left() + 0.5*rect.width(), y0 + rect.top() + 0.5*rect.height()]
            else :
                centre = [x0+arete + rect.left() + 0.5*rect.width(), y0 +arete+ rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_q']
            if Langue == 'E' :  texte = ind[n]['Nom_q']
            objet_legende = TexteLegende(self, d, x0,  y0,  Element, 'Nom_q', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_q',  var[n]['Symb_q'], ind[n]['Symb_q'])
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_q'])
        if Element == 'N_EntiteQte_Base':
            objet = NoeudEntiteQteBase(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['qte_base'])
            rect =   objet.boundingRect()
            centre = [x0+arete + rect.left() + 0.5*rect.width(), y0 +arete+ rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_S_q'] 
            if Langue == 'E' :  texte = ind[n]['Nom_S_q'] 
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_q', texte)
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element,'Symb_S_q',  var[n]['Symb_S_q'],  ind[n]['Symb_S_q'])
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_q'])
        elif Element == 'N_Effort':
            objet = NoeudEffort(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['effort'])
            rect =   objet.boundingRect()
            centre = [x0 + rect.left() + 0.5*rect.width(), y0 +arete+ rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_e']
            if Langue == 'E' :  texte = ind[n]['Nom_e']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_e', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_e', var[n]['Symb_e'], ind[n]['Symb_e'])
            objet_unite = TexteUnites(self, d, x0,  y0, Element, coul_elem['unite_e'])
        elif Element == 'N_EffortSingulier':
            objet = NoeudEffortSingulier(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['effort'])
            rect =   objet.boundingRect()
            centre = [x0 - 0.01*arete + rect.left() + 0.5*rect.width(), y0 + rect.top() + 0.5*rect.height()] # x0 - 0.04*arete +
            if Langue == 'F' : texte = var[n]['Nom_S_e']
            if Langue == 'E' :  texte = ind[n]['Nom_S_e']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_S_e', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_S_e', var[n]['Symb_S_e'], ind[n]['Symb_S_e'])
            objet_unite = TexteUnites(self, d, x0,  y0, Element, coul_elem['unite_e'])
        elif Element == 'N_Impulsion':
            objet = NoeudImpulsion(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['impulsion'])
            rect =   objet.boundingRect()
            if ObjetFormel[n] == 1 :
                centre = [x0 + rect.left() + 0.5*rect.width(), y0 + arete + rect.top() + 0.5*rect.height()]
            else :
                centre = [x0 + rect.left() + 0.5*rect.width(), y0 + rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_p']
            if Langue == 'E' :  texte = ind[n]['Nom_p']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_p', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_p',  var[n]['Symb_p'], ind[n]['Symb_p'])
            objet_unite = TexteUnites(self, d, x0,  y0, Element, coul_elem['unite_p'])
        elif Element == 'N_EntiteImpulsion':
            objet = NoeudEntiteImpulsion(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['impulsion'])
            rect =   objet.boundingRect()
            centre = [x0 - 0.04*arete + rect.left() + 0.5*rect.width(), y0+ rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_S_p']
            if Langue == 'E' :  texte = ind[n]['Nom_S_p']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_p', texte) 
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_S_p',  var[n]['Symb_S_p'],  ind[n]['Symb_S_p'])
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_p'])
        elif Element == 'N_Flot':
            objet = NoeudFlot(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['flot'])
            rect =   objet.boundingRect()
            centre = [x0+arete + rect.left() + 0.5*rect.width(), y0 + rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_f']
            if Langue == 'E' :  texte = ind[n]['Nom_f']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_f', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_f',  var[n]['Symb_f'], ind[n]['Symb_f'] )
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_f'])
        elif Element == 'N_FlotSingulier':
            objet = NoeudFlotSingulier(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['flot'])
            rect =   objet.boundingRect()
            centre = [x0+0.98*arete + rect.left() + 0.5*rect.width(), y0 +arete + rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = var[n]['Nom_S_f']
            if Langue == 'E' :  texte = ind[n]['Nom_S_f']
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_S_f', texte )
            
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_S_f',  var[n]['Symb_S_f'], ind[n]['Symb_S_f'] )
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_f'])
        elif Element == 'N_Compteur_C':
            objet = CompteurQte_Base(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['compteur'])
            rect =   objet.boundingRect()
            centre = [x0+arete + rect.left() + 0.5*rect.width(), y0 +arete+ rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte = "Compteur " + var[n]['Nom_q']
            if Langue == 'E' :  texte = ind[n]['Nom_q'] + " Counter"
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_q', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_q',  var[n]['Symb_q'], ind[n]['Symb_q'])
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_q'])
        elif Element == 'N_Compteur_L':
            objet = CompteurImpulsion(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['compteur'])
            rect =   objet.boundingRect()
            centre = [x0 + rect.left() + 0.5*rect.width(), y0 + rect.top() + 0.5*rect.height()]
            if Langue == 'F' : texte =  "Compteur " + var[n]['Nom_p']
            if Langue == 'E' :  texte = ind[n]['Nom_p'] + " Counter"
            objet_legende = TexteLegende(self, d, x0,  y0, Element, 'Nom_p', texte )
            objet_label = TexteLabel(self, d, x0,  y0, centre, Element, 'Symb_p',  var[n]['Symb_p'], ind[n]['Symb_p'])
            objet_unite = TexteUnites(self, d, x0, y0, Element, coul_elem['unite_p'])
        elif Element == 'O_Capacitance'  :
            objet = Capacitance(d, x0,  y0, Element, coul_elem['capacitance'])
            objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Capacitance',  oper['Capacitance'],oper['Police_C'], ind_op['Capacitance'], ind_op['Police_C'], Repetition)
        elif Element == 'O_Capacitance_Singuliere' or Element == 'O_Elastance_Singuliere' :
            objet = Croix_L(d, x0,  y0, coul_elem['barriere'])
        elif Element == 'O_Elastance'  :
            objet = Elastance(d, x0,  y0, Element, coul_elem['capacitance'])
            objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Elastance', oper['Elastance'],oper['Police_C-1'], ind_op['Elastance'], ind_op['Police_C-1'], Repetition)
        elif Element == 'O_Inductance' or Element == 'O_Inductance_Singuliere' :
            objet = Inductance(d, x0,  y0, Element, coul_elem['inductance'])
            if ObjetFormel[n] == 1 : # Singleton
                objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Inductance_S', oper['Inductance_S'],oper['Police_LS'], ind_op['Inductance_S'], ind_op['Police_L'], Repetition)
            else :
                objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Inductance', oper['Inductance'], oper['Police_L'], ind_op['Inductance'], ind_op['Police_L'], Repetition)
        elif Element == 'O_Reluctance' or Element == 'O_Reluctance_Singuliere' :
            objet = Reluctance(d, x0,  y0, Element, coul_elem['inductance'])
            if ObjetFormel[n] == 1 : # Singleton
                objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Reluctance_S', oper['Reluctance_S'], oper['Police_LS'], ind_op['Reluctance_S'], ind_op['Police_L-1S'], Repetition)
            else :
                objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Reluctance', oper['Reluctance'], oper['Police_L'], ind_op['Reluctance'], ind_op['Police_L-1'], Repetition)
        elif Element == 'O_Conductance':
            objet = Conductance(d, x0,  y0, coul_elem['conductance'])
            objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Conductance',  oper['Conductance'], oper['Police_G'], ind_op['Conductance'], ind_op['Police_G'], Repetition)                    
        elif Element == 'O_Resistance':
            objet = Resistance(d, x0,  y0, coul_elem['conductance'])
            objet_operateur = TexteOper(self, d, x0,  y0, Element, 'Resistance',  oper['Resistance'], oper['Police_R'], ind_op['Resistance'], ind_op['Police_R'], Repetition)                    
        elif Element == 'O_Evolution_C':
            objet = Evolution_C(d, x0,  y0, dynamique, coul_elem['temps'])
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Evolution_C', dynamique, coul_elem['temps'])
        elif Element == 'O_Involution_C':
            objet = Involution_C(d, x0,  y0, dynamique, coul_elem['temps'])
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Involution_C', dynamique, coul_elem['temps'])
        elif Element == 'O_Evolution_L':
            objet = Evolution_L(d, x0,  y0, dynamique, coul_elem['temps'])
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Evolution_L', dynamique, coul_elem['temps'])
        elif Element == 'O_Involution_L':
            objet = Involution_L(d, x0,  y0, dynamique, coul_elem['temps'])
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Involution_L', dynamique, coul_elem['temps'])
        elif Element == 'O_Multiplication_C':
            if NomObjetFormel == 'Singleton_C' : couleur = coul_elem['qte_base']
            if NomObjetFormel == 'Singleton_L' : couleur = coul_elem['flot']
            objet = Evolution_C(d, x0,  y0, 0, couleur)
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Evolution_C', 0, couleur)
        elif Element == 'O_Multiplication_L':
            if NomObjetFormel == 'Singleton_C' : couleur = coul_elem['effort']
            if NomObjetFormel == 'Singleton_L' : couleur = coul_elem['impulsion']
            objet = Evolution_L(d, x0,  y0, 0, couleur)
            objet_operateur = Texte_Evolution(self, d, x0,  y0, 'O_Evolution_L', 0, couleur)
        elif Element == 'O_Croix_D':
            objet = Croix_D(d, x0,  y0, coul_elem['barriere'])
        elif Element == 'O_Croix_G':
            objet = Croix_G(d, x0,  y0, coul_elem['barriere'])
        elif Element == 'O_Croix_C':
            objet = Croix_C(d, x0,  y0, coul_elem['barriere'])
        elif Element == 'O_Croix_L':
            objet = Croix_L(d, x0,  y0, coul_elem['barriere'])
        elif Element == 'C_Somme_E':
            objet = Sommation_Effort(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['synapse'])
        elif Element == 'C_Somme_F':
            objet = Sommation_Flot(d, x0,  y0, 1+int(var[n]['Vecteur']), coul_elem['synapse'])

        self.scene.addItem(objet)
        if Element[0]  == 'N' : # Noeud
            self.scene.addItem(objet_legende)
            self.scene.addItem(objet_label)
            if objet_unite.toPlainText() != '?' : self.scene.addItem(objet_unite )
            
        if Element[0]  == 'O' : # Operateur
            self.scene.addItem(objet_operateur)
        Liste_elements.append(Prefix_elem + Element)
        
def DessineGraphe(self):
    global can_exit
    global vue
    global Liste_elements
    global graphe_existe
    global ObjetFormel # Entier spécifiant l'objet formel choisi
    global Code_SVG
    global Texte_Fenetre
    if ObjetFormel[n] != 0 : # Pas de tracé en cas d'absence d'objet formel
    #Effacement de tous les éléments implantés (Noeuds et Operateurs)
        for item in self.scene.items(): # procédure alternative au clear() qui garde la cadre
            if item.data(0) == 'Noeud' or item.data(0) == 'Operateur'or item.data(0) == 'Label' or item.data(0) == 'Connecteur'or item.data(0) == 'Legende':
                self.scene.removeItem(item)
        Liste_elements = [] # Liste des éléments composant un graphe
        vue.setToolTip("")
        Code_SVG = '' # Afin de forcer le recalcul du code SVG si la visionneuse est actionnée
        Texte_Fenetre = '' # Effacement du code éventuellement visionné
        #Implantation des propriétés demandées    
        if ObjetFormel[n] == 2 : # Pôle
            Implante(self, d, x0,  y0, 'O_Croix_D', '0000', coul_elem)
            Implante(self, d, x0,  y0, 'O_Croix_G', '0000', coul_elem)
        Compte = 0
        if Condu_Resistance[n] == -1  :
            Compte += 1
        if Evol_Involution_L[n] == 1 :
            Compte += 1
        if Capa_Elastance[n] == -1  :
            Compte += 1
        if ObjetFormel[n] == 4 and Compte > 1 :
            Implante(self, d, x0,  y0, 'C_Somme_E', '0000', coul_elem)
        Compte = 0
        if Condu_Resistance[n] == 1 : 
            Compte += 1
        if Evol_Involution_C[n] == 1 : 
            Compte += 1
        if Indu_Reluctance[n] == -1  : 
            Compte += 1
        if ObjetFormel[n] == 4 and Compte > 1 :
            Implante(self, d, x0,  y0, 'C_Somme_F', '0000', coul_elem)
        if Capa_Elastance[n] == 1 :
            if ObjetFormel[n] == 1: ##Singleton
                if NomObjetFormel == "Singleton_C" :  Implante(self, d, x0,  y0, 'O_Croix_C', '0000',  coul_elem) 
                if NomObjetFormel == "Singleton_L" :   Implante(self, d, x0,  y0, 'O_Inductance', '0000', coul_elem)
            else:
                Implante(self, d, x0,  y0, 'O_Capacitance', '0000', coul_elem)
        if Capa_Elastance[n] == -1 :
            if ObjetFormel[n] == 1: ##Singleton
                Implante(self, d, x0,  y0, 'O_Croix_C', '0000', coul_elem) 
            else:
                Implante(self, d, x0,  y0, 'O_Elastance', '0000', coul_elem)
        if Indu_Reluctance[n] == 1 :
            Implante(self, d, x0,  y0, 'O_Inductance', '0000', coul_elem)
        if Indu_Reluctance[n] == -1 :
            Implante(self, d, x0,  y0, 'O_Reluctance', '0000', coul_elem)
        if Condu_Resistance[n] == 1 :
            Implante(self, d, x0,  y0, 'O_Conductance', '0000', coul_elem)
        if Condu_Resistance[n] == -1 :
            Implante(self, d, x0,  y0, 'O_Resistance', '0000', coul_elem)
        if Evol_Involution_C[n] == 1 :
            Implante(self, d, x0,  y0, 'O_Evolution_C', '0000', coul_elem)
        if Evol_Involution_C[n] == -1 :
            Implante(self, d, x0,  y0, 'O_Involution_C', '0000', coul_elem)
        if Evol_Involution_L[n] == 1 :
            Implante(self, d, x0,  y0, 'O_Evolution_L', '0000', coul_elem)
        if Evol_Involution_L[n] == -1 :
            Implante(self, d, x0,  y0, 'O_Involution_L', '0000', coul_elem)
        if Multiplication_C[n] != 0 :
            Implante(self, d, x0,  y0, 'O_Multiplication_C', '0000', coul_elem)
        if Multiplication_L[n] != 0 :    
            Implante(self, d, x0,  y0, 'O_Multiplication_L', '0000', coul_elem)
        if Capa_Elast_Sing[n] != 0 :
            Implante(self, d, x0,  y0, 'O_Croix_L', '0000',  coul_elem) 
        if Indu_Reluct_Sing[n] == 1 :
            Implante(self, d, x0,  y0, 'O_Inductance_Singuliere', '0000', coul_elem)
        if Indu_Reluct_Sing[n] == -1 :
            Implante(self, d, x0,  y0, 'O_Reluctance_Singuliere', '0000', coul_elem)
        
        # Implantation des noeuds en fonction des propriétés        
        if Capa_Elastance[n] == 0 and Evol_Involution_C[n] == -1 : 
            if ObjetFormel[n] == 3  and (Indu_Reluctance[n] != 0 or Condu_Resistance[n] != 0):  # Dipôle
                Implante(self, d, x0,  y0, 'N_Compteur_C', '0000', coul_elem) 
        if Capa_Elastance[n] == 0 and Evol_Involution_C[n] != 0 : 
            if ObjetFormel[n] >= 3  : #and Condu_Resistance[n] != 0 :  # Assemblage conductif
                Implante(self, d, x0,  y0, 'N_Compteur_C', '0000', coul_elem) 
        if ObjetFormel[n] == 1 and (Capa_Elastance[n] != 0  or Multiplication_C[n]) == 1  :   # Singleton capacitif
                Implante(self, d, x0,  y0, 'N_EntiteQte_Base', '0000', coul_elem) 
        if Capa_Elastance[n] != 0 and ObjetFormel[n] > 1: 
            Implante(self, d, x0,  y0, 'N_Qte_Base', '0000', coul_elem)
        if Capa_Elastance[n] != 0 or Condu_Resistance[n] != 0 or Evol_Involution_L[n] != 0 or Multiplication_L[n] == 1 :
            Implante(self, d, x0,  y0, 'N_Effort', '0000', coul_elem)
        if (ObjetFormel[n] == 2 or ObjetFormel[n] == 3) and Condu_Resistance[n] == 0 and Indu_Reluctance[n] != 0:  # Pôle ou dipôle inductif
            Implante(self, d, x0,  y0, 'N_Effort', '0000', coul_elem)
        if Indu_Reluctance[n] == 0 and Evol_Involution_L[n] == -1 : 
            if ObjetFormel[n] == 3  and (Capa_Elastance[n] != 0 or Condu_Resistance[n] != 0):  # Dipôle + Assemblage
                Implante(self, d, x0,  y0, 'N_Compteur_L', '0000', coul_elem) 
        if Indu_Reluctance[n] == 0 and Evol_Involution_L[n] != 0 : 
            if ObjetFormel[n] >= 3  : #and Condu_Resistance[n] == 0 :  # Assemblage conductif
                Implante(self, d, x0,  y0, 'N_Compteur_L', '0000', coul_elem) 
        if  Indu_Reluctance[n] != 0 and ObjetFormel[n] == 1 or Multiplication_L[n] == -1  :   # Singleton inductif
            Implante(self, d, x0,  y0, 'N_EntiteImpulsion', '0000', coul_elem) 
        if Indu_Reluctance[n] != 0 and  ObjetFormel[n] > 1:  
            Implante(self, d, x0,  y0, 'N_Impulsion', '0000', coul_elem) 
        if Multiplication_L[n] == -1 :  # singleton inductif
            Implante(self, d, x0,  y0, 'N_Impulsion', '0000', coul_elem) 
        if Indu_Reluctance[n] != 0 or Condu_Resistance[n] != 0 or Evol_Involution_C[n] != 0 or Multiplication_C[n] == -1:
            Implante(self, d, x0,  y0, 'N_Flot', '0000', coul_elem)
        if (ObjetFormel[n] == 2 or ObjetFormel[n] == 3) and Condu_Resistance[n] == 0 and Capa_Elastance[n] != 0: # Pôle ou dipôle capacitif
            Implante(self, d, x0,  y0, 'N_Flot', '0000', coul_elem)
        if Multiplication_C[n] == 1 or Capa_Elast_Sing[n] != 0 : # Singleton
            Implante(self, d, x0,  y0, 'N_Qte_Base', '0000', coul_elem)
        if Multiplication_L[n] == 1 or Capa_Elast_Sing[n] != 0 : # Singleton
            Implante(self, d, x0,  y0, 'N_EffortSingulier', '0000', coul_elem)
        if Multiplication_L[n] == -1 or Indu_Reluct_Sing[n] != 0 : # Singleton 
            Implante(self, d, x0,  y0, 'N_Impulsion', '0000', coul_elem)
        if Multiplication_C[n] == -1 or Indu_Reluct_Sing[n] != 0 : # Singleton
            Implante(self, d, x0,  y0, 'N_FlotSingulier', '0000', coul_elem)
        graphe_existe = True
        can_exit = False
        ActiveMenus(self, True)
    else:
        QMessageBox.information(self, "Graphe non réalisable",  "Choisissez d'abord un objet Formel !")
        
def EffacerScene(self):
    global vue
    self.scene.clear()
    self.initialiserScene()    
    for item in self.scene.items(): # procédure alternative au clear()
        if item.data(0) == 'Noeud' or item.data(0) == 'Operateur' or item.data(0) == 'Connecteur':
            self.scene.removeItem(item)
    global can_exit
    global Capa_Elastance
    global Indu_Reluctance
    global Condu_Resistance
    global Evol_Involution_C
    global Evol_Involution_L
    global Enum_proprietes
    global ObjetFormel
    global NomObjetFormel
    Capa_Elastance[n] = 0
    Indu_Reluctance[n] = 0
    Condu_Resistance[n] = 0
    Evol_Involution_C[n] = 0
    Evol_Involution_L[n] = 0
    Multiplication_C [n] = 0
    Multiplication_L [n] = 0
    Indu_Reluct_Sing[n]  = 0
    Capa_Elast_Sing[n] = 0

    Enum_proprietes = ''
    ObjetFormel[n] = 0 
    NomObjetFormel = ''
    can_exit = True
    vue.setToolTip("Aire de dessin automatique du graphe\n (par sélection des propriétés ci-contre)")

def  ActiveMenus(self,Etat):
    self.actionEnregistrer_Graphe_en_SVG.setEnabled(Etat)  
    self.actionEnregistrer_Graphe_en_bitmap.setEnabled(Etat)  
    self.actionEnregistrer_description_en_XML.setEnabled(Etat) 
    self.actionEnregistrer_structure_en_XML.setEnabled(Etat) 
    self.actionEnregistrer_structure_en_LaTex.setEnabled(Etat) 
    self.actionCopie_dans_Presse_Papier.setEnabled(Etat)   
    self.actionEffacer.setEnabled(Etat)
    
    self.actionVisionneuse_SVG.setEnabled(Etat)  
    self.actionVisionneuse_Bitmap.setEnabled(Etat)  
    self.actionCode_SVG.setEnabled(Etat)  
    self.actionCode_XML_description.setEnabled(Etat)  
    self.actionCode_XML_structure.setEnabled(Etat)  
    self.actionCode_LaTeX_script.setEnabled(Etat) 
 
def ActiveBoutons(self, Etat):    
    self.tbtInductance.setEnabled(Etat)           
    self.tbtReluctance.setEnabled(Etat)           
    self.tbtConductance.setEnabled(Etat)           
    self.tbtResistance.setEnabled(Etat)           
    self.tbtCapacitance.setEnabled(Etat)           
    self.tbtElastance.setEnabled(Etat)           
    self.tbtEvolution_C.setEnabled(Etat)           
    self.tbtInvolution_C.setEnabled(Etat)           
    self.tbtEvolution_L.setEnabled(Etat)           
    self.tbtInvolution_L.setEnabled(Etat)
    if Etat == False:
        self.tbtInductance.setChecked(False)
        self.tbtReluctance.setChecked(False)
        self.tbtConductance.setChecked(False)
        self.tbtResistance.setChecked(False)
        self.tbtCapacitance.setChecked(False)
        self.tbtElastance.setChecked(False)
        self.tbtEvolution_C.setChecked(False)
        self.tbtInvolution_C.setChecked(False)
        self.tbtEvolution_L.setChecked(False)
        self.tbtInvolution_L.setChecked(False)
        
def AnalysePossibilites(self):
    # Active ou désactive les boutons de propriétés
    global ObjetFormel
    global Capa_Elastance
    global Indu_Reluctance
    global  Condu_Resistance
    global Evol_Involution_C
    global Evol_Involution_L
    if ObjetFormel[n] == 2 or ObjetFormel[n] == 3 : #Pôle ou dipôle     
        if Indu_Reluctance[n] == 0 and Capa_Elastance[n] == 0 and Condu_Resistance[n] == 0 :
            self.tbtInductance.setEnabled(True)  
            self.tbtReluctance.setEnabled(True)
            self.tbtCapacitance.setEnabled(True)           
            self.tbtElastance.setEnabled(True)
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(True)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(True)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(True)           
                self.tbtInvolution_L.setEnabled(True)           
        if Indu_Reluctance[n] == 1 and Condu_Resistance[n] == 0 and Capa_Elastance[n] == 0 :
            self.tbtReluctance.setEnabled(True)
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(False)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(True)           
                self.tbtInvolution_L.setEnabled(False)           
        if Indu_Reluctance[n] == -1 and Condu_Resistance[n] == 0 and Capa_Elastance[n] == 0 :
            self.tbtInductance.setEnabled(True)
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(True)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Condu_Resistance[n] == 1 and Indu_Reluctance[n] == 0 and Capa_Elastance[n] == 0 :
            self.tbtInductance.setEnabled(True)
            self.tbtReluctance.setEnabled(False)
            self.tbtResistance.setEnabled(True)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(True)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Condu_Resistance[n] == -1 and Indu_Reluctance[n] == 0 and Capa_Elastance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(True)
            self.tbtConductance.setEnabled(True)           
            self.tbtCapacitance.setEnabled(True)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Capa_Elastance[n] == 1 and Condu_Resistance[n] == 0 and Indu_Reluctance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(False)
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(True)
            self.tbtElastance.setEnabled(True)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(True)           
                self.tbtInvolution_C.setEnabled(False)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Capa_Elastance[n] == -1 and Condu_Resistance[n] == 0 and Indu_Reluctance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(False)
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(False)
            self.tbtCapacitance.setEnabled(True)           
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Indu_Reluctance[n] == 1 and Condu_Resistance[n] == 1 and Capa_Elastance[n] == 0 :
            self.tbtInductance.setEnabled(True)
            self.tbtReluctance.setEnabled(False)
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(False)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(False)           
        if Indu_Reluctance[n] == -1 and Condu_Resistance[n] == -1 and Capa_Elastance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(True)
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(True)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(True)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(False)           
        if Capa_Elastance[n] == 1 and Condu_Resistance[n] == -1 and Indu_Reluctance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(False)
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(True)
            self.tbtCapacitance.setEnabled(True)           
            self.tbtElastance.setEnabled(False)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(False)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True)           
        if Capa_Elastance[n] == -1 and Condu_Resistance[n] == 1 and Indu_Reluctance[n] == 0 :
            self.tbtInductance.setEnabled(False)
            self.tbtReluctance.setEnabled(False)
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(False)
            self.tbtCapacitance.setEnabled(False)           
            self.tbtElastance.setEnabled(True)
            if ObjetFormel[n] == 3 : #Dipôle  
                self.tbtEvolution_C.setEnabled(False)           
                self.tbtInvolution_C.setEnabled(False)           
                self.tbtEvolution_L.setEnabled(False)           
                self.tbtInvolution_L.setEnabled(True) 
    if ObjetFormel[n] == 3 : #Dipôle  
        if Evol_Involution_C[n] != 0 :
            if Capa_Elastance[n] != 0  : 
                self.tbtConductance.setEnabled(False)           
                self.tbtResistance.setEnabled(False)
            if Condu_Resistance[n] != 0 :
                self.tbtCapacitance.setEnabled(False)           
                self.tbtElastance.setEnabled(False)
        if Evol_Involution_L[n] != 0 :
            if Indu_Reluctance[n] != 0 :
                self.tbtConductance.setEnabled(False)           
                self.tbtResistance.setEnabled(False)
            if Condu_Resistance[n] != 0 :
                self.tbtInductance.setEnabled(False)
                self.tbtReluctance.setEnabled(False)
    if ObjetFormel[n] == 4 : # Assemblage 
        Autorise = [True, True, True, True, True, True, True, True, True, True]
        if Capa_Elastance[n] == 1 : # Capacitance
            Autorise[3] = False # Résistance
            Autorise[4] = False # Inductance
            Autorise[7] = False # Involution_C
            Autorise[9] = False # Evolution_L
        if Capa_Elastance[n] == -1 : # Elastance
            Autorise[5] = False # Réluctance
            Autorise[6] = False # Evolution_C            
        if Condu_Resistance[n] == 1 :
            Autorise[4] = False # Inductance
            Autorise[9] = False # Evolution_L            
        if Condu_Resistance[n] == -1 :    
            Autorise[0] = False #  Capacitance
            Autorise[6] = False # Evolution_C            
        if Indu_Reluctance[0] == 1 :
            Autorise[0] = False #  Capacitance
            Autorise[2] = False # Conductance
            Autorise[6] = False # Evolution_C            
            Autorise[8] = False # Involution_L              
        if Indu_Reluctance[0] == -1 : 
            Autorise[1] = False # Elastance
            Autorise[9] = False # Evolution_L        
        if Evol_Involution_C[0] == 1 :
            Autorise[1] = False # Elastance
            Autorise[3] = False # Résistance
            Autorise[4] = False # Inductance
            Autorise[9] = False # Evolution_L    
        if Evol_Involution_C[0] == -1 :
            Autorise[0] = False #  Capacitance
        if Evol_Involution_L[0] == 1 :  
            Autorise[0] = False #  Capacitance
            Autorise[2] = False # Conductance
            Autorise[5] = False # Réluctance
            Autorise[6] = False # Evolution_C            
        if Evol_Involution_L[0] == -1 : 
            Autorise[4] = False # Inductance
        self.tbtCapacitance.setEnabled(Autorise[0])
        self.tbtElastance.setEnabled(Autorise[1])
        self.tbtConductance.setEnabled(Autorise[2])
        self.tbtResistance.setEnabled(Autorise[3])
        self.tbtInductance.setEnabled(Autorise[4])
        self.tbtReluctance.setEnabled(Autorise[5])
        self.tbtEvolution_C.setEnabled(Autorise[6])           
        self.tbtInvolution_C.setEnabled(Autorise[7])           
        self.tbtInvolution_L.setEnabled(Autorise[8])           
        self.tbtEvolution_L.setEnabled(Autorise[9])  
    
def Enumere_proprietes(self):
    # Construit la chaîne "Enum_proprietes" à partir de la liste "Liste_elements"
    Enum_proprietes = ''
    if len(Liste_elements) :
        #print(Liste_elements)
        for item in Liste_elements :
            Prefix_elem = item [:4]
            Element = item [4:]
            if Element == 'O_Capacitance':
                Enum_proprietes += Suffix_prop['Capacitance']
            elif Element == 'O_Elastance':
                Enum_proprietes += Suffix_prop['Elastance']
            elif Element == 'O_Inductance':
                Enum_proprietes += Suffix_prop['Inductance']
            elif Element == 'O_Reluctance':
                Enum_proprietes += Suffix_prop['Reluctance']
            elif Element == 'O_Conductance' :
                Enum_proprietes += Suffix_prop['Conductance'] 
            elif Element == 'O_Resistance':
                Enum_proprietes += Suffix_prop['Resistance'] 
            elif Element == 'O_Evolution_C' or Element == 'O_Evolution_L':
                if dynamique : 
                    Enum_proprietes += 'd' + Suffix_prop['Evolution'].lower() 
                else :
                    Enum_proprietes += Suffix_prop['Evolution']
            elif Element == 'O_Involution_C' or Element == 'O_Involution_L':
                if dynamique : 
                    Enum_proprietes += 'd' + Suffix_prop['Involution'].lower() 
                else :
                    Enum_proprietes += Suffix_prop['Involution']
            elif Element == 'O_Croix_C' or Element == 'O_Croix_L' :
                Enum_proprietes += Suffix_prop['Barriere'] # Suffixe = 'X'
            elif Element == 'O_Multiplication_C':                    # Les multiplications par N sont toujours par paire
                Enum_proprietes += Suffix_prop['Multiplication_C'] # Suffixe = 'N'
            elif Element == 'O_Multiplication_L':                    # (mais construites individuellement)
                Enum_proprietes += Suffix_prop['Multiplication_L'] # Suffixe = 'N'
            elif Element == 'O_Inductance_Singuliere':
                Enum_proprietes += Suffix_prop['Inductance'] + 's'  # Position inférieure
            elif Element == 'O_Reluctance_Singuliere':
                Enum_proprietes += Suffix_prop['Reluctance'] + 's' # Position inférieure
    return Enum_proprietes

def Reset_noms_boutons(self):
    self.lblNoeud_Impulsion.setText('<html><head/><body><p><img src=":/Ressources/GF_Impulsion150.gif"/></p></body></html>')        
    self.lblImpulsion.setText('Impulsion')
    self.lblNoeud_Flot.setText('<html><head/><body><p><img src=":/Ressources/GF_Flot150.gif"/></p></body></html>')        
    self.lblFlot.setText('Flot')
    self.lblNoeud_Effort.setText('<html><head/><body><p><img src=":/Ressources/GF_Effort150.gif"/></p></body></html>')        
    self.lblEffort.setText('Effort')
    self.lblNoeud_QuantiteBase.setText('<html><head/><body><p><img src=":/Ressources/GF_QteBase150.gif"/></p></body></html>')        
    self.lblQuantiteBase.setText('Quantité de Base')
    icon = QIcon()
    icon.addPixmap(QPixmap(":/Ressources/ico_Inductance_nue.gif"), QIcon.Normal, QIcon.Off)
    self.tbtInductance.setIcon(icon)
    self.tbtInductance.setText('Inductance')           
    icon = QIcon()
    icon.addPixmap(QPixmap(":/Ressources/ico_Reluctance_nue.gif"), QIcon.Normal, QIcon.Off)
    self.tbtReluctance.setIcon(icon)
    self.tbtReluctance.setText('Réluctance')           
    icon = QIcon()
    icon.addPixmap(QPixmap(":/Ressources/ico_Capacitance_nue.gif"), QIcon.Normal, QIcon.Off)
    self.tbtCapacitance.setIcon(icon)
    self.tbtCapacitance.setText('Capacitance')           
    icon = QIcon()
    icon.addPixmap(QPixmap(":/Ressources/ico_Elastance_nue.gif"), QIcon.Normal, QIcon.Off)
    self.tbtElastance.setIcon(icon)
    self.tbtElastance.setText('Elastance')           
    self.tbtEvolution_L.setText('    Evolution')          
    self.tbtEvolution_C.setText('    Evolution') 
    Multiplication_C[n] = 0
    Multiplication_L[n] = 0

def Extraction_Commentaire(self, nomFichier):
    # Extrait un commentaire Exif ou un texte d'un fichier JPEG, PNG ou BMP
    # Dans un BMP, le format doit être celui d'un chunk PNG nnnntEXtxxxxxxxxxxxxxxxxxcrcx
    Fichier = open(nomFichier, "rb")
    sequence_octets = Fichier.read()
    Fichier.close()
    if len(sequence_octets) == 0 :
        texte = ''
        return
    #print('longueur =', len(sequence_octets))
    posExif = sequence_octets.find(b'Exif')
    if posExif > 5 :
        #print ('\'Exif\' at', posExif)
        if sequence_octets[12:14] == b'MM' : Mode = 'big'
        if sequence_octets[12:14] == b'II' : Mode = 'little'
        pos9286 = sequence_octets.find(b'\x92\x86\x00\x07')
        if pos9286 > 0 :
            octets4 = sequence_octets[pos9286+4:pos9286+8]
            Nb_octets = int.from_bytes(octets4, byteorder=Mode)
            posUnicode = sequence_octets.find(b'UNICODE')
            texte = sequence_octets[posUnicode+8:posUnicode+Nb_octets].decode(encoding="utf-16", errors="strict")
        else :
            #print ('Pas de texte Unicode en commentaire')
            texte = ''
    postEXt = sequence_octets.find(b'tEXt')
    if postEXt > 5 :
        octets4 = sequence_octets[postEXt-4:postEXt]
        Nb_octets = int.from_bytes(octets4, byteorder='big')
        #print('Nb_octets =', Nb_octets)
        texte = sequence_octets[postEXt+4:postEXt+4+Nb_octets].decode(encoding="utf-8", errors="strict")
    if postEXt < 5  and posExif < 5 :
        #print ('Pas de données \'Exif\' ou  \'tEXt\' dans ce fichier')
        texte = ''
    return texte  

def Enregistrement_Texte_JPEG(self,  nomFichier, pixmap,  texte):
    # imports : struct, os, QByteArray, QBuffer, QIODevice
    image_bytes = QByteArray() # Déclaration d'un bytearray
    buf = QBuffer(image_bytes) # Acssociation du bytearray au buffer
    buf.open(QIODevice.WriteOnly) # Ouverture du buffer en mode écriture only
    pixmap.save(buf, 'JPEG') #Remplissage du buffer, et donc du bytearray avec le pixmap
    buf.close()
    image_bytes = image_bytes[2:] # Suppression du FFD8
    chaine = "FFD8FFE100004578696600004D4D002A00000008" # Position image "0000" à ajuster  JFIF ="45786966" MM ="4D4D" *="2A"
    chaine += "000187690004000000010000001A0000000"
    # (Sub)Directory EXIF NbTags ="0001" ID = "8769" Format = "0004" Nb composants ="00000001" Offset ="0000001A" (=26) Séparateur ="00000000"
    chaine +="00001928600070000088C0000002C00000000554E49434F444500"
    # (Sub)Directory EXIF NbTags ="0001" ID = "9286" Format = "0007" Nb composants ="0000088C" Offset ="0000002C" (=44) Séparateur ="00000000"
    # Encodage ="554E49434F4445" (=UNICODE) Nb composants = "00000xxx" (= 8 + 2*longueur texte) à ajuster 
    debut_bytes = bytearray.fromhex(chaine)            
    texte_bytes = bytearray(texte, 'utf-16')  # 2 octets par caractère
    texte_bytes = texte_bytes[2:] # suppression deux premiers octets FFFE
    longueur = len(texte_bytes)
    #print ('taille_texte_bytes =', longueur)
    #print(texte_bytes)
    octets4 = struct.pack('>L', 8+ longueur) # big_endian 	L = unsigned long
    debut_bytes[44:48] = octets4
    #print ('octets_taille_unicode =', debut_bytes[44:48])
    longueur_exif = len(debut_bytes)+len(texte_bytes) 
    #print ('longueur_exif =', longueur_exif)
    octets2 = struct.pack('>H', longueur_exif) # big_endian 	H = unsigned short
    debut_bytes[4:6] = octets2
    #print ('octets_fin_exif =', debut_bytes[4:6])
    sequence_bytes = debut_bytes+ texte_bytes
    fichier_bytes = sequence_bytes + image_bytes
    fichier = open(nomFichier, 'wb') # Ecriture binaire
    fichier.write(fichier_bytes) # Enregistrement du bytearray
    fichier.close()
    taille = os.path.getsize(nomFichier)
    return taille

def Enregistrement_Texte_PNG(self,  nomFichier, pixmap,  texte):
    # imports : struct, os, binascii, QByteArray, QBuffer, QIODevice
    # Le format est celui d'un chunk PNG 'nnnntEXtxxxxxxxxxxxxxxxxxcrcx' avec nnnn le nombre d'octets placé en fin de fichier
    image_bytes = QByteArray() # Déclaration d'un bytearray
    buf = QBuffer(image_bytes) # Acssociation du bytearray au buffer
    buf.open(QIODevice.WriteOnly) # Ouverture du buffer en mode écriture only
    pixmap.save(buf, 'PNG') #Remplissage du buffer, et donc du bytearray avec le pixmap
    buf.close()
    #print ('taille_image_bytes =', len(image_bytes))
    posIEND = len(image_bytes)-12 # La séquence d'octets ne contient rien d'autre que l'image
    sequence_fin = image_bytes[posIEND:]
    longueur_texte = len(texte)
    texte_bytes = bytearray("tEXt" + texte, 'utf-8')  # 1 octet par caractère
    crc = binascii.crc32(texte_bytes)
    #print('crc32 = {:#010x}'.format(crc))
    crc_octets = struct.pack('>L', crc) # big_endian 	L = unsigned long
    #print('crc =', crc_octets)
    octets4 = struct.pack('>L', longueur_texte) # big_endian 	L = unsigned long
    sequence_bytes = bytearray(4 + len(texte_bytes)+ 12)
    sequence_bytes = octets4 + texte_bytes + crc_octets + sequence_fin
    #print(sequence_bytes)
    fichier_bytes = image_bytes + sequence_bytes
    fichier = open(nomFichier, 'wb') # Ecriture binaire
    fichier.write(fichier_bytes) # Enregistrement du bytearray
    fichier.close()
    taille = os.path.getsize(nomFichier)
    return taille

def Enregistrement_Texte_BMP(self,  nomFichier, pixmap,  texte):
    # imports : struct, os, QByteArray, QBuffer, QIODevice
    # Le format est celui d'un chunk PNG sans crc 'nnnntEXtxxxxxxxxxxxxxxxxx' avec nnnn le nombre d'octets placé à la fin du fichier
    image_bytes = QByteArray() # Déclaration d'un bytearray
    buf = QBuffer(image_bytes) # Acssociation du bytearray au buffer
    buf.open(QIODevice.WriteOnly) # Ouverture du buffer en mode écriture only
    pixmap.save(buf, 'BMP') #Remplissage du buffer, et donc du bytearray avec le pixmap
    buf.close()
    longueur_texte = len(texte)
    texte_bytes = bytearray("tEXt" + texte, 'utf-8')  # 1 octet par caractère
    octets4 = struct.pack('>L', longueur_texte) # big_endian 	L = unsigned long
    sequence_bytes =  octets4 + texte_bytes
    #print ('taille_sequence_bytes =', len(sequence_bytes))
    #print(sequence_bytes)
    fichier_bytes =image_bytes + sequence_bytes
    #print ('taille_fichier_bytes =', len(fichier_bytes))
    fichier = open(nomFichier, 'wb') # Ecriture binaire
    fichier.write(fichier_bytes) # Enregistrement du bytearray
    fichier.close()
    taille = os.path.getsize(nomFichier)
    return taille
    
class MainWindow(QMainWindow,  Ui_MainWindow):
    
    def __init__(self,  parent = None):
        global redimensionnement
        global ordre_cles
        global taille_symbole

        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        redimensionnement =0
        self.showMaximized()
        #print("Maximized", self.size().width(), "x", self.size().height())
        
        self.scene =  QGraphicsScene() # La scène contient les dessins (items), la vue montre ce qui est affiché
        self.initialiserScene() # Création du fond blanc et de la scene avec un carré de bord gris (carré élémentaire)
        
        ActiveBoutons(self, False)
        ActiveMenus(self, False)
        
        Lecture_parametres(self) # Dans fichiers 'GF_Data_varietes.csv' et 'GF_Data_proprietes.csv'
        self.spnTailleSymb.setValue(taille_symbole)
        self.svg_def = Definitions_SVG()
        self.svg_elem = Dico_Elements_SVG()
        construction_listes_varietes(self,  Langue) # Procédure
        self.cb = self.comboBox # Copie de l'objet "liste de choix" définie  dans Ui_labels_variables.py
        i = 0
        for cle in ordre_cles : # Remplissage du combobox dans l'ordre du tableau CSV
            i += 1
            for paire in self.liste_varietes : #  paire (nom variété, cle)
                if cle == paire[1] :
                    self.cb.addItem(paire[0], userData = paire[1])
                    #print("i =",  i, "cle =",  cle, paire[0])
                    break
            
        self.show()
             
        global vue # = ce qui est affiché
        vue = self.grvGraphe # grvGraphe = QGraphicsView défini dans Ui_MainWindow 
        vue.setScene(self.scene)
        vue.setRenderHints(QPainter.Antialiasing)
        vue.fitInView(self.fond,  Qt.KeepAspectRatio)
        vue.showFullScreen()

        global autorise_constr_dicos
        # Detection d'un choix effectué
        self.cb.currentIndexChanged.connect(self.selectionchange) #Renvoi à la procédure adhoc
        autorise_constr_dicos = True
        self.cb.setCurrentIndex(1)
        self.cb.setCurrentIndex(0)

    def initialiserScene(self):
        # définition de la scène
        self.scene.clear()
        self.fond= QGraphicsRectItem(0., 0., delta_x, delta_y)
        self.fond.setBrush(QBrush(Qt.white) )
        self.fond.setPen(QPen(Qt.white))     
        self.scene.addItem(self.fond)
        #print("initialisation scene largeur =", self.scene.width(), "hauteur =", self.scene.height())

        cadre = QGraphicsRectItem(x0, y0, arete, arete)
        cadre.setBrush(QBrush(Qt.white) )
        cadre.setPen(QPen(QColor(164, 164, 164)) )
        self.scene.addItem(cadre)

    def selectionchange(self, i):
        global graphe_existe
        global autorise_constr_dicos
        global Vecteur
        global Variete
        global Fichier_signification
        global Signification_on
        #for count in range(self.cb.count()):
            # print (self.cb.itemText(count))
        self.cb.setMaxVisibleItems(self.cb.count())
        #Variete_longue = self.cb.currentText() #Nom de la variété choisie
        cle = self.cb.itemData(i)  #Cle de la variété choisie
        # print ("Current index", i, "selection changed ", Variete_longue,  "userData =", cle)
        if autorise_constr_dicos :
            construction_dicos_var(self, cle) # Procédure
            construction_dicos_prop(self, cle) # Procédure
            #print ("dicos construits")
        Vecteur = int(var[n]['Vecteur'])
        Variete[n] = cle # var[n]['Nom court']
        #print("Variete =", Variete[n],'-------------------------------')
        if Signification_on :
            Fichier_signification = 'Docs/Dessinateur_GF_Signif_' + Variete[n] +'.htm'
            if self.popUp.isVisible() :
                self.popUp = Fenetre_Afficheur()
                self.popUp.setGeometry(300, 30, 800, 600)
                self.popUp.show()
            else :
                Signification_on = False
        remplissage_labels(self) # Procédure
        #Affiche(self)
        if graphe_existe :
            self.initialiserScene()    
            DessineGraphe(self)
            
    def closeEvent(self, event): # Fermeture demandée
        global can_exit 
        if can_exit:
            event.accept() # let the window close
        else:
            Reponse = QMessageBox.question(self, "Demande de confirmation", \
            "Le graphe n'a pas été sauvegardé. Quitter quand même ?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if Reponse == QMessageBox.Yes:
                event.accept() 
            elif Reponse == QMessageBox.No:
                event.ignore()
        return
            
    def eventFilter(self, source, event):
        global police_legendes
        global taille_legendes
        global style_legendes
        if    (event.type() == QEvent.FocusOut) : #and source is QGraphicsTextItem()): 
            #print("Name =",  source.objectName())  # forme "Type%id_texte"
            id_objet = source.objectName().split('%')
            if id_objet[0] == "Legende" and len(id_objet) > 1:
                texte = source.toPlainText()
                if len(id_objet) >2 : # C'est une seconde ligne
                    for item in self.scene.items(): # recherche de la premiere ligne pour fusionner avec la seconde
                        if item.data(0) == 'Legende' :
                            #print ("Name =", item.objectName(), " Data[1] =", item.data(1) )
                            id2_objet = item.objectName().split('%')
                            if len(id2_objet) == 2 and id2_objet[1] == id_objet[1] :
                                texte = item.toPlainText() + ' ' + source.toPlainText()
                                print ("texte =", texte,  Langue)
                if Langue == 'F' : var[n][id_objet[1]] = texte
                if Langue == 'E' : ind[n][id_objet[1]] = texte 
            if id_objet[0] == "Unite" and len(id_objet) > 1:
                    texte = source.toPlainText().split('=')
                    if len(texte) > 1 :
                        ind[n][id_objet[1]] = texte[0].strip(' ')
                        var[n][id_objet[1]] = texte[1].strip(' ')
                    else :
                        ind[n][id_objet[1]] = " "
                        var[n][id_objet[1]] = texte[0].strip(' ')
            if id_objet[0] == "Symbole" and len(id_objet) > 1:
                    var[n][id_objet[1]] = source.toPlainText()
            if id_objet[0] == "Indice" and len(id_objet) > 1:
                    ind[n][id_objet[1]] = source.toPlainText()
            if id_objet[0] == "Symb_op" and len(id_objet) > 1:
                    oper[id_objet[1]] = source.toPlainText()
            if id_objet[0] == "Indice_op" and len(id_objet) > 1:
                    ind_op[id_objet[1]] = source.toPlainText()
            #print ("Plain Texte =",  source.toPlainText())
            #print ("Html Texte =",  source.toHtml()) # permet de récupérer les attributs font-family, font-size, font-style, font-weight (en numérique)
        
        if event.type() == QEvent.GraphicsSceneMouseDoubleClick :
           #print(source)
           if source :  
                police = source.font()
                font, ok = QFontDialog.getFont(police)
                if ok:
                    source.setFont(font)
                    police = font.family()
                    style = ''
                    if font.bold() : style = 'bold'
                    if font.italic() : style = 'italic'
                    if font.bold() and font.italic(): style = 'bolditalic'
                    taille = 2. * font.pointSizeF()
                    id_objet = source.objectName().split('%')
                    if len(id_objet) > 1:
                        lettre_id = id_objet[1].split('_')
                        if id_objet[0] == "Legende" and len(id_objet) > 1:
                            police_legendes = police
                            taille_legendes = taille / 2.
                            style_legendes = style
                            #print("police_legendes =", police_legendes)
                            #print("taille_legendes =",  taille_legendes )
                            #print("style_legendes =",  style_legendes )
                        
                        if id_objet[0] == "Unite" and len(id_objet) > 1:   # attributs fixes
                                texte = source.toPlainText().split('=')
                                if len(texte) > 1 :
                                    ind[n][id_objet[1]] = texte[0].strip(' ')
                                    var[n][id_objet[1]] = texte[1].strip(' ')
                                else :
                                    ind[n][id_objet[1]] = " "
                                    var[n][id_objet[1]] = texte[0].strip(' ')
                        if id_objet[0] == "Symbole" and len(lettre_id) > 1:
                                var[n]['Police_' + lettre_id[1]] = police
                                var[n]['Taille_' + lettre_id[1]] = taille
                                var[n]['Police_' + lettre_id[1]] = style
                        if id_objet[0] == "Indice" and len(lettre_id) > 1:
                                ind[n]['Police_' + lettre_id[1]] = police
                                ind[n]['Taille_' + lettre_id[1]] = taille
                                ind[n]['Police_' + lettre_id[1]] = style
                        if id_objet[0] == "Symb_op" and len(lettre_id) > 1:
                                oper['Police_' + lettre_id[1]] = police
                                oper['Taille_' + lettre_id[1]] = taille
                                oper['Police_' + lettre_id[1]] = style
                        if id_objet[0] == "Indice_op" and len(lettre_id) > 1:
                                ind_op['Police_' + lettre_id[1]] = police
                                ind_op['Taille_' + lettre_id[1]] = taille
                                ind_op['Police_' + lettre_id[1]] = style
        return QWidget.eventFilter(self, source, event)

    def on_action450_x_300_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "450x300"  
        self.action450_x_300_pixels.setChecked(True)
        self.action600_x_400_pixels.setChecked(False)
        self.action800_x_600_pixels.setChecked(False)
        self.action1000_x_750_pixels.setChecked(False)
        self.action1200_x_800_pixels.setChecked(False)
        self.action1600_x_1200_pixels.setChecked(False)

    def on_action600_x_400_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "600x400"  
        self.action450_x_300_pixels.setChecked(False)
        self.action600_x_400_pixels.setChecked(True)
        self.action800_x_600_pixels.setChecked(False)
        self.action1000_x_750_pixels.setChecked(False)
        self.action1200_x_800_pixels.setChecked(False)
        self.action1600_x_1200_pixels.setChecked(False)

    def on_action800_x_600_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "800x600" 
        self.action450_x_300_pixels.setChecked(False)
        self.action600_x_400_pixels.setChecked(False)
        self.action800_x_600_pixels.setChecked(True)
        self.action1000_x_750_pixels.setChecked(False)
        self.action1200_x_800_pixels.setChecked(False)
        self.action1600_x_1200_pixels.setChecked(False)

    def on_action1000_x_750_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "1000x750"  
        self.action450_x_300_pixels.setChecked(False)
        self.action600_x_400_pixels.setChecked(False)
        self.action800_x_600_pixels.setChecked(False)
        self.action1000_x_750_pixels.setChecked(True)
        self.action1200_x_800_pixels.setChecked(False)
        self.action1600_x_1200_pixels.setChecked(False)

    def on_action1200_x_800_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "1200x800"  
        self.action450_x_300_pixels.setChecked(False)
        self.action600_x_400_pixels.setChecked(False)
        self.action800_x_600_pixels.setChecked(False)
        self.action1000_x_750_pixels.setChecked(False)
        self.action1200_x_800_pixels.setChecked(True)
        self.action1600_x_1200_pixels.setChecked(False)

    def on_action1600_x_1200_pixels_triggered(self):
        global Dimension_bitmap
        Dimension_bitmap = "1600x1200"  
        self.action450_x_300_pixels.setChecked(False)
        self.action600_x_400_pixels.setChecked(False)
        self.action800_x_600_pixels.setChecked(False)
        self.action1000_x_750_pixels.setChecked(False)
        self.action1200_x_800_pixels.setChecked(False)
        self.action1600_x_1200_pixels.setChecked(True)


    @pyqtSlot()  

    def on_tbtSingleton_L_released(self):
        global ObjetFormel
        global NomObjetFormel
        source = self.sender()
        EffacerScene(self)
        if source.isChecked():
            ActiveBoutons(self, False)
            ActiveMenus(self, False)
            ObjetFormel[n] = 1
            NomObjetFormel = "Singleton_L"
            self.tbtSingleton_C.setChecked(False)
            self.tbtPole.setChecked(False)
            self.tbtDipole.setChecked(False)
            self.tbtAssemblage.setChecked(False)
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Inductance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtInductance.setIcon(icon)
            self.tbtInductance.setText('Inductance')           
            self.tbtInductance.setEnabled(True)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Reluctance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtReluctance.setIcon(icon)
            self.tbtReluctance.setText('Réluctance')           
            self.tbtReluctance.setEnabled(True)           
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(False)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Inductance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtCapacitance.setIcon(icon)
            self.tbtCapacitance.setText('Inductance')           
            self.tbtCapacitance.setEnabled(True)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Reluctance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtElastance.setIcon(icon)
            self.tbtElastance.setText('Réluctance')           
            self.tbtElastance.setEnabled(True)           
            self.tbtEvolution_C.setText('   Multiplication') 
            self.tbtEvolution_C.setToolTip('Conjugaison corpusculaire\n(multiplication par le nombre de corpuscules)')
            self.tbtEvolution_C.setEnabled(True)           
            self.tbtInvolution_C.setEnabled(False)           
            self.tbtEvolution_L.setText('   Multiplication')
            self.tbtEvolution_L.setToolTip('Conjugaison corpusculaire\n(multiplication par le nombre de corpuscules)')
            self.tbtEvolution_L.setEnabled(True)           
            self.tbtInvolution_L.setEnabled(False) 
            self.tbtInvolution_L.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.tbtInvolution_C.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.lblNoeud_Impulsion.setText('<html><head/><body><p><img src=":/Ressources/GF_Entite_Impulsion62.gif"/></p></body></html>')        
            self.lblImpulsion.setText('Entité\nImpulsion')
            self.fraImpulsion.setToolTip('Entité inductive\n (Variable d\'état réduite)')
            self.lblNoeud_Flot.setText('<html><head/><body><p><img src=":/Ressources/GF_Flot150.gif"/></p></body></html>')        
            self.lblFlot.setText('Flot')
            self.fraFlot.setToolTip('Energie-par-entité inductive\n (Variable d\'état)')
            self.lblNoeud_Effort.setText('<html><head/><body><p><img src=":/Ressources/GF_Impulsion150.gif"/></p></body></html>')        
            self.lblEffort.setText('Impulsion')
            self.fraEffort.setToolTip('Nombre d\'entités inductives\n (Variable d\'état)')
            self.lblNoeud_QuantiteBase.setText('<html><head/><body><p><img src=":/Ressources/GF_Singulier_Flot62.gif"/></p></body></html>')        
            self.lblQuantiteBase.setText('Flot singulier')
            self.fraQuantiteBase.setToolTip('Energie-par-entité inductive corpusculaire\n (Variable d\'état réduite)')
            Multiplication_C[n] = 0
            Multiplication_L[n] = 0

            
        else:
            ObjetFormel[n] = 0
            ActiveBoutons(self, False)
            Reset_noms_boutons(self)

            
    def on_tbtSingleton_C_released(self):
        global ObjetFormel
        global NomObjetFormel
        source = self.sender()
        EffacerScene(self)
        if source.isChecked():
            ActiveBoutons(self, False)
            ActiveMenus(self, False)
            ObjetFormel[n] = 1
            NomObjetFormel = "Singleton_C"
            self.tbtSingleton_L.setChecked(False)
            self.tbtPole.setChecked(False)
            self.tbtDipole.setChecked(False)
            self.tbtAssemblage.setChecked(False)
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Capacitance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtInductance.setIcon(icon)
            #self.tbtInductance.setIconSize(QSize(31, 10))
            self.tbtInductance.setText('Capacitance')
            self.tbtInductance.setEnabled(True)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Elastance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtReluctance.setIcon(icon)
            self.tbtReluctance.setText('Elastance')           
            self.tbtReluctance.setEnabled(True)           
            self.tbtConductance.setEnabled(False)           
            self.tbtResistance.setEnabled(False)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Capacitance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtCapacitance.setIcon(icon)
            self.tbtCapacitance.setText('Capacitance')           
            self.tbtCapacitance.setEnabled(True)           
            icon = QIcon()
            icon.addPixmap(QPixmap(":/Ressources/ico_Elastance_nue.gif"), QIcon.Normal, QIcon.Off)
            self.tbtElastance.setIcon(icon)
            self.tbtElastance.setText('Elastance')           
            self.tbtElastance.setEnabled(True)           
            self.tbtEvolution_C.setText('   Multiplication') 
            self.tbtEvolution_C.setToolTip('Conjugaison corpusculaire\n(multiplication par le nombre de corpuscules)')
            self.tbtEvolution_C.setEnabled(True)           
            self.tbtInvolution_C.setEnabled(False)           
            self.tbtEvolution_L.setText('   Multiplication')
            self.tbtEvolution_L.setToolTip('Conjugaison corpusculaire\n(multiplication par le nombre de corpuscules)')
            self.tbtEvolution_L.setEnabled(True)           
            self.tbtInvolution_L.setEnabled(False)
            self.tbtInvolution_L.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.tbtInvolution_C.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.lblNoeud_Impulsion.setText('<html><head/><body><p><img src=":/Ressources/GF_Singulier_Effort63.gif"/></p></body></html>')
            self.lblImpulsion.setText('Effort\nsingulier')
            self.fraImpulsion.setToolTip('Energie-par-entité capacitive corpusculaire\n (Variable d\'état réduite)')
            self.lblNoeud_Flot.setText('<html><head/><body><p><img src=":/Ressources/GF_QteBase150.gif"/></p></body></html>')
            self.lblFlot.setText('Quantité de base')
            self.fraFlot.setToolTip('Nombre d\'entités capacitives\n (Variable d\'état)')
            self.lblNoeud_Effort.setText('<html><head/><body><p><img src=":/Ressources/GF_Effort150.gif"/></p></body></html>')
            self.fraEffort.setToolTip('Energie-par-entité capacitive\n (Variable d\'état)')
            self.lblEffort.setText('Effort')
            self.lblNoeud_QuantiteBase.setText('<html><head/><body><p><img src=":/Ressources/GF_Entite_QteBase63.gif"/></p></body></html>')
            self.fraQuantiteBase.setToolTip('Entité capacitive\n (Variable d\'état réduite)')
            self.lblQuantiteBase.setText('Entité\nQuantité de base')
            Multiplication_C[n] = 0
            Multiplication_L[n] = 0 
            
            
        else:
            ObjetFormel[n] = 0
            ActiveBoutons(self, False)
            Reset_noms_boutons(self)
            
    def on_tbtPole_released(self):
        global ObjetFormel
        global NomObjetFormel
        EffacerScene(self)
        source = self.sender()
        if source.isChecked():
            ActiveBoutons(self, False)
            ActiveMenus(self, False)
            Reset_noms_boutons(self)
            ObjetFormel[n] = 2
            NomObjetFormel = "Pole"
            self.tbtSingleton_L.setChecked(False)
            self.tbtSingleton_C.setChecked(False)
            self.tbtDipole.setChecked(False)
            self.tbtAssemblage.setChecked(False)
            
            self.tbtInductance.setEnabled(True)           
            self.tbtReluctance.setEnabled(True)           
            self.tbtConductance.setEnabled(True)           
            self.tbtResistance.setEnabled(True)           
            self.tbtCapacitance.setEnabled(True)           
            self.tbtElastance.setEnabled(True)           
            self.tbtEvolution_C.setEnabled(False)           
            self.tbtInvolution_C.setEnabled(False)           
            self.tbtEvolution_L.setEnabled(False)           
            self.tbtInvolution_L.setEnabled(False)

            self.fraImpulsion.setToolTip('Nombre d\'entités inductives\n (Variable d\'état)')
            self.fraFlot.setToolTip('Energie-par-entité inductive\n (Variable d\'état)')
            self.fraEffort.setToolTip('Energie-par-entité capacitive\n (Variable d\'état)')
            self.fraQuantiteBase.setToolTip('Nombre d\'entités capacitives\n (Variable d\'état)')
            self.tbtEvolution_L.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.tbtEvolution_C.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.tbtInvolution_L.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')
            self.tbtInvolution_C.setToolTip('Propriété spatiotemporelle \nnon disponible pour un singleton ou un pôle')

           
        else:
            ObjetFormel[n] = 0
            ActiveBoutons(self, False)
            
    def on_tbtDipole_released(self):
        global ObjetFormel
        global NomObjetFormel
        EffacerScene(self)
        source = self.sender()
        if source.isChecked():
            ActiveBoutons(self, False)
            Reset_noms_boutons(self)
            ObjetFormel[n] = 3
            NomObjetFormel = "Dipole"
            self.tbtSingleton_L.setChecked(False)
            self.tbtSingleton_C.setChecked(False)
            self.tbtPole.setChecked(False)
            self.tbtAssemblage.setChecked(False)
            self.tbtEvolution_L.setText('    Evolution')          
            self.tbtEvolution_C.setText('    Evolution')          
            ActiveBoutons(self, True)
            ActiveMenus(self, False)
            
            self.fraImpulsion.setToolTip('Nombre d\'entités inductives\n (Variable d\'état)')
            self.fraFlot.setToolTip('Energie-par-entité inductive\n (Variable d\'état)')
            self.fraEffort.setToolTip('Energie-par-entité capacitive\n (Variable d\'état)')
            self.fraQuantiteBase.setToolTip('Nombre d\'entités capacitives\n (Variable d\'état)')
            self.tbtEvolution_L.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtEvolution_C.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtInvolution_L.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtInvolution_C.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            
            
        else:
            ObjetFormel[n] = 0
            ActiveBoutons(self, False)
            
    def on_tbtAssemblage_released(self):
        global ObjetFormel
        global NomObjetFormel
        EffacerScene(self)
        source = self.sender()
        if source.isChecked():
            ActiveBoutons(self, False)
            Reset_noms_boutons(self)
            ObjetFormel[n] = 4
            NomObjetFormel = "Assemblage"
            self.tbtSingleton_L.setChecked(False)
            self.tbtSingleton_C.setChecked(False)
            self.tbtPole.setChecked(False)
            self.tbtDipole.setChecked(False)
            ActiveBoutons(self, True)
            ActiveMenus(self, False)
            
            self.fraImpulsion.setToolTip('Nombre d\'entités inductives\n (Variable d\'état)')
            self.fraFlot.setToolTip('Energie-par-entité inductive\n (Variable d\'état)')
            self.fraEffort.setToolTip('Energie-par-entité capacitive\n (Variable d\'état)')
            self.fraQuantiteBase.setToolTip('Nombre d\'entités capacitives\n (Variable d\'état)')
            self.tbtEvolution_L.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtEvolution_C.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtInvolution_L.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            self.tbtInvolution_C.setToolTip('Propriété spatiotemporelle \nd\'évolution de l\'énergie')
            
        else:
            ObjetFormel[n] = 0
            self.tbtEvolution_L.setText('    Evolution')          
            self.tbtEvolution_C.setText('    Evolution')          
            ActiveBoutons(self, False)
        
    def on_tbtInductance_released(self):
        source = self.sender()
        if source.isChecked():
            if NomObjetFormel == 'Singleton_C' :
                Capa_Elast_Sing[n] = 1
            else :        
                Indu_Reluctance[n] = 1
            self.tbtReluctance.setChecked(False)
        else:
            if NomObjetFormel == 'Singleton_C' :
                Capa_Elast_Sing[n] = 0
            else :        
                Indu_Reluctance[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtReluctance_released(self):
        source = self.sender()
        if source.isChecked():
            if NomObjetFormel == 'Singleton_C' :
                Capa_Elast_Sing[n] = -1
            else :        
                Indu_Reluctance[n] = -1
            self.tbtInductance.setChecked(False)
        else:
            if NomObjetFormel == 'Singleton_C' :
                Capa_Elast_Sing[n] = 0
            else :        
                Indu_Reluctance[n] = 0
            self.tbtConductance.setChecked(False)
            self.tbtResistance.setChecked(False)
            self.tbtCapacitance.setChecked(False)
            self.tbtElastance.setChecked(False)
        AnalysePossibilites(self)
        DessineGraphe(self)
        #if NomObjetFormel == 'Singleton_C' :

    def on_tbtConductance_released(self):
        source = self.sender()
        if source.isChecked():
            Condu_Resistance[n] = 1
            self.tbtResistance.setChecked(False)
        else:
            Condu_Resistance[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)
            
    def on_tbtResistance_released(self):
        source = self.sender()
        if source.isChecked():
            Condu_Resistance[n] = -1
            self.tbtConductance.setChecked(False)
        else:
            Condu_Resistance[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtCapacitance_released(self):
        source = self.sender()
        if source.isChecked():
            if NomObjetFormel == 'Singleton_L' :
                Indu_Reluct_Sing[n]  = 1
            else :
                Capa_Elastance[n] = 1
            self.tbtElastance.setChecked(False)
        else:
            if NomObjetFormel == 'Singleton_L' :
                Indu_Reluct_Sing[n]  = 0
            else :
                Capa_Elastance[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)
            
    def on_tbtElastance_released(self):
        source = self.sender()
        if source.isChecked():
            if NomObjetFormel == 'Singleton_L' :
                Indu_Reluct_Sing[n]  = -1
            else :
                Capa_Elastance[n] = -1
            self.tbtCapacitance.setChecked(False)
        else:
            if NomObjetFormel == 'Singleton_L' :
                Indu_Reluct_Sing[n]  = 0
            else :
                Capa_Elastance[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtEvolution_C_released(self):
        global Evol_Involution_C
        source = self.sender()
        if source.isChecked():
            if ObjetFormel[n] == 1 : # Singleton
                if NomObjetFormel == 'Singleton_L' :  Multiplication_C[n] = -1 # Singleton inductif
                if NomObjetFormel == 'Singleton_C' :  Multiplication_C[n] = +1 # Singleton capacitif
            else :
                Evol_Involution_C[n] = 1
                self.tbtInvolution_C.setChecked(False)
        else:
            Evol_Involution_C[n] = 0
            Multiplication_C[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtInvolution_C_released(self):
        global Evol_Involution_C
        source = self.sender()
        if source.isChecked():
            Evol_Involution_C[n] = -1
            self.tbtEvolution_C.setChecked(False)
        else:
            Evol_Involution_C[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtEvolution_L_released(self):
        global Evol_Involution_L
        source = self.sender()
        if source.isChecked():
            if ObjetFormel[n] == 1 : # Singleton
                if NomObjetFormel == 'Singleton_L' :  Multiplication_L[n] = -1 # Singleton inductif
                if NomObjetFormel == 'Singleton_C' :  Multiplication_L[n] = +1 # Singleton capacitif
            else :
                Evol_Involution_L[n] = 1
                self.tbtInvolution_L.setChecked(False)
        else:
            Evol_Involution_L[n] = 0
            Multiplication_L[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)

    def on_tbtInvolution_L_released(self):
        global Evol_Involution_L
        source = self.sender()
        if source.isChecked():
            Evol_Involution_L[n] = -1
            self.tbtEvolution_L.setChecked(False)
        else:
            Evol_Involution_L[n] = 0
        AnalysePossibilites(self)
        DessineGraphe(self)
        
    def on_rdoDynamique_toggled(self):   
        global dynamique
        source = self.sender()
        if source.isChecked():
            dynamique =1
        else:
            dynamique =0
        if ObjetFormel[n] >0 :
            DessineGraphe(self)

    def on_actionEffacer_triggered(self):
        msg = 'Effacement ' 
        self.statusBar().showMessage(msg)
        EffacerScene(self)
        ActiveBoutons(self, False)
        self.tbtSingleton_L.setChecked(False)
        self.tbtSingleton_C.setChecked(False)
        self.tbtPole.setChecked(False)
        self.tbtDipole.setChecked(False)
        self.tbtAssemblage.setChecked(False)
        
        global Texte_Fenetre
        print(Texte_Fenetre)

    def on_chkEcrireOperateurs_toggled(self):
        global operateurs
        source = self.sender()
        operateurs = source.isChecked()
        if ObjetFormel[n] >0 :  DessineGraphe(self)
    
    def on_chkOperateurs_lin_toggled(self):
        global linearite
        source = self.sender()
        linearite = source.isChecked()
        if ObjetFormel[n] >0 :  DessineGraphe(self)
    
    def on_chkEcrireLegendes_toggled(self):
        global legendes
        source = self.sender()
        legendes =source.isChecked()
        if ObjetFormel[n] >0 :  DessineGraphe(self)
 
    def on_chkEcrireUnitesSI_toggled(self):
        global unites
        source = self.sender()
        if source.isChecked() : # SI demandé
            if unites == 1 :  # SI_J seul
                unites = 3   # SI_J  + SI
            else :  #  SI_J  + SI
                unites = 2 # SI seul
        else : # SI non demandé
            if unites == 3 : #  SI_J  + SI
                unites = 1  # SI_J seul
            else : #   SI seul
                unites = 0
        if ObjetFormel[n] >0 : DessineGraphe(self)
        
    def on_chkEcrireUnitesSI_J_toggled(self):
        global unites
        source = self.sender()
        if source.isChecked() : # SI_J demandé
            if unites == 2 :  # SI seul
                unites = 3   # SI_J  + SI
            else :  #  SI_J  + SI
                unites = 1 # SI_J seul
        else : # SI_J non demandé
            if unites == 3 : #  SI_J  + SI
                unites = 2  # SI seul
            else : #   SI_J seul
                unites = 0
        if ObjetFormel[n] >0 : DessineGraphe(self)
        
    def on_spnTailleSymb_valueChanged(self):
        global taille_symbole
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        taille_symbole = self.spnTailleSymb.value()
        if graphe_existe and ObjetFormel[n] >0 : DessineGraphe(self)
        
    def on_actionFrancais_triggered(self): 
        global Langue
        Langue = 'F' 
        self.actionFrancais.setChecked(True)
        self.actionEnglish.setChecked(False)
        remplissage_labels(self)
        if ObjetFormel[n] >0 : DessineGraphe(self)

    def on_actionEnglish_triggered(self): 
        global Langue
        Langue = 'E'
        self.actionFrancais.setChecked(False)
        self.actionEnglish.setChecked(True)
        remplissage_labels(self)
        if ObjetFormel[n] >0 : DessineGraphe(self)

    def on_actionPleinEcran_triggered(self):
        # self = __main__.MainWindow
        global Plein_ecran
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if Plein_ecran == True :
            self.showMaximized()
            Plein_ecran = False 
        else :
            self.showFullScreen()
            Plein_ecran = True
    
    def on_actionMode_d_emploi_triggered(self):
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        #print("Notice demandée")
        self.popUp = Fenetre_Notice()
        self.popUp.showMaximized() 
        #self.popUp.setGeometry(300, 200, 300, 200)
        self.popUp.show()
        
    def on_actionA_propos_triggered(self):
        global Titre_Fenetre
        global Texte_Fenetre
        global Fichier_signification
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        #print("actionA_propos")
        Texte_Fenetre = ''
        Fichier_signification = 'Docs/Dessinateur_GF_A_propos.htm'
        Titre_Fenetre = "Fiche d'identité du programme"
        self.popUp = Fenetre_Afficheur()
        self.popUp.setGeometry(100, 100, 650, 350)
        self.popUp.show()


    def on_actionVisionneuse_SVG_triggered(self):
        global compteur_appel
        global Code_SVG
        global Liste_elements
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if Code_SVG == '' :
            Code_SVG = Description_SVG(self, Liste_elements) 
        self.popUp = Fenetre_SVG() 
        self.popUp.setGeometry(300, 200, 600, 600)
        self.popUp.show()

    def on_actionCarte_des_vari_t_s_d_nergie_triggered(self):
        global pixmap
        global Dimension_bitmap
        global titre_bitmap
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        pixmap = QPixmap('Docs/G_Energy_Couplings_18_var.png') 
        dimensions =  Dimension_bitmap.split('x')
        pixmap = pixmap.scaledToWidth(int(dimensions[0]))
        titre_bitmap = 'Carte des relations entre 18 variétés d\'énergie'
        self.popUp = Fenetre_Bitmap() 
        self.popUp.setGeometry(10, 30, int(dimensions[0]), int(dimensions[1]))
        self.popUp.show()

    def on_actionVisionneuse_Bitmap_triggered(self):
        global pixmap
        global Dimension_bitmap
        global titre_bitmap
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        vue_alt = QGraphicsView(self.centralwidget) # 
        vue_alt.setScene(self.scene)  # Attribue la scène à la vue
        vue_alt.setRenderHints(QPainter.Antialiasing)
        dimensions =  Dimension_bitmap.split('x')
        vue_alt.resize(int(dimensions[0]), int(dimensions[1]))
        vue_alt.scale(int(dimensions[0])/300.,int(dimensions[0])/300.)
        vue_alt.hide()
        pixmap = vue_alt.grab() 
        vue_alt.destroy(destroyWindow = True)
        titre_bitmap ='Graphe en format Bitmap :'
        self.popUp = Fenetre_Bitmap() 
        self.popUp.setGeometry(10, 30, int(dimensions[0]), int(dimensions[1]))
        self.popUp.show()
        
    def on_actionCode_SVG_triggered(self):     # Fichier TEX
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Variete
        global Repertoire
        global Texte_Fenetre
        global Titre_Fenetre
        global Fichier_signification
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
            return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        try :
            Code_SVG =""
            Code_SVG = Description_SVG(self, Liste_elements) 
        except :
            QMessageBox.information(self, "Erreur",  "Le code SVG n'a pas pu être construit !")
        if len(Code_SVG) > 0 :
            Fichier_signification = ''
            Texte_Fenetre = Code_SVG
            Titre_Fenetre = "Code SVG"
            self.popUp = Fenetre_Afficheur()
            self.popUp.setGeometry(100, 30, 800, 850)
            self.popUp.show()

    def on_actionCode_XML_structure_triggered(self):     # Fichier TEX
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Variete
        global Repertoire
        global Texte_Fenetre
        global Titre_Fenetre
        global Fichier_signification
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Code_XML =""
        Code_XML = Description_XML(self, Liste_elements, False) # Structure seulement
        if len(Code_XML) > 0 :
            Fichier_signification = ''
            Texte_Fenetre = Code_XML
            Titre_Fenetre = "Code XML"
            self.popUp = Fenetre_Afficheur()
            self.popUp.setGeometry(100, 30, 800, 850)
            self.popUp.show()

    def on_actionCode_XML_description_triggered(self):     # Fichier TEX
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Variete
        global Repertoire
        global Texte_Fenetre
        global Titre_Fenetre
        global Fichier_signification
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Code_XML =""
        Code_XML = Description_XML(self, Liste_elements, True) # Description
        if len(Code_XML) > 0 :
            Fichier_signification = ''
            Texte_Fenetre = Code_XML
            Titre_Fenetre = "Code XML"
            self.popUp = Fenetre_Afficheur()
            self.popUp.setGeometry(100, 30, 800, 850)
            self.popUp.show()

        
    def on_actionCode_LaTeX_script_triggered(self):     # Fichier TEX
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Variete
        global Repertoire
        global Texte_Fenetre
        global Titre_Fenetre
        global Fichier_signification
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Code_LaTex =""
        Code_LaTex = Description_LaTex(self, Liste_elements)
        if len(Code_LaTex) > 0 :
            Fichier_signification = ''
            Texte_Fenetre = Code_LaTex
            Titre_Fenetre = "Code LaTeX"
            self.popUp = Fenetre_Afficheur()
            self.popUp.setGeometry(100, 100, 650, 500)
            self.popUp.show()

    def on_btnSignification_released(self):
        global Texte_Fenetre
        global Titre_Fenetre
        global Fichier_signification
        global Signification_on
        if Signification_on  :
           Signification_on = False 
           self.popUp.hide()
        else :
            Signification_on = True   
            Fichier_signification = 'Docs/Dessinateur_GF_Signif_' + Variete[n] +'.htm'
            Texte_Fenetre = ''
            Titre_Fenetre = "Signification physique"
            self.popUp = Fenetre_Afficheur()
            #self.popUp.showMaximized() 
            self.popUp.setGeometry(300, 30, 800, 800)
            self.popUp.show()

    def keyPressEvent(self, keyEvent):
        # self = MainWindow
        global Plein_ecran
        key = keyEvent.key() 
        if key == Qt.Key_F11 :
            if Plein_ecran :
                self.showMaximized()
                Plein_ecran = False
            else :
                self.showFullScreen()
                Plein_ecran = True
        
        elif key == Qt.Key_Escape :  
            self.showNormal()

    def on_actionEnregistrer_Graphe_en_SVG_triggered(self): #§ Enregistrer Graphe en SVG
        global Texte_Fenetre
        global Code_SVG
        global can_exit
        global graphe_existe
        global Liste_elements
        global NomObjetFormel
        global ObjetFormel
        global Enum_proprietes
        global Vecteur 
        global Variete
        global Repertoire
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.information(self, "Problème",  "Il n'y a aucun graphe à enregistrer !")
            return
        P = Dimension_bitmap.find('x')
        Enum_proprietes = Enumere_proprietes(self) + Dimension_bitmap[P+1:]
        nomFichier = Repertoire +NomObjetFormel +'_'+Variete[n] +'_' + Enum_proprietes + '.svg'
        (nomFichier,  filtre) = QFileDialog.getSaveFileName(self, \
        "Sauvegarder dans un fichier graphique vectoriel",  nomFichier, filter ="SVG (*.svg)")
        if nomFichier :
            if '.svg' in nomFichier.lower() :
                try :
                    Code_SVG =""
                    P = Texte_Fenetre.find('<svg')
                    if P > 1 and P < 50 : 
                        Code_SVG = Texte_Fenetre
                    if len(Code_SVG) == 0 : 
                        Code_SVG = Description_SVG(self, Liste_elements) 
                    if len(Code_SVG) > 0 :
                        file = open(nomFichier, 'w', encoding="UTF-8")  
                        file.write(Code_SVG) 
                        file.close() 
                        can_exit = True
                        self.statusBar().showMessage('Fichier SVG enregistré dans ' + nomFichier + " Nb de caractères =" + str(len(Code_SVG)))
                        #print ("Code SVG enrégistré dans", nomFichier, " Longueur =", len(Code_SVG))
                        P = nomFichier.rfind('/')
                        if P >= 0 :
                            Repertoire = nomFichier[:P+1]
                except :
                    QMessageBox.information(self, "Problème",  "Le fichier n'a pas pu être enregistré !")

    def on_actionEnregistrer_Graphe_en_bitmap_triggered(self):
        global graphe_existe
        global can_exit
        global Dimension_bitmap
        global Repertoire
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à enregistrer !")
            return
        Enum_proprietes = Enumere_proprietes(self)
        if Langue =='F' : Enum_proprietes += '_F'
        nomFichier = Repertoire + NomObjetFormel +'_'+Variete[n] +'_' + Enum_proprietes + '_' + Dimension_bitmap
        (nomFichier,  filtre) = QFileDialog.getSaveFileName(self, \
        "Sauvegarder dans un fichier image bitmap (" + Dimension_bitmap + ")",  nomFichier,  filter ="PNG (*.png);;JPEG (*.jpg);;BITMAP (*.bmp)")
        if nomFichier :
            P = nomFichier.rfind('/')
            if P >= 0 :
                Repertoire = nomFichier[:P+1]
            Code_XML =""
            Code_XML = Description_XML(self, Liste_elements, True) # decrire_details = True
            
            vue_alt = QGraphicsView(self.centralwidget) # 
            vue_alt.setScene(self.scene)  # Attribue la scène à la vue
            vue_alt.setRenderHints(QPainter.Antialiasing)
            P1 = Dimension_bitmap.find('x')
            largeur = float(Dimension_bitmap[0:P1] )
            hauteur = float(Dimension_bitmap[P1+1:])
            echelle = hauteur / 200.
            vue_alt.resize(largeur, hauteur)
            vue_alt.scale(echelle, echelle)
            vue_alt.hide()
            pixmap = vue_alt.grab() 
            vue_alt.destroy(destroyWindow = True)
            
            taille = 0
            if nomFichier.find('.jpg') > 0 :
                taille = Enregistrement_Texte_JPEG(self, nomFichier, pixmap,  Code_XML)
            if nomFichier.find('.png') > 0 :
                taille = Enregistrement_Texte_PNG(self,  nomFichier, pixmap,  Code_XML)
            if nomFichier.find('.bmp') > 0 :
                taille = Enregistrement_Texte_BMP(self,  nomFichier, pixmap,  Code_XML)
            if taille > 0 :    
                self.statusBar().showMessage('Fichier bitmap (' + Dimension_bitmap + ') enregistré dans ' + nomFichier)
                can_exit = True    

    def on_actionCopie_dans_Presse_Papier_triggered(self):
        global graphe_existe
        global vue
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.information(self, "Problème",  "Il n'y a aucun graphe à copier !")
            return
        
        vue_alt = QGraphicsView(self.centralwidget) # 
        vue_alt.setScene(self.scene)  # Attribue la scène à la vue
        vue_alt.setRenderHints(QPainter.Antialiasing)
        P1 = Dimension_bitmap.find('x')
        largeur = float(Dimension_bitmap[0:P1] )
        hauteur = float(Dimension_bitmap[P1+1:])
        echelle = hauteur / 200.
        vue_alt.resize(largeur, hauteur)
        vue_alt.scale(echelle, echelle)
        vue_alt.hide()
        pixmap = vue_alt.grab() 
        vue_alt.destroy(destroyWindow = True)
        
        presse_papier = QApplication.clipboard()
        presse_papier.clear(mode=presse_papier.Clipboard )
        presse_papier.setPixmap(pixmap, mode=presse_papier.Clipboard)
        self.statusBar().showMessage('Graphe copié dans le Presse_Papier (en bitmap ' + Dimension_bitmap + ')')
        
    def on_actionEnregistrer_description_en_XML_triggered(self): # Fichier XML
        global Texte_Fenetre
        global can_exit
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Enum_proprietes
        global Variete
        global Repertoire
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Enum_proprietes = Enumere_proprietes(self)
        if Langue =='F' : Enum_proprietes += '_F'
        nomFichier = Repertoire +NomObjetFormel +'_'+Variete[n] +'_' + Enum_proprietes + '.xml'
        (nomFichier,  filtre) = QFileDialog.getSaveFileName(self, \
        "Sauvegarder dans un fichier de description XML",  nomFichier, filter ="XML (*.xml)")
        if nomFichier :
            decrire_details = True
            if '.xml' in nomFichier.lower() :
                Code_XML =""
                P = Texte_Fenetre.find('<xml')
                if P > 1 and P<50 : 
                    if Texte_Fenetre.find('<font>') > P and Texte_Fenetre.find('<size>') > P :
                        Code_XML = Texte_Fenetre
                if len(Code_XML) == 0 : 
                    Code_XML = Description_XML(self, Liste_elements, decrire_details)
                if len(Code_XML) > 0 :
                    file = open(nomFichier, 'w', encoding="UTF-8") 
                    file.write(Code_XML) 
                    file.close()
                    can_exit= True        
                    self.statusBar().showMessage('Fichier XML de description enregistré dans ' + nomFichier + " Nb de caractères =" + str(len(Code_XML)))
                    #print ("Code XML enrégistré dans", nomFichier, " Longueur =", len(Code_XML))
                    P = nomFichier.rfind('/')
                    if P >= 0 :
                        Repertoire = nomFichier[:P+1]

    def on_actionEnregistrer_structure_en_XML_triggered(self): # Fichier XML
        global Texte_Fenetre
        global can_exit
        global graphe_existe
        global NomObjetFormel
        global ObjetFormel
        global Enum_proprietes
        global Variete
        global Repertoire
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Enum_proprietes = Enumere_proprietes(self) + '_struct'
        nomFichier = Repertoire +NomObjetFormel +'_'+Variete[n] +'_' + Enum_proprietes + '.xml'
        (nomFichier,  filtre) = QFileDialog.getSaveFileName(self, \
        "Sauvegarder dans un fichier de structure XML",  nomFichier, filter ="XML (*.xml)")
        #print(Liste_elements)
        if nomFichier :
            decrire_details = False
            if '.xml' in nomFichier.lower() :
                Code_XML =""
                P = Texte_Fenetre.find('<xml')
                if P > 1 and P<50 : 
                    if Texte_Fenetre.find('<font>') <0 and Texte_Fenetre.find('<size>') <0 : 
                        Code_XML = Texte_Fenetre
                if len(Code_XML) == 0 : 
                    Code_XML = Description_XML(self, Liste_elements, decrire_details)
                if len(Code_XML) > 0 :
                    file = open(nomFichier, 'w', encoding="UTF-8") 
                    file.write(Code_XML) 
                    file.close()
                    can_exit = True
                    self.statusBar().showMessage('Fichier XML de structure enregistré dans ' + nomFichier + " Nb de caractères =" + str(len(Code_XML)))
                    #print ("Code XML enrégistré dans", nomFichier, " Longueur =", len(Code_XML))
                    P = nomFichier.rfind('/')
                    if P >= 0 :
                        Repertoire = nomFichier[:P+1]

    def on_actionEnregistrer_structure_en_LaTex_triggered(self):     # Fichier TEX
        global Texte_Fenetre
        global graphe_existe
        global can_exit
        global NomObjetFormel
        global ObjetFormel
        global Enum_proprietes
        global Variete
        global Repertoire
        global compteur_appel
        compteur_appel += 1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if not graphe_existe :
            QMessageBox.warning(self, "Problème",  "Il n'y a aucun graphe à traiter !")
            return
        Enum_proprietes = Enumere_proprietes(self)
        if Langue =='F' : Enum_proprietes += '_F'
        nomFichier = Repertoire +NomObjetFormel +'_'+Variete[n] +'_' + Enum_proprietes + '.tex'
        (nomFichier,  filtre) = QFileDialog.getSaveFileName(self, \
        "Sauvegarder dans un fichier de structure en LaTeX",  nomFichier, filter ="TEX (*.tex)")
        if nomFichier :
            if '.tex' in nomFichier.lower() :
                Code_LaTex =""
                if Texte_Fenetre.find('LaTeX') == 1 : 
                    Code_LaTex = Texte_Fenetre
                if len(Code_LaTex) == 0 : 
                    Code_LaTex = Description_LaTex(self, Liste_elements)
                if len(Code_LaTex) > 0 :
                    file = open(nomFichier, 'w', encoding="UTF-8") 
                    file.write(Code_LaTex) 
                    file.close() 
                    can_exit = True
                    self.statusBar().showMessage('Fichier LaTeX enregistré dans ' + nomFichier + " Nb de caractères =" + str(len(Code_LaTex)))
                    #print ("Code LaTeX enrégistré dans", nomFichier, " Longueur =", len(Code_LaTex))
                    P = nomFichier.rfind('/')
                    if P >= 0 :
                        Repertoire = nomFichier[:P+1]
                        
    def on_actionOuvrir_Graphe_en_SVG_triggered(self): # Fichier SVG
        global Repertoire
        global compteur_appel
        global Code_SVG
        global Variete
        global NomObjetFormel
        global ObjetFormel
        global Enum_proprietes
        global Liste_elements
        global Capa_Elastance
        global Indu_Reluctance
        global Condu_Resistance
        global Evol_Involution_C
        global Evol_Involution_L
        global graphe_existe
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if len(Repertoire) == 0 :
            Repertoire = os.getcwd()
        nomFichier = '' 
        (nomFichier,  filtre) = QFileDialog.getOpenFileName(self, \
        "Lire un fichier de graphe SVG",  Repertoire , filter ="SVG (*.svg)")
        if len(nomFichier) == 0 :
            return
        P = nomFichier.rfind('/')
        if P >= 0 :
            Repertoire = nomFichier[:P+1]
        Code_SVG = open(nomFichier).read() # Utilisé  pour la visionneuse mais pas pour construire le graphe
        Nouvelle_Variete = Lecture_SVG(self,  nomFichier)
        if NomObjetFormel == '':
            QMessageBox.warning(self, "Représentation du graphe impossible",  "Le fichier ne contient pas de nom d'Objet Formel ! ")
            return
        message = Analyse_Liste_Elements(self, Liste_elements)
        if len(message) > 0 :
            QMessageBox.warning(self, "Lecture du graphe impossible",  message)
            return
        self.statusBar().showMessage('Graphe provenant du fichier ' + nomFichier )
        graphe_existe = True
        if Nouvelle_Variete == Variete[n] :
            self.scene.clear()
            self.initialiserScene()    
            DessineGraphe(self)
        else :
            index = 0
            for paire in self.liste_varietes : #  paire (nom variété, cle)
                if Nouvelle_Variete == paire[1] :
                    index = self.cb.findText(paire[0])
            self.cb.setCurrentIndex ( index) # Provoque le tracé du graphe                     
    
    def on_actionOuvrir_description_XML_de_Graphe_triggered(self): # Fichier XML
        global Repertoire
        global Variete
        global NomObjetFormel
        global ObjetFormel
        global Liste_elements
        global graphe_existe
        global compteur_appel
        global autorise_constr_dicos
        global coul_elem
        
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if len(Repertoire) == 0 :
            Repertoire = os.getcwd()
        nomFichier = '' 
        (nomFichier,  filtre) = QFileDialog.getOpenFileName(self, \
        "Lire un fichier de description XML",  Repertoire , filter ="XML (*.xml)")
        if len(nomFichier) == 0 :
            return
        P = nomFichier.rfind('/')
        if P >= 0 :
            Repertoire = nomFichier[:P+1]
        Nouvelle_Variete = Lecture_XML(self, nomFichier, '') # Pas d'argument texte
        #print(Nouvelle_Variete,  Liste_elements)
        message = Analyse_Liste_Elements(self, Liste_elements)
        if len(message) > 0 :
            QMessageBox.warning(self, "Lecture du graphe impossible",  message)
        else :
            self.statusBar().showMessage('Graphe provenant du fichier ' + nomFichier )
            #print(var[n]['Symb_q'],  var[n]['Symb_e'], var[n]['Symb_p'], var[n]['Symb_f'])
            #print(ind[n]['Symb_q'],  ind[n]['Symb_e'], ind[n]['Symb_p'], ind[n]['Symb_f'])
            
            graphe_existe = True
            if Nouvelle_Variete == Variete[n] :
                self.scene.clear()
                self.initialiserScene()    
                DessineGraphe(self)
            else :
                index = 0
                for paire in self.liste_varietes : #  paire (nom variété, cle)
                    if Nouvelle_Variete == paire[1] :
                        index = self.cb.findText(paire[0])
                autorise_constr_dicos = False
                self.cb.setCurrentIndex (index) # Provoque le tracé du graphe
                autorise_constr_dicos = True

    def on_actionOuvrir_description_en_bitmap_triggered(self): # Fichier XML
        #imports : Qt, QFileDialog, QPixmap, QGraphicsPixmapItem, QSize
        global Repertoire
        global Variete
        global NomObjetFormel
        global ObjetFormel
        global Liste_elements
        global graphe_existe
        global compteur_appel
        global autorise_constr_dicos
        global coul_elem
        global vue # = ce qui est affiché
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        if len(Repertoire) == 0 :
            Repertoire = os.getcwd()
        nomFichier = '' 
        (nomFichier,  filtre) = QFileDialog.getOpenFileName(self, \
        "Lire un fichier bitmap",  Repertoire , filter ="Bitmap (*.png *.jpg *.bmp);;JPG (*.jpg);;PNG (*.png);;Windows BMP (*.bmp);;Tous (*.*)")
        if len(nomFichier) == 0 :
            return
        P = nomFichier.rfind('/')
        if P >= 0 :
            Repertoire = nomFichier[:P+1]
        texte = Extraction_Commentaire(self, nomFichier)
        if len(texte) == 0 :
            message = "Le fichier \"" + nomFichier + "\" ne contient pas de description XML."
            QMessageBox.warning(self,  "Lecture du graphe impossible",  message)
            return
        else :
            self.statusBar().showMessage('Fichier lu =' + nomFichier + " avec texte incorporé")
        Nouvelle_Variete = Lecture_XML(self, '', texte) # Analyse directe de texte
        #print(Nouvelle_Variete,  Liste_elements)
        message = Analyse_Liste_Elements(self, Liste_elements)
        if len(message) > 0 :
            QMessageBox.warning(self, "Lecture du graphe impossible",  message)
        else :
            self.statusBar().showMessage('Graphe provenant du fichier ' + nomFichier )
            graphe_existe = True
            if Nouvelle_Variete == Variete[n] :
                self.scene.clear()
                self.initialiserScene()    
                DessineGraphe(self)
            else :
                index = 0
                for paire in self.liste_varietes : #  paire (nom variété, cle)
                    if Nouvelle_Variete == paire[1] :
                        index = self.cb.findText(paire[0])
                autorise_constr_dicos = False
                self.cb.setCurrentIndex (index) # Provoque le tracé du graphe
                autorise_constr_dicos = True
    
    def on_actionQuitter_triggered(self):
        global compteur_appel
        compteur_appel +=1
        if compteur_appel == 1 : # Pour éviter le double appel
             return
        compteur_appel = 0 
        Reponse = QMessageBox.question(self, "Demande de confirmation", \
        "Voulez-vous vraiment quitter ? ", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if Reponse == QMessageBox.Yes:
            sys.exit(app.exec_())
        if Reponse == QMessageBox.No:
            return

class Fenetre_Bitmap(QMainWindow):
    def __init__(self ):
        global pixmap
        global titre_bitmap
        super().__init__()
        layout = QHBoxLayout()
        Afficheur = QLabel()
        Afficheur.setPixmap(pixmap)
        layout.addWidget(Afficheur)
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        texte = ' (dimensions ' + str(pixmap.width() ) +'x' + str(pixmap.height() ) + ' pixels)' 
        self.setWindowTitle(titre_bitmap + texte)


class Fenetre_SVG( QMainWindow):
    def __init__(self ):
        global Code_SVG
        super().__init__()
        layout = QHBoxLayout()
        Afficheur = QSvgWidget()
        ba = QByteArray()
        try :
            ba.append(Code_SVG)
            Afficheur.load(ba)
            layout.addWidget(Afficheur)
            self.widget = QWidget()
            self.widget.setLayout(layout)
            self.setCentralWidget(self.widget)
            balise = '<title>' # Balise de titre
            if balise in Code_SVG : 
                P1 = Code_SVG.find(balise)
                P2 = Code_SVG.find('</' + balise[1:])
                ligne =  Code_SVG[P1+7: P2]
                self.setWindowTitle("SVG :" + ligne)
        except :
            QMessageBox.warning(self, "Problème",  "Restitution impossible par la visionneuse SVG")


class Fenetre_Notice(QMainWindow):
    def __init__(self):
        super().__init__()
        try :
            nomFichier = 'Docs/Dessinateur_Graphe_Formel_Notice.htm'
            source = open(nomFichier, 'r', encoding = "=utf-8")
        except :
            QMessageBox.warning(self,'Introuvable',  'Le fichier ' + nomFichier + ' est absent !')
        else :
            layout = QHBoxLayout()
            Afficheur = QTextEdit()
            Afficheur.setHtml (source.read())
            Afficheur.setReadOnly(True)
            police = QFont('Helvetica', 12)  
            Afficheur.setFont(police)
            Afficheur.setDocumentTitle("Mode d'emploi")        
            layout.addWidget(Afficheur)
            self.widget = QWidget()
            self.widget.setLayout(layout)
            self.setCentralWidget(self.widget)
            self.setWindowTitle("Mode d'emploi")

class Fenetre_Afficheur(QMainWindow):
    def __init__(self):
        global Titre_Fenetre
        global Texte_Fenetre
        global Fichier_signification
        super().__init__()
        layout = QHBoxLayout()
        Afficheur = QTextEdit(self)
        if len(Fichier_signification) > 0 :
            try :
                source = open(Fichier_signification, 'r', encoding = "=utf-8")
                Texte_Fenetre = source.read()
            except :
                QMessageBox.warning(self,'Introuvable',  'Le fichier ' + Fichier_signification + ' est absent !')
            else :
                Afficheur.setHtml (Texte_Fenetre)
                Afficheur.setReadOnly(True)
        else :    
            Afficheur.setPlainText(Texte_Fenetre)
            Afficheur.setReadOnly(False)
        police = QFont()
        police.setPointSize(12)
        Afficheur.setFont(police)
        Afficheur.setDocumentTitle(Titre_Fenetre)
        Afficheur.textChanged.connect(lambda: self.text_modifie(Afficheur.document().toPlainText()))
        layout.addWidget(Afficheur)
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        self.setWindowTitle(Titre_Fenetre)
        
    def  text_modifie(self, Texte):  
        global Texte_Fenetre
        Texte_Fenetre = Texte
        print('----------------------------changement du code !')
        return
        
def Analyse_Liste_Elements(self,  Liste_elements):    
    # Traduit la Liste_Elements en valeurs des propriétés orientées
    # Positionne les boutons d'objet formel selon le contenu de la chaîne NomObjetFormel
    global ObjetFormel
    global NomObjetFormel
    global Enum_proprietes
    global Capa_Elastance
    global Indu_Reluctance
    global  Condu_Resistance
    global Evol_Involution_C
    global Evol_Involution_L
    message = ''
    Capa_Elastance[n] = 0
    Indu_Reluctance[n] = 0
    Condu_Resistance[n] = 0
    Evol_Involution_C[n] = 0
    Evol_Involution_L[n] = 0
    Indu_Reluct_Sing[n] = 0
    Capa_Elast_Sing[n] = 0
    Multiplication_C[n] = 0
    Multiplication_L[n] = 0
    if len(Liste_elements) :
        for item in Liste_elements :
            Prefix_elem = item [:4]
            Element = item [4:]
            if Element == 'O_Capacitance':
                Capa_Elastance[n] =  1
            elif Element == 'O_Elastance':
                Capa_Elastance[n] =  -1
            elif Element == 'O_Inductance':
                Indu_Reluctance[n] = 1
            elif Element == 'O_Reluctance':
                Indu_Reluctance[n] = -1
            elif Element == 'O_Conductance' :
                Condu_Resistance[n] = 1
            elif Element == 'O_Resistance':
                Condu_Resistance[n] = -1
            elif Element == 'O_Evolution_C':
                Evol_Involution_C[n] = 1
            elif Element == 'O_Involution_C':
                Evol_Involution_C[n] = -1
            elif Element == 'O_Evolution_L':
                Evol_Involution_L[n] = 1
            elif Element == 'O_Involution_L':
                Evol_Involution_L[n] = -1
                Enum_proprietes += Suffix_prop['Involution']
            elif Element == 'O_Croix_C':
                Capa_Elastance[n] =  1
            elif Element == 'O_Croix_L':
                Capa_Elast_Sing[n] = 1
            elif Element == 'O_Multiplication_C':  
                if NomObjetFormel == 'Singleton_L' :  Multiplication_C[n] = -1 # Singleton inductif
                if NomObjetFormel == 'Singleton_C' :  Multiplication_C[n] = +1 # Singleton capacitif
            elif Element == 'O_Multiplication_L': 
                if NomObjetFormel == 'Singleton_L' :  Multiplication_L[n] = -1 # Singleton inductif
                if NomObjetFormel == 'Singleton_C' :  Multiplication_L[n] = +1 # Singleton capacitif
            elif Element == 'O_Inductance_Singuliere':
                Indu_Reluct_Sing[n] = 1 
            elif Element == 'O_Reluctance_Singuliere':
                Indu_Reluct_Sing[n] = -1 

    Enum_proprietes = Enumere_proprietes(self)  # Récupère la chaîne "Enum_proprietes" à partir de la liste "Liste_elements"

    if len(Enum_proprietes) > 0 :
        if len(NomObjetFormel) :
            if NomObjetFormel.find('_') >= 0 :
                morceau =NomObjetFormel.split('_')
                NomObjetFormel = morceau[0].capitalize() + '_' + morceau[1].capitalize()
            else :
                NomObjetFormel = NomObjetFormel.capitalize()
            Liste_ObjetsFormels = ['Singleton_L', 'Singleton_C', 'Pole',  'Dipole',  'Assemblage']
            try :
                position = Liste_ObjetsFormels.index(NomObjetFormel) 
            except ValueError :
                NomObjetFormel = ''
                ObjetFormel[n] = 0
            else : 
                ObjetFormel[n] = position  
                if NomObjetFormel == 'Singleton_L' : ObjetFormel[n] = 1
                self.tbtSingleton_L.setChecked(False)
                self.tbtSingleton_C.setChecked(False)
                self.tbtPole.setChecked(False)
                self.tbtDipole.setChecked(False)
                self.tbtAssemblage.setChecked(False)
                if NomObjetFormel == 'Singleton_L'  : self.tbtSingleton_L.setChecked(True)
                if NomObjetFormel == 'Singleton_C'  : self.tbtSingleton_C.setChecked(True)
                if NomObjetFormel == 'Pole'  : self.tbtPole.setChecked(True)
                if NomObjetFormel == 'Dipole'  : self.tbtDipole.setChecked(True)
                if NomObjetFormel == 'Assemblage'  : self.tbtAssemblage.setChecked(True)
        else : 
                    message = 'Aucun Objet Formel n\'a pu être identifié dans le fichier'
    else :
        message = 'Aucune propriété ou noeud n\'a pu être identifié dans le fichier'
    #print ("NomObjetFormel =",  NomObjetFormel,  "Numero =", ObjetFormel[n] )
    return message
    
#---------------------------------Traduction du graphe en SVG---------------------------------------------       

def Lecture_SVG(self,  nomFichier):    
    global NomObjetFormel
    global Liste_elements
    global dynamique
    Prefix_elem = '0000'  # A modifier !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    source = open(nomFichier, 'r')
    compteur = 0
    #print("--------------------------------------------------")
    ligne= source.readline()
    ligne = ligne.strip() # Enlève espaces aux extrémités et le \n
    if ligne.find('<?xml') != 0 :
        QMessageBox.information(self, "Décodage impossible",  "Le fichier n'est pas structuré XML/SVG !")
        return
    compteur += 1 
    ligne= source.readline()
    ligne = ligne.strip() # Enlève espaces aux extrémités et le \n
    if ligne.find('<svg') != 0 :
        QMessageBox.information(self, "Décodage impossible",  "Le fichier n'est pas un fichier SVG !")
        return
    compteur += 1 
    #print('ligne ', compteur,  ' = ', ligne)
    while 1: # Saut par dessus les lignes de définition
        ligne= source.readline()
        ligne = ligne.strip() # Enlève espaces aux extrémités et le \n
        compteur += 1 
        #print('ligne ', compteur,  ' = ', ligne)
        if len(ligne) == 0 or ligne == '</defs>':
            break
    Variete_trouvee = "Generique"
    Liste_elements =[]
    while 1:
        ligne= source.readline()
        ligne = ligne.strip() # Enlève espaces aux extrémités et le \n
        #print('ligne ', compteur,  ' = ', ligne)
        if ligne == '</svg>':
            break
        compteur += 1 
        balise = '<title'
        if balise in ligne : # Ligne de description libre pouvant comporter  la varieté et l'objet, utile si une balise <desc> est absente
            ligne = ligne.strip(' ').lstrip(balise)  # Débarasse la ligne de la 1ere balise
            titre = ligne[:ligne.find('</' + balise[1:])] # Débarasse la ligne de la 2ème balise
            #print("titre =",  titre) 
        balise = '<desc>' # Balise de description
        if balise in ligne : # Ligne comportant des affectations (varieté, objet, ...)
            ligne = ligne.strip(' ').lstrip(balise)  # Débarasse la ligne de la 1ere balise
            ligne = ligne[:ligne.find('</' + balise[1:])] # Débarasse la ligne de la 2ème balise
            #print('ligne =', ligne)
            for affectation in ligne.split( ',') : # Liste d'affectations
                if  affectation.split('=')[0]. strip().lower() == "variete" : Variete_trouvee =  affectation.split('=')[1]. strip()
                if  affectation.split('=')[0]. strip().lower() == "objet" : NomObjetFormel =  affectation.split('=')[1]. strip()
            Variete_trouvee = Variete_trouvee.capitalize()
            NomObjetFormel = NomObjetFormel.replace('ô', 'o').capitalize()
        balise = '<g id=\"'
        P = ligne.find(balise)
        if P >= 0 : # Ligne comportant un identifiant d'élément
            ligne = ligne[P + len(balise):] # Débarasse la ligne de la premiere balise
            P = ligne.find('>' )
            if P >0 : 
                P = ligne[:P].find('"' )
                etiquette =  ligne[:P].strip()
                Element = DecodeSVG(self, etiquette)
                #print ("etiquette =",  etiquette,  "Element =", Element)
                if len(Element) == 0 :
                    QMessageBox.information(self, "Décodage incomplet",  "L'étiquette \"" + etiquette + "\" n'est pas reconnue !")
                else :
                    Liste_elements.append(Prefix_elem + Element) 
    if NomObjetFormel == '' :       # Recherche du nom de l'objet formel si absent de la description
        texte = titre.lower()
        if ' singleton ' in texte : 
            if "capacitif"  in texte : NomObjetFormel = 'Singleton_C'
            if "inductif"  in texte : NomObjetFormel = 'Singleton_L'
        if ' pole ' in texte : NomObjetFormel = 'Pole'
        if ' pôle ' in texte : NomObjetFormel = 'Pole'
        if ' dipole ' in texte : NomObjetFormel = 'Dipole'
        if ' dipôle ' in texte : NomObjetFormel = 'Dipole'
        if ' assemblage ' in texte : NomObjetFormel = 'Assemblage'
    if NomObjetFormel == 'Singleton_c' :  NomObjetFormel = 'Singleton_C'     
    if NomObjetFormel == 'Singleton_l' :  NomObjetFormel = 'Singleton_L'     
    #print("Variete =",  Variete_trouvee) 
    #print("Objet =", NomObjetFormel)   
    
    source.close()
    #print (Liste_elements)
    return Variete_trouvee    

def DecodeSVG(self, Ligne): 
    global dynamique   
    Element = ''
    if Ligne == "N_Qte_Base" or  Ligne == "Qte_Base":
      Element = 'N_Qte_Base'
    if Ligne == "N_Qte_BaseSingleton" or  Ligne == "Qte_BaseSingleton":
      Element = 'N_Qte_BaseSingleton'
    elif Ligne == "N_EntiteQte_Base" or Ligne == "EntiteQte_Base"  :
      Element ='N_EntiteQte_Base'
    elif Ligne == "N_Effort" or Ligne == "Effort"  :
       Element ='N_Effort'
    elif Ligne == "N_EffortSingulier" or Ligne == "EffortSingulier" :
       Element ='N_EffortSingulier'
    elif Ligne == "N_Zero_Effort" or Ligne == "Zero_Effort"  :
       Element ='N_Zero_Effort'
    elif Ligne == "N_Impulsion" or Ligne == "Impulsion"  :
       Element ='N_Impulsion'
    elif Ligne == "N_ImpulsionSingleton" or Ligne == "ImpulsionSingleton"  :
       Element ='N_ImpulsionSingleton'
    elif Ligne == "N_EntiteImpulsion" or Ligne == "EntiteImpulsion"  :
       Element ='N_EntiteImpulsion'
    elif Ligne == "N_Flot" or Ligne == "Flot":
       Element ='N_Flot'
    elif Ligne == "N_FlotSingulier" or Ligne == "FlotSingulier":
       Element ='N_FlotSingulier'
    elif Ligne == "N_Zero_Flot" or Ligne == "Zero_Flot"  :
       Element ='N_Zero_Flot'
    elif Ligne == "N_Compteur_C" or Ligne == "Qte_Base_count"  :
       Element ='N_Compteur_C'
    elif Ligne == "N_Compteur_L" or  Ligne == "Impulsion_count" :
       Element ='N_Compteur_L'
    elif Ligne == "Capacitance" or Ligne == "O_Capacitance" :
       Element ='O_Capacitance'
    elif Ligne == "Elastance" or Ligne == "O_Elastance" :
       Element ='O_Elastance'
    elif Ligne == "Inductance" or Ligne == "O_Inductance" :
       Element ='O_Inductance'
    elif Ligne == "InductanceSinguliere" or Ligne == "O_InductanceSinguliere" :
       Element ='O_InductanceSinguliere'
    elif Ligne == "Reluctance" or Ligne == "O_Reluctance" :
       Element ='O_Reluctance'
    elif Ligne == "ReluctanceSinguliere" or Ligne == "O_ReluctanceSinguliere" :
       Element ='O_ReluctanceSinguliere'
    elif Ligne == "Conductance" or Ligne == "O_Conductance" :
       Element ='O_Conductance'
    elif Ligne == "Resistance" or Ligne == "O_Resistance" :
       Element ='O_Resistance'
    elif Ligne == "UR_Evol"  or Ligne == "O_UR_Evol" :
        Element ='O_Evolution_C'
        dynamique = True
        self.rdoDynamique.setChecked(True)
    elif Ligne == "DR_Evol" or Ligne == "O_DR_Evol":
        Element ='O_Involution_C'
        dynamique = True
        self.rdoDynamique.setChecked(True)
    elif Ligne == "DL_Evol" or Ligne == "O_DL_Evol":
        Element ='O_Evolution_L'
        dynamique = True
        self.rdoDynamique.setChecked(True)
    elif Ligne == "UL_Evol" or Ligne == "O_UL_Evol" :
        Element ='O_Involution_L'
        dynamique = True
        self.rdoDynamique.setChecked(True)
    elif Ligne == "UR_Evol_Stat"  or Ligne == "O_UR_Evol_Stat" :
        Element ='O_Evolution_C'
        dynamique = False
        self.rdoStatique.setChecked(True)
    elif Ligne == "DR_Evol_Stat" or Ligne == "O_DR_Evol_Stat":
        Element ='O_Involution_C'
        dynamique = False
        self.rdoStatique.setChecked(True)
    elif Ligne == "DL_Evol_Stat" or Ligne == "O_DL_Evol_Stat":
        Element ='O_Evolution_L'
        dynamique = False
        self.rdoStatique.setChecked(True)
    elif Ligne == "UL_Evol_Stat" or Ligne == "O_UL_Evol_Stat" :
        Element ='O_Involution_L'
        dynamique = False
        self.rdoDynamique.setChecked = False
    elif Ligne == "Multiplication_C" or Ligne == "O_Multiplication_C" :
       Element ='O_Multiplication_C'
    elif Ligne == "Multiplication_L" or Ligne == "O_Multiplication_L" :
       Element ='O_Multiplication_L'
    elif Ligne == "Multiplication_fs" or Ligne == "O_Multiplication_fs" :
       Element ='O_Multiplication_C'
    elif Ligne == "Multiplication_pe" or Ligne == "O_Multiplication_pe" :
       Element ='O_Multiplication_L'
    elif Ligne == "Multiplication_qe" or Ligne == "O_Multiplication_qe" :
       Element ='O_Multiplication_C'
    elif Ligne == "Multiplication_es" or Ligne == "O_Multiplication_es" :
       Element ='O_Multiplication_L'
    elif Ligne == "Barrage_D" or Ligne == "O_Barrage_D":
       Element ='O_Croix_D'
    elif Ligne == "Barrage_G" or Ligne == "O_Barrage_G" :
       Element ='O_Croix_G'
    elif Ligne == "Barrage_C" or Ligne == "O_Barrage_C" :
       Element ='O_Croix_C'
    elif Ligne == "Barrage_L" or Ligne == "O_Barrage_L" :
       Element ='O_Croix_L'
    elif Ligne == "Sommation_Effort" or Ligne == "C_Sommation_Effort" :
       Element ='C_Somme_E'
    elif Ligne == "Sommation_Flot" or Ligne == "C_Sommation_Flot":
       Element ='C_Somme_F'
    elif Ligne == "Rectangle" or Ligne == "C_Rectangle" :
       Element ='C_Rectangle'
    return Element


def Description_SVG(self, Liste_elements):
    # Fabrication avec les seules définitions nécessaires
    global Variete 
    global Vecteur 
    global ObjetFormel
    global Dimension_bitmap
    nb = len(self.svg_def) - 1
    Code_SVG = self.svg_def [nb] # fin des définitions
    for paire in self.liste_varietes : #  paire (nom variété, cle)
        if paire[1] == Variete[n]:
            break
    Variete_full = paire[0].lower()
    Contenu = ''
    if ObjetFormel[n] == 1 :
      if NomObjetFormel == 'Singleton_L' :
        Contenu = "<title>Graphe Formel d'un singleton inductif en variété " + Variete_full + "</title>\n"
      if NomObjetFormel == 'Singleton_C' :
        Contenu = "<title>Graphe Formel d'un singleton capacitif en variété " + Variete_full + "</title>\n"
    if ObjetFormel[n] == 2 :
      Contenu = "<title>Graphe Formel d'un pôle en variété " + Variete_full + "</title>\n" # TODO utf-8
    if ObjetFormel[n] == 3 :
      Contenu = "<title>Graphe Formel d'un dipôle en variété " + Variete_full + "</title>\n"
    if ObjetFormel[n] == 4 :
      Contenu = "<title>Graphe Formel d'un assemblage en variété " + Variete_full + "</title>\n"
    Contenu += "<desc>Variete = " + Variete[n] + ", Objet = "+ NomObjetFormel+ ", Produit par : Dessine_Graphe version " + version + "</desc>\n"
    Code_SVG += Contenu
    Code_SVG += self.svg_elem['Geometry']
    Code_SVG += self.svg_elem['Cadre']

    for item in Liste_elements :
        Prefix_elem = item [:4]
        element = item [4:]
        code_element = ImplanteSVG(self, element)
        Code_SVG += code_element 

    Code_SVG += self.svg_elem['Cloture']

    i = 0  # Recherche du premier item commençant par "<g"
    for item in self.svg_def :
        if i == 0 and item[:6] == "<g id=" :
            i = self.svg_def.index(item)

    Pre_code_SVG = ''
    for item in self.svg_def [i:] : # Analyse des défintions d'éléments
        P1 = item.find('id=')
        if P1 > 0 :
            
            chaine =  item[P1+4:]
            P2 =chaine.find('"')
            id  = '#' + chaine[: P2] + '\"'
            if id in Code_SVG :  
                #print(id )
                Pre_code_SVG += item
    
    Code_SVG = Pre_code_SVG +  Code_SVG 
    
    P1 = Dimension_bitmap.find('x')
    largeur = Dimension_bitmap[0:P1] # non utilisée
    hauteur = Dimension_bitmap[P1+1:]
    Pre_code_SVG = self.svg_def [0]
    P1 = Pre_code_SVG.find('width=') + 7
    Pre_code_SVG =Pre_code_SVG[:P1]  + hauteur + Pre_code_SVG[P1:]
    P1 = Pre_code_SVG.find('height=') + 8
    Pre_code_SVG =Pre_code_SVG[:P1]  + hauteur + Pre_code_SVG[P1:]
    for item in self.svg_def [1:i] : # Analyse des défintions avant les éléments
        P1 = item.find('id=')
        if P1 > 0 :
            chaine =  item[P1+4:]
            P2 =chaine.find('"')
            id  = '#' + chaine[: P2] + ')'
            if id in Code_SVG :  
                Pre_code_SVG += item
        else :    
            Pre_code_SVG += item
    Code_SVG = Pre_code_SVG +  Code_SVG 
    return Code_SVG

def Compose_attributsSVG(Symbole, Taille, Police, Style_Graisse):
    global var
    global taille_symbole    
    Style = ""
    Graisse = ""
    if Style_Graisse == 'bolditalic' :
        Style = "italic"
        Graisse = "bold"
    elif Style_Graisse == 'bold' :
        Graisse = "bold"
    elif Style_Graisse == 'italic' :
        Style = "italic"
    P = Symbole.find('0x')
    if P >= 0 : 
        Symbole = "&#x" + Symbole[P+2:P+6] + ";"  + Symbole[P+6:] 
    taille_effective = str(int(0.8 * float(Taille) * (float(taille_symbole)/13.)))
    Morceau = " font-size=\"" + taille_effective + "\" font-family=\"" + Police + "\" font-style=\"" + Style
    Morceau +=  "\" font-weight=\"" + Graisse + "\">" + Symbole
    return Morceau

def Implante_TexteSVG(self, Noeud, Symbole, Taille, Police, Style):
  Contenu = self.svg_elem[Noeud]
  P = Contenu.find("<tspan") # Pour placer le symbole
  Morceau = Compose_attributsSVG(var[n][Symbole], var[n][Taille], var[n][Police], var[n][Style])
  Contenu = Contenu[0: P - 1] + Morceau + Contenu[P:]
  indice = ind[n][Symbole]
  if indice != '~' : 
      P = Contenu.find("</tspan") # Pour placer l'indice
      Morceau = Compose_attributsSVG(indice, ind[n][Taille], ind[n][Police], ind[n][Style])
      Contenu = Contenu[0: P - 1] + Morceau + Contenu[P:]
  return Contenu

def Label_propriete_SVG(Element, Etiquette_1,Police1, Etiquette_2, Police2, Repetition, Contenu):
  #Insertion du code représentant l'opérateur de la propriété
  Symbole = Etiquette_1
  P = Symbole.find('0x')
  if P >= 0 : Symbole = "&#x" + Symbole[P+2:P+6] + ";"  + Symbole[P+6:] 
  P = Symbole.find('-1')
  if P >= 0 :
    Morceau = " font-family=\"" + Police1 + "\">" + Symbole[0: P]
    P = Contenu.find("<tspan") # 1° <tspan>
    if P > 0 : Contenu = Contenu[0:P - 1] + Morceau + Contenu[P:]
    P = Contenu.find("</tspan></text>") # 
    if P > 0 : Contenu = Contenu[0:P] + "-1" + Contenu[P:]
  else :
    Morceau = " font-family=\"" + Police1 + "\">" + Symbole
    P = Contenu.find("<tspan") # 2° <tspan>
    if P > 0 : Contenu = Contenu[0: P - 1] + Morceau + Contenu[P:]
  if Symbole.find("J") == 0 : # Cas de lettres décentrées
    P = Contenu.find(">^<")
    if P > 0 : 
        Contenu = Contenu[0: P ] + ' ' + Contenu[P:]
  # Indice
  Piece = formatte_indice(Etiquette_2, Repetition)
  if len(Piece) > 0 :
    if Piece == '~' : Piece =''
    Morceau = " font-size=\"10\" font-family=\"" + Police2 + "\" font-style=\"italic\">" + Piece
    if "Singuliere"  in Element : Morceau += 's'
    P = Contenu.find(" </text")
    if P > 1 : Contenu = Contenu[:P - 1] + Morceau + Contenu[P + 1:]
  return Contenu

def ImplanteSVG(self, Element): 
    global ObjetFormel
    global Vecteur
    global Indice
    if ObjetFormel[n] <= 2 : # Singleton ou Pôle
      Repetition = 0
    else :
      Repetition = 1 # Pour doubler l'indice des propriétés
    Ligne = ''
    if Element == 'N_Qte_Base':
        if Vecteur == 1 :
            if NomObjetFormel == 'Singleton_C' :
                Ligne = Implante_TexteSVG(self,'N_Qte_BaseSingleton_bold','Symb_q', 'Taille_q', 'Police_q', 'Style_q')
            else :
                Ligne = Implante_TexteSVG(self,'N_Qte_Base_bold','Symb_q', 'Taille_q', 'Police_q', 'Style_q')
        else :
            if NomObjetFormel == 'Singleton_C' :
                Ligne = Implante_TexteSVG(self,'N_Qte_BaseSingleton','Symb_q', 'Taille_q', 'Police_q', 'Style_q')
            else :    
                Ligne = Implante_TexteSVG(self,'N_Qte_Base','Symb_q', 'Taille_q', 'Police_q', 'Style_q')   
    if Element == 'N_EntiteQte_Base':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_EntiteQte_Base_bold','Symb_S_q', 'Taille_q', 'Police_S_q', 'Style_q')
      else :
        Ligne = Implante_TexteSVG(self,'N_EntiteQte_Base','Symb_S_q', 'Taille_q', 'Police_S_q', 'Style_q')   
    elif Element == 'N_Effort':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_Effort_bold','Symb_e', 'Taille_e', 'Police_e', 'Style_e')
      else :
         Ligne = Implante_TexteSVG(self,'N_Effort','Symb_e', 'Taille_e', 'Police_e', 'Style_e')     
    elif Element == 'N_EffortSingulier':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_EffortSingulier_bold','Symb_S_e', 'Taille_e', 'Police_S_e', 'Style_e')
      else :
         Ligne = Implante_TexteSVG(self,'N_EffortSingulier','Symb_S_e', 'Taille_e', 'Police_S_e', 'Style_e')     
    elif Element == 'N_Zero_Effort':
      if Vecteur == 1 :
        Ligne = self.svg_elem['N_Zero_Effort_bold']
      else :
         Ligne = self.svg_elem['N_Zero_Effort']
    elif Element == 'N_Impulsion':
        if Vecteur == 1 :
            if NomObjetFormel == 'Singleton_L' :
                Ligne = Implante_TexteSVG(self,'N_ImpulsionSingleton_bold','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
            else :
                Ligne = Implante_TexteSVG(self,'N_Impulsion_bold','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
        else :
            if NomObjetFormel == 'Singleton_L' :
                Ligne = Implante_TexteSVG(self,'N_ImpulsionSingleton','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
            else :    
                Ligne = Implante_TexteSVG(self,'N_Impulsion','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
    elif Element == 'N_EntiteImpulsion':
      if Vecteur == 1 :
        Ligne =  Implante_TexteSVG(self,'N_EntiteImpulsion_bold','Symb_S_p', 'Taille_p', 'Police_S_p', 'Style_p')
      else :
        Ligne = Implante_TexteSVG(self,'N_EntiteImpulsion','Symb_S_p', 'Taille_p', 'Police_S_p', 'Style_p')
    elif Element == 'N_Flot':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_Flot_bold','Symb_f', 'Taille_f', 'Police_f', 'Style_f')
      else :
        Ligne = Implante_TexteSVG(self,'N_Flot','Symb_f', 'Taille_f', 'Police_f', 'Style_f')
    elif Element == 'N_FlotSingulier':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_FlotSingulier_bold','Symb_S_f', 'Taille_f', 'Police_S_f', 'Style_f')
      else :
        Ligne = Implante_TexteSVG(self,'N_FlotSingulier','Symb_S_f', 'Taille_f', 'Police_S_f', 'Style_f')
    elif Element == 'N_Zero_Flot':
      if Vecteur == 1 :
        Ligne = self.svg_elem['N_Zero_Flot_bold']
      else :
        Ligne = self.svg_elem['N_Zero_Flot']
    elif Element == 'N_Compteur_C':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_Compteur_C_bold','Symb_q', 'Taille_q', 'Police_q', 'Style_q')
      else :
        Ligne = Implante_TexteSVG(self,'N_Compteur_C','Symb_q', 'Taille_q', 'Police_q', 'Style_q')              
    elif Element == 'N_Compteur_L':
      if Vecteur == 1 :
        Ligne = Implante_TexteSVG(self,'N_Compteur_L_bold','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
      else :
        Ligne = Implante_TexteSVG(self,'N_Compteur_L','Symb_p', 'Taille_p', 'Police_p', 'Style_p')
    elif Element == 'O_Capacitance':
      Ligne = Label_propriete_SVG(Element, oper['Capacitance'],oper['Police_C'], ind_op['Capacitance'], ind_op['Police_C'], Repetition, self.svg_elem['0_Capacitance'])
    elif Element == 'O_Elastance':
      Ligne = Label_propriete_SVG(Element, oper['Elastance'],oper['Police_C-1'], ind_op['Elastance'], ind_op['Police_C-1'], Repetition, self.svg_elem['0_Elastance'])
    elif Element == 'O_Inductance':
        if ObjetFormel[n] == 1 :
            Ligne = Label_propriete_SVG(Element, oper['Inductance_S'],oper['Police_LS'], ind_op['Inductance_S'], ind_op['Police_LS'], Repetition, self.svg_elem['0_Inductance'])
        else :  
            Ligne = Label_propriete_SVG(Element, oper['Inductance'],oper['Police_L'], ind_op['Inductance'], ind_op['Police_L'], Repetition, self.svg_elem['0_Inductance'])
    elif Element == 'O_Inductance_Singuliere':
        Ligne = Label_propriete_SVG(Element, oper['Inductance_S'],oper['Police_LS'], ind_op['Inductance_S'], ind_op['Police_LS'], Repetition, self.svg_elem['0_Inductance_Singuliere'])
    elif Element == 'O_Reluctance':
        if ObjetFormel[n] == 1 :
            Ligne = Label_propriete_SVG(Element, oper['Reluctance_S'],oper['Police_L-1S'], ind_op['Reluctance_S'], ind_op['Police_L-1S'], Repetition, self.svg_elem['0_Reluctance'])
        else :
            Ligne = Label_propriete_SVG(Element, oper['Reluctance'],oper['Police_L-1'], ind_op['Reluctance'], ind_op['Police_L-1'], Repetition, self.svg_elem['0_Reluctance'])
    elif Element == 'O_Reluctance_Singuliere':
        Ligne = Label_propriete_SVG(Element, oper['Reluctance_S'],oper['Police_L-1S'], ind_op['Reluctance_S'], ind_op['Police_L-1S'], Repetition, self.svg_elem['0_Reluctance_Singuliere'])
    elif Element == 'O_Conductance' :
      Ligne = Label_propriete_SVG(Element, oper['Conductance'],oper['Police_G'], ind_op['Conductance'], ind_op['Police_G'], Repetition, self.svg_elem['0_Conductance'])
    elif Element == 'O_Resistance':
      Ligne = Label_propriete_SVG(Element, oper['Resistance'],oper['Police_R'], ind_op['Resistance'], ind_op['Police_R'], Repetition, self.svg_elem['0_Resistance'])
    elif Element == 'O_Evolution_C':
        if dynamique :
            Ligne = self.svg_elem['O_Evolution_C']
        else :
            Ligne = self.svg_elem['O_Evolution_C_Stat']
    elif Element == 'O_Involution_C':
        if dynamique :
            Ligne = self.svg_elem['O_Involution_C']
        else :
            Ligne = self.svg_elem['O_Involution_C_Stat']
    elif Element == 'O_Evolution_L':
        if dynamique :
            Ligne = self.svg_elem['O_Evolution_L']
        else :
            Ligne = self.svg_elem['O_Evolution_L_Stat']
    elif Element == 'O_Involution_L':
        if dynamique :
            Ligne = self.svg_elem['O_Involution_L']
        else :
            Ligne = self.svg_elem['O_Involution_L_Stat']
    elif Element == 'O_Multiplication_C' and NomObjetFormel == 'Singleton_C' :
        Ligne = self.svg_elem['O_Multiplication_qe']
    elif Element == 'O_Multiplication_C' and NomObjetFormel == 'Singleton_L' :
        Ligne = self.svg_elem['O_Multiplication_fs']
    elif Element == 'O_Multiplication_L'and NomObjetFormel == 'Singleton_C' :
        Ligne = self.svg_elem['O_Multiplication_es']
    elif Element == 'O_Multiplication_L'and NomObjetFormel == 'Singleton_L' :
        Ligne = self.svg_elem['O_Multiplication_pe']
    elif Element == 'O_Croix_D':
        Ligne = self.svg_elem['O_Croix_D']  #          Adapter D              !!!!!!!!!!
    elif Element == 'O_Croix_G':
        Ligne = self.svg_elem['O_Croix_G']  #          Adapter G              !!!!!!!!!!
    elif Element == 'O_Croix_C':
        Ligne = self.svg_elem['O_Croix_C']
    elif Element == 'O_Croix_L':
        Ligne = self.svg_elem['O_Croix_L']   #          Adapter L              !!!!!!!!!!
    elif Element == 'C_Somme_E':
        Ligne = self.svg_elem['C_Somme_E']
    elif Element == 'C_Somme_F':
        Ligne = self.svg_elem['C_Somme_F']
    return Ligne

#---------------------------------Fin de la traduction du graphe en SVG---------------------------------------------                 

def Trouve_attributs_XML(element): # element appartient à xml.etree.ElementTree 
    attributs = {} # dictionnaire
    if element.find('text') != None :
        texte = element.find('text').text
        p = texte.find('\\')
        if p > 0 :  # Elimination d'anti-slash ("replace" est inopérant)
            texte = texte[:p] + texte[p+1:]
        attributs['texte'] = texte
    if element.find('font') != None : attributs['police'] = element.find('font').text
    if element.find('size') != None : attributs['taille'] = element.find('size').text
    if element.find('style') != None : 
        if element.find('style').text == 'normal' :
            attributs['style'] = ' '
        else :
            attributs['style'] = element.find('style').text
    if element.find('color') != None : attributs['couleur'] = element.find('color').text
    return attributs

def   Analyse_arbreXML(article):
    nom = 'inconnu'
    if len(article) :
        nom = article.attrib['type']
        #print("composant =", nom)
        for element in article :  
            nom_elem = element.tag
            #print("  tag =", nom_elem)
            if len(element.text.strip()) > 0 : 
                nature = element.text.strip()
                #print("    texte =", nature)
            dico = Trouve_attributs_XML(element)
        if nom == "evolution" :
            if nature == 'capacitive' : nom += '_c'
            if nature == 'inductive' : nom += '_l'
            #print("---------------------------------------")
    return nom
        
def Lecture_XML(self,  nomFichier,  texte):  
    # Analyse dircte de texte si nomFichier vide
    global Langue
    global Variete
    global NomObjetFormel
    global ObjetFormel
    global Liste_elements
    global Suffix_prop # Dictionnaire
    global police_legendes
    global taille_legendes
    global style_legendes
    global coul_elem
    Prefix_elem = '0000'  # A modifier !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if len(nomFichier) > 0 :
        Fichier = open(nomFichier, "r")
        # Vérification de la conformité du fichier. Si la première balise <xml> manque, elle est insérée.
        texte = Fichier.readline()
        if texte.find("<?xml ") < 0 :
            QMessageBox.warning(self, "Problème",  "Fichier non identifié comme xml !")
            exit()
        ligne = Fichier.readline()
        if ligne.find("<xml>") < 0 : texte +=  "<xml>\n" #print ("Fichier non strictement structuré xml")
        texte +=  ligne     
        while  ligne != '': # Poursuite du remplissage de la chaîne 'texte'
            ligne = Fichier.readline()
            texte +=  ligne 
        Fichier.close()
    # Analyse du contenu
    NouvelleVariete = "inconnue" # Défaut
    ObjetFormel[n] = 0
    Liste_elements =[]
    try :
        root = ET.fromstring(texte)
    except :    
        QMessageBox.warning(self, "Problème",  "Structure incorrecte du fichier \"", nomFichier, "\"")
        exit()
    #print(root.tag)  # La  balise <xml> 
    #print("---------------------------------------")
    for child in root:
        if len(child.attrib) == 0 : # Elements monolignes
            #print(child.tag, "=",  child.text)
            if child.tag == 'language' : 
                if child.text.capitalize() == 'Français' or child.text.capitalize() == 'French' :  Langue = 'F'  
                if child.text.capitalize()  == 'English' :  Langue = 'E'  
            if child.tag == 'variety' : NouvelleVariete = child.text.capitalize()
            if child.tag == 'object' : 
                NomObjetFormel = child.text .capitalize()   
                if NomObjetFormel == "Singleton_c" : NomObjetFormel = "Singleton_C"
                if NomObjetFormel == "Singleton_l" : NomObjetFormel = "Singleton_L"
                if 'singleton' in child.text  : ObjetFormel[n] = 1
                if child.text == 'pole' : ObjetFormel[n] = 2
                if child.text == 'dipole' : ObjetFormel[n] = 3
                if child.text == 'assemblage' : ObjetFormel[n] = 4
            if child.tag == 'frame' : 
                Liste_elements.append(Prefix_elem + DecodeXML(child.text)) # rectangle
            
    #print("---------------------------------------")
    if NouvelleVariete != Variete[n] :
        construction_dicos_var(self, NouvelleVariete) # Procédure
        construction_dicos_prop(self, NouvelleVariete) # Procédure

    for article in root.iter('frame'):
        nom = 'inconnu'
        if len(article) :
            nom = article.attrib['type']
            #print("composant =", nom)
            for element in article :  
                nom_elem = element.tag
                #print("  tag =", nom_elem)
                if len(element.text.strip()) > 0 : 
                    nature = element.text.strip()
                    #print("    texte =", nature)
                dico = Trouve_attributs_XML(element)
            Liste_elements.append(Prefix_elem + DecodeXML(nom))

    for article in root.iter('synapse'):
        nom = 'inconnu'
        if len(article) :
            nom = article.attrib['type']
            #print("composant =", nom)
            for element in article :  
                nom_elem = element.tag
                #print("  tag =", nom_elem)
                if len(element.text.strip()) > 0 : 
                    nature = element.text.strip()
                    #print("    texte =", nature)
                dico = Trouve_attributs_XML(element)
            #print("---------------------------------------")
            Liste_elements.append(Prefix_elem + DecodeXML(nom))

    for article in root.iter('property'):
        nom = 'inconnu'
        if len(article) :
            nom = article.attrib['type'].capitalize()
            #print("composant =", nom)
            post_suffix = ''
            if 'singuliere' in nom :
                morceau = nom.split(' ')
                nom = morceau[0] 
                suffix = Suffix_prop[nom] + 'S'
                post_suffix = '_S'
            for element in article :  
                nom_elem = element.tag
                #print("  tag =", nom_elem)
                if len(element.text.strip()) > 0 : 
                    nature = element.text.strip()
                    #print("    texte =", nature)
                    if 'qte_base' in nature : nom = nom + '_C'
                    if 'effort' in nature : nom = nom + '_L'
                    if 'flot' in nature : nom = nom + '_C'
                    if 'impulsion' in nature : nom = nom + '_L'
                suffix = Suffix_prop[nom]
                
                dico = Trouve_attributs_XML(element)
                if nom_elem == 'operator' : 
                    if "texte" in dico : oper[nom+post_suffix] = dico['texte']
                    if "police" in dico :  oper['Police_' + suffix] = dico['police']
                if nom_elem == 'subscript' : 
                    if "texte" in dico :  ind_op[nom+post_suffix] = dico['texte']
                    if "police" in dico :  ind_op['Police_' + suffix] = dico['police']
            if nom == "Evolution" or nom == "Involution"  :
                if nature == 'capacitive' : nom += '_c'
                if nature == 'inductive' : nom += '_l'
            #print("---------------------------------------")
            Liste_elements.append(Prefix_elem + DecodeXML(nom+post_suffix))

    for article in root.iter('node'):
        nom = 'inconnu'
        if len(article) :
            suffix = ''
            nom = article.attrib['type']
            #print("composant =", nom)
            if nom == 'qtebase' or  nom == 'entite_qtebase' or  nom == 'compteur_qtebase': suffix = 'q'
            if nom == 'effort' or nom == 'effort_singulier' or  nom == 'zero_effort' :  suffix = 'e'
            if nom == 'impulsion' or  nom == 'entite_impulsion' or  nom == 'compteur_impulsion': suffix = 'p' 
            if nom == 'flot' or nom == 'flot_singulier'  or  nom == 'zero_flot': suffix = 'f'
            #print("suffix =", suffix)
            if len(suffix) == 0 : 
                QMessageBox.warning(self, "Erreur de lecture du fichier XML", "Nom de noeud = \"" + nom + "\" non reconnu")
                return ''
            for element in article :  
                nom_elem = element.tag
                #print("nom", nom, "  tag =", nom_elem, " texte =",  str(element.text))
                if len(element.text.strip()) > 0 : 
                    nature = element.text.strip()
                    #print("    texte =", nature)
                dico = Trouve_attributs_XML(element)
                if nom_elem == 'legend' : 
                    if Langue == 'F': var[n]['Nom_' + suffix] = dico['texte']
                    if Langue == 'E': ind[n]['Nom_' + suffix] = dico['texte']
                    if "police" in dico :  police_legendes = dico['police']
                    if "taille" in dico : taille_legendes = float(dico['taille'])
                    if "style" in dico :style_legendes = dico['style']
                if nom_elem == 'symbol' : 
                    #print("    symbole =", dico['texte'])
                    var[n]['Symb_' + suffix] = dico['texte']
                    if "police" in dico : var[n]['Police_' + suffix]= dico['police']
                    if "taille" in dico : var[n]['Taille_' + suffix] = dico['taille']
                    if "style" in dico : var[n]['Style_' + suffix] = dico['style']
                if nom_elem == 'subscript' : 
                    ind[n]['Symb_' + suffix] = dico['texte']
                    if "police" in dico : ind[n]['Police_' + suffix]= dico['police']
                    if "taille" in dico : ind[n]['Taille_' + suffix] = dico['taille']
                    if "style" in dico : ind[n]['Style_' + suffix] = dico['style']
                if nom_elem == 'unit' : 
                    morceau = dico['texte']
                    if len(morceau) >0 : 
                        P = morceau.rfind('=')
                        ind[n]['Unite_' + suffix] = '~'
                        if P >= 0 : ind[n]['Unite_' + suffix] = morceau[:P]
                        var[n]['Unite_' + suffix] = morceau[P+1:]
                        #print("P =", P, "var:", var[n]['Unite_' + suffix], "ind:", ind[n]['Unite_' + suffix])    
                    if "couleur" in dico :  
                        couleur = '#' + dico['couleur'].strip('\"')
                        coul = QColor(couleur)
                        if coul.name() != '#000000' or couleur == '#000000'and coul.name() == '#000000' :
                            if suffix == 'q' or suffix == 'e' or suffix == 'p' or suffix == 'f' :
                               coul_elem['unite_'+suffix] = couleur
            #print("---------------------------------------")
            Liste_elements.append(Prefix_elem + DecodeXML(nom))
    #print("---------------------------------------")
    #print ('Lecture_XML =', Liste_elements) # DEBUG
    return NouvelleVariete    
    
def Indentation_XML(chaine):
    ligne =''
    texte = ''
    pas = 4
    indent = 0 
    p2 = 0
    while p2 < len(chaine) -1:
        p1 = chaine.index('<',  p2)
        p2 = chaine.index('>',  p1) +1
        balise = chaine[p1:p2]
        if chaine[p1+1] != '/' : # balise de début   <balise> ou <balise x = "... "> ou autonome <balise x = "... " />
            if chaine[p2-2] == '/' : # balise autonome <balise x = "... " />
                ligne  = ' '*indent + balise     # Affectation balise autonome <balise x = "... " />
                texte += ligne + '\n'
                ligne =''
            else :  # balise de début <balise> ou <balise x = "... ">
                if len(ligne) > 0 :
                    texte += ligne + '\n'  # Ligne avec la balise de début précédente
                    ligne =''
                    indent += pas
                p3 = p1 + balise.find(' ') 
                if p3 > p1 :  # Balise avec attribut interne <balise x = "... ">
                    balise_fin = "</" + chaine[p1+1:p3] + '>'
                else : # Balise simple <balise> 
                    balise_fin = "</" + chaine[p1+1:p2]
                p4 = p2 + chaine[p2:].find('<')  # Début de la balise suivante
                ligne  = ' '*indent + chaine[p1:p4] # Affectation balise de début
        else :  #  chaine[p1+1] == '/' : balise de fin
            if balise == balise_fin :
                ligne  += balise # Affectation ligne autonome
            else : 
                indent -= pas   
                ligne  = ' '*indent + balise # Affectation ligne = balise de fin seule
            texte += ligne + '\n'
            ligne =''
    return texte
    
def Sequence_attributs(elem_XML, texte, decrire_details,  police,  taille,  style,  couleur):    
    if len(texte) > 0 :
        sub_elem_XML = ET.SubElement(elem_XML, 'text')
        p = texte.find("0x")
        if p >= 0 : texte = "&#" + texte[p+1:] + ";"
        sub_elem_XML.text = texte
    if decrire_details :
        if len(police) > 0 :
            sub_elem_XML = ET.SubElement(elem_XML, 'font')
            sub_elem_XML.text = police
        if len(taille) > 0 :
            sub_elem_XML = ET.SubElement(elem_XML, 'size')
            sub_elem_XML.text = taille
        if len(style) > 0 :
            sub_elem_XML = ET.SubElement(elem_XML, 'style')
            sub_elem_XML.text = style
        if len(couleur) > 0 :
            sub_elem_XML = ET.SubElement(elem_XML, 'color')
            sub_elem_XML.text = couleur

def Description_XML(self,  Liste_elements, decrire_details):
    # Création d'un texte en XML
    global Langue
    global Variete
    global NomObjetFormel
    Code_XML = '<?xml version="1.0" encoding="UTF-8"?>\n'
    elem_XML = ET.Element('xml')
    sub_elem_XML = ET.SubElement(elem_XML, 'title')
    sub_elem_XML.text = 'Graphe Formel'
    sub_elem_XML = ET.SubElement(elem_XML, 'version')
    sub_elem_XML.text = 'Produit par \"Dessine_Graphe\" version ' + version
    sub_elem_XML = ET.SubElement(elem_XML, 'language')
    if Langue == 'F' :  sub_elem_XML.text = 'Français'
    if Langue == 'E' :  sub_elem_XML.text = 'English'
    sub_elem_XML = ET.SubElement(elem_XML, 'variety')
    sub_elem_XML.text = Variete[n].lower()
    sub_elem_XML = ET.SubElement(elem_XML, 'object')
    sub_elem_XML.text = NomObjetFormel.lower()
    sub_elem_XML = ET.SubElement(elem_XML, 'frame')
    sub_elem_XML.text = 'rectangle'
    for item in Liste_elements :
        Prefix_elem = item [:4]
        Element = item [4:]
        ImplanteXML(self, elem_XML, Element, Prefix_elem, decrire_details)
    chaine = str(ET.tostring(elem_XML)) #,encoding="iso-8859-1" , encoding="UTF-8"
    chaine = chaine[2:len(chaine)-1] # Elagage
    Code_XML += Indentation_XML(chaine)
    Code_XML = Code_XML.replace("&amp;","&") # Neutralisation du codage HTML
    #print(chaine) # Chaîne d'un seul paragraphe
    #print('-----------------------------')
    #print(Code_XML)  # Texte indenté"""
    return Code_XML
    
def  Code_Noeud_XML(self, elem_XML, type_noeud,  suffix, decrire_details, couleur):
    sub_elem_XML = ET.SubElement(elem_XML, 'node')
    sub_elem_XML.set('type', type_noeud)
    suffix_long = suffix
    if "singulier" in type_noeud or "entite" in type_noeud :  suffix_long = "S_" + suffix 
    if legendes :
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'legend')
        if Langue == 'F' : texte = var[n]['Nom_'+suffix_long]
        if Langue == 'E' :  texte = ind[n]['Nom_'+suffix_long]
        Sequence_attributs(subsub_elem_XML, texte, decrire_details,  police_legendes,  str(taille_legendes),  style_legendes,  '')
    subsub_elem_XML = ET.SubElement(sub_elem_XML, 'symbol') 
    texte = var[n]['Symb_'+suffix_long].strip()
    Sequence_attributs(subsub_elem_XML, texte, decrire_details,  var[n]['Police_'+suffix_long],  var[n]['Taille_'+suffix],  var[n]['Style_'+suffix],  '')
    indice = ind[n]['Symb_'+suffix_long].strip()
    if len(indice) > 0  and indice != '~':
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'subscript') 
        Sequence_attributs(subsub_elem_XML, indice, decrire_details,  ind[n]['Police_'+suffix_long],  ind[n]['Taille_'+suffix],  ind[n]['Style_'+suffix],  '')
    if unites > 0 :
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'unit')
        id_unite = 'Unite_'+suffix_long
        unite = var[n][id_unite] # Unité par défaut
        if ind[n][id_unite] != '~' : 
            if unites == 2 : unite = ind[n][id_unite] # unité SI
            if unites == 3 : unite = ind[n][id_unite] +" = " + unite  # unité SI + SI_J
        couleur = "\"" + couleur[1:].upper() + "\""
        Sequence_attributs(subsub_elem_XML, unite, decrire_details,  'Helvetica',  '4.5',  '',  couleur)
        
def  Code_Propriete_XML(self, elem_XML, type_propriete, nom_propriete, decrire_details, couleur):
    global Suffix_prop
    sub_elem_XML = ET.SubElement(elem_XML, 'property')
    sub_elem_XML.set('type', type_propriete)
    suffix = Suffix_prop[nom_propriete] 
    if "singuliere"  in type_propriete : 
        suffix = suffix + 'S'
        post_suffix = '_S'
    else :
        post_suffix = ''
    if operateurs :
        couleur = "\"" + couleur[1:].upper() + "\""
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'operator') 
        texte = oper[nom_propriete + post_suffix].strip()
        P = texte.find('-1')
        if P >= 0 : texte = texte[:P]
        style = ''
        if linearite : style = 'italic'        
        Sequence_attributs(subsub_elem_XML, texte, decrire_details,  oper['Police_'+suffix],  '8.',  style,  couleur)  # Si decrire_details_en_XML = True
        if not linearite :
            subsub_elem_XML = ET.SubElement(sub_elem_XML, 'hat') 
            Sequence_attributs(subsub_elem_XML, '^', decrire_details,  'Arial',  '8.',  '', couleur)
        texte_indice = ind_op[nom_propriete + post_suffix].strip()
        if "singuliere"  in type_propriete : texte_indice += 's'
        if len(texte_indice) > 0 :
            if ObjetFormel[n] >= 3 : # Dipôle ou Assemblage
                texte_indice = formatte_indice(texte_indice, 1) # Pour doubler l'indice des propriétés
            else :
                texte_indice = formatte_indice(texte_indice, 0) # Non doublement de l'indice des propriétés
            subsub_elem_XML = ET.SubElement(sub_elem_XML, 'subscript') 
            Sequence_attributs(subsub_elem_XML, texte_indice, decrire_details,  ind_op['Police_'+suffix],  '6.',  'italic', couleur)  # Si decrire_details_en_XML = True
        if P >= 0 : 
            subsub_elem_XML = ET.SubElement(sub_elem_XML, 'superscript')
            Sequence_attributs(subsub_elem_XML, '-1', decrire_details,  'Arial',  '5.',  '', couleur) # Si decrire_details_en_XML = True

def  Code_Evolution_XML(self, elem_XML, type_propriete, nature_propriete, decrire_details, dynamique,  couleur):
    sub_elem_XML = ET.SubElement(elem_XML, 'property')
    sub_elem_XML.set('type', type_propriete)
    subsub_elem_XML = ET.SubElement(sub_elem_XML, 'nature') 
    subsub_elem_XML.text = nature_propriete 
    if operateurs :
        couleur = "\"" + couleur[1:].upper() + "\""
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'operator') 
        if type_propriete == "evolution" :
            if dynamique :
                texte = "d/dt"
                taille = '6.'
            else :
                texte = "T-1"
                taille = '8.'
            Sequence_attributs(subsub_elem_XML, texte, decrire_details,  'Arial',  taille, '',  couleur) # Si decrire_details_en_XML = True
        elif type_propriete == "involution" :
            if dynamique :
                texte = "d-1/dt-1"
                taille = '6.'
            else :
                texte = "T"
                taille = '8.'
            Sequence_attributs(subsub_elem_XML, texte, decrire_details,  'Arial',  taille, '',  couleur) # Si decrire_details_en_XML = True
        elif type_propriete == "multiplication" :
            texte = "N"
            taille = '8.'
            Sequence_attributs(subsub_elem_XML, texte, decrire_details,  'Times New Roman',  taille, '',  couleur) # Si decrire_details_en_XML = True

def  Code_Element_XML(self, elem_XML, type_propriete, nom_propriete, decrire_details,  couleur):
    sub_elem_XML = ET.SubElement(elem_XML, type_propriete)
    sub_elem_XML.set('type', nom_propriete)
    if decrire_details :
        couleur = "\"" + couleur[1:].upper() + "\""
        subsub_elem_XML = ET.SubElement(sub_elem_XML, 'color') 
        subsub_elem_XML.text = couleur
        
def ImplanteXML(self, elem_XML, Element, Prefix_elem, decrire_details):    
    #Prefix_elem non utilisé provisoirement
    global Langue
    global legendes
    global police_legendes
    global taille_legendes
    global style_legendes
    if Element == 'N_Qte_Base':
        Code_Noeud_XML(self, elem_XML, "qtebase",  'q', decrire_details, coul_elem['qte_base'])      
    if Element == 'N_EntiteQte_Base':
        Code_Noeud_XML(self, elem_XML, "entite_qtebase",  'q', decrire_details, coul_elem['qte_base'])      
    elif Element == 'N_Compteur_C':
        Code_Noeud_XML(self, elem_XML, "compteur_qtebase",  'q', decrire_details, coul_elem['compteur'])      
    elif Element == 'N_Effort':
        Code_Noeud_XML(self, elem_XML, "effort",  'e', decrire_details, coul_elem['effort'])      
    elif Element == 'N_EffortSingulier':
        Code_Noeud_XML(self, elem_XML, "effort_singulier",  'e', decrire_details, coul_elem['effort'])      
    elif Element == 'N_Zero_Effort':
        Code_Noeud_XML(self, elem_XML, "zero_effort",  'e', decrire_details, coul_elem['effort'])      
    elif Element == 'N_Impulsion':
        Code_Noeud_XML(self, elem_XML, "impulsion",  'p', decrire_details, coul_elem['impulsion'])      
    elif Element == 'N_EntiteImpulsion':
        Code_Noeud_XML(self, elem_XML, "entite_impulsion",  'p', decrire_details, coul_elem['impulsion'])      
    elif Element == 'N_Compteur_L':
        Code_Noeud_XML(self, elem_XML, "compteur_impulsion",  'p', decrire_details, coul_elem['compteur'])      
    elif Element == 'N_Flot':
        Code_Noeud_XML(self, elem_XML, "flot",  'f', decrire_details, coul_elem['flot'])      
    elif Element == 'N_FlotSingulier':
        Code_Noeud_XML(self, elem_XML, "flot_singulier",  'f', decrire_details, coul_elem['flot'])      
    elif Element == 'N_Zero_Flot':
        Code_Noeud_XML(self, elem_XML, "zero_flot",  'f', decrire_details, coul_elem['flot'])      
    elif Element == 'O_Capacitance':
        Code_Propriete_XML(self, elem_XML, "capacitance", 'Capacitance', decrire_details,  coul_elem['capacitance'])      
    elif Element == 'O_Elastance':
        Code_Propriete_XML(self, elem_XML, "elastance", 'Elastance', decrire_details,  coul_elem['capacitance'])      
    elif Element == 'O_Inductance':
        Code_Propriete_XML(self, elem_XML, "inductance", 'Inductance', decrire_details, coul_elem['inductance'])      
    elif Element == 'O_Reluctance':
        Code_Propriete_XML(self, elem_XML, "reluctance", 'Reluctance', decrire_details, coul_elem['inductance'])      
    elif Element == 'O_Conductance' :
        Code_Propriete_XML(self, elem_XML, "conductance", 'Conductance', decrire_details, coul_elem['conductance'])      
    elif Element == 'O_Resistance':
        Code_Propriete_XML(self, elem_XML, "resistance", 'Resistance', decrire_details, coul_elem['conductance'])      
    elif Element == 'O_Evolution_C':
        Code_Evolution_XML(self, elem_XML, "evolution", 'capacitive', decrire_details, dynamique, coul_elem['temps'])      
    elif Element == 'O_Involution_C':
        Code_Evolution_XML(self, elem_XML, "involution", 'capacitive', decrire_details, dynamique, coul_elem['temps'])      
    elif Element == 'O_Evolution_L':
        Code_Evolution_XML(self, elem_XML, "evolution", 'inductive', decrire_details, dynamique, coul_elem['temps'])      
    elif Element == 'O_Involution_L':
        Code_Evolution_XML(self, elem_XML, "involution", 'inductive', decrire_details, dynamique,  coul_elem['temps'])      
    elif Element == 'O_Croix_D':
        Code_Element_XML(self, elem_XML, "frame","barrage_droite", decrire_details,  coul_elem['barriere'])      
    elif Element == 'O_Croix_G':
        Code_Element_XML(self, elem_XML, "frame","barrage_gauche", decrire_details,  coul_elem['barriere'])      
    elif Element == 'O_Croix_C':
        Code_Element_XML(self, elem_XML, "frame","barrage_capacitance", decrire_details,  coul_elem['barriere'])      
    elif Element == 'O_Croix_L':
        Code_Element_XML(self, elem_XML, "frame","barrage_inductance", decrire_details,  coul_elem['barriere'])      
    elif Element == 'C_Somme_E':
        Code_Element_XML(self, elem_XML, "synapse","somme_effort", decrire_details,  coul_elem['synapse'])      
    elif Element == 'C_Somme_F':
        Code_Element_XML(self, elem_XML, "synapse","somme_flot", decrire_details,  coul_elem['synapse']) 
    elif Element == 'O_Multiplication_C':        # Les multiplications par N sont toujours par paire
        if NomObjetFormel == 'Singleton_C' : Code_Evolution_XML(self, elem_XML, "multiplication", 'vers qte_base', decrire_details, 0, coul_elem['qte_base'])      
        if NomObjetFormel == 'Singleton_L' :  Code_Evolution_XML(self, elem_XML, "multiplication", 'vers flot', decrire_details, 0, coul_elem['flot'])      
    elif Element == 'O_Multiplication_L':         # Les multiplications par N sont toujours par paire
        if NomObjetFormel == 'Singleton_C' : Code_Evolution_XML(self, elem_XML, "multiplication", 'vers effort', decrire_details, 0, coul_elem['effort'])      
        if NomObjetFormel == 'Singleton_L' : Code_Evolution_XML(self, elem_XML, "multiplication", 'vers impulsion', decrire_details, 0, coul_elem['impulsion'])      
    elif Element == 'O_Inductance_Singuliere':
        Code_Propriete_XML(self, elem_XML, "inductance singuliere", 'Inductance', decrire_details, coul_elem['inductance'])      
    elif Element == 'O_Reluctance_Singuliere':
        Code_Propriete_XML(self, elem_XML, "reluctance singuliere", 'Reluctance', decrire_details, coul_elem['inductance'])      
    return

def DecodeXML(Ligne):    
    Element = ''
    Ligne = Ligne.strip().lower()
    if Ligne == "qtebase" :
      Element = 'N_Qte_Base'
    elif Ligne == "entite_qtebase" :
      Element ='N_EntiteQte_Base'
    elif Ligne == "effort" :
       Element ='N_Effort'
    elif Ligne == "effort_singulier" :
       Element ='N_EffortSingulier'
    elif Ligne == "zero_effort" :
       Element ='N_Zero_Effort'
    elif Ligne == "impulsion" :
       Element ='N_Impulsion'
    elif Ligne == "entite_impulsion" :
       Element ='N_EntiteImpulsion'
    elif Ligne == "flot" :
       Element ='N_Flot'
    elif Ligne == "flot_singulier" :
       Element ='N_FlotSingulier'
    elif Ligne == "zero_flot" :
       Element ='N_Zero_Flot'
    elif Ligne == "compteur_qtebase" :
       Element ='N_Compteur_C'
    elif Ligne == "compteur_impulsion" :
       Element ='N_Compteur_L'
    elif Ligne == "capacitance" :
       Element ='O_Capacitance'
    elif Ligne == "elastance" :
       Element ='O_Elastance'
    elif Ligne == "inductance" :
       Element ='O_Inductance'
    elif Ligne == "inductance_s" :
       Element ='O_Inductance_Singuliere'
    elif Ligne == "reluctance" :
       Element ='O_Reluctance'
    elif Ligne == "reluctance_s" :
       Element ='O_Reluctance_Singuliere'
    elif Ligne == "conductance" :
       Element ='O_Conductance'
    elif Ligne == "resistance" :
       Element ='O_Resistance'
    elif Ligne == "evolution_c" :
       Element ='O_Evolution_C'
    elif Ligne == "involution_c" :
       Element ='O_Involution_C'
    elif Ligne == "evolution_l" :
       Element ='O_Evolution_L'
    elif Ligne == "involution_l" :
       Element ='O_Involution_L'
    elif Ligne == "multiplication_c" :
       Element ='O_Multiplication_C'
    elif Ligne == "multiplication_l" :
       Element ='O_Multiplication_L'
    elif Ligne == "barrage_droite"  or Ligne == "barrage_f_q" :
       Element ='O_Croix_D'
    elif Ligne == "barrage_gauche" or Ligne == "barrage_e_p" :
       Element ='O_Croix_G'
    elif Ligne == "barrage_capacitance" or Ligne == "barrage_e_q" :
       Element ='O_Croix_C'
    elif Ligne == "barrage_inductance" or Ligne == "barrage_p_f" :
       Element ='O_Croix_L'
    elif Ligne == "somme_effort" :
       Element ='C_Somme_E'
    elif Ligne == "somme_flot" :
       Element ='C_Somme_F'
    elif Ligne == "rectangle" :
       Element ='C_Rectangle'
    return Element
#__________________________________________________________________________________
# LaTex

def Description_LaTex(self,  Liste_elements): # Création d'un texte en LaTex
    global version
    global Variete 
    #global NomObjetFormel
    Variete_LaTex = {}
    Variete_LaTex['Generique'] = 'gene' # Generic form of energy
    Variete_LaTex['Translation'] = 'tran' # Translation mechanics
    Variete_LaTex['Rotation'] = 'rota' # Rotation mechanics
    Variete_LaTex['Surface'] = 'surf' # Surface energy
    Variete_LaTex['Hydrodynamique'] = 'hydr' # Hydrodynamics
    Variete_LaTex['Gravitation'] = 'grav' # Gravitational energy
    Variete_LaTex['Electrodynamique'] = 'elec' # Electrodynamics
    Variete_LaTex['Magnetisme'] = 'magn' # Magnetism
    Variete_LaTex['Polarisation'] = 'pola' # Electric polarization
    Variete_LaTex['Corpusculaire'] = 'corp' # Corpuscular energy
    Variete_LaTex['Physico_chimique'] = 'chim' # Physical chemistry
    Variete_LaTex['Reaction_corpusculaire'] = 'reco' # Corpuscular reaction
    Variete_LaTex['Reaction_chimique'] = 'rech' # Chemical reaction
    Variete_LaTex['Thermique'] = 'ther' # Thermics
    Variete_LaTex['Ondulatoire'] = 'ondu' # Energie ondulatoire
    Variete_LaTex['Oscillatoire'] = 'osci' # Energie oscillatoire    
    for paire in self.liste_varietes : #  paire (nom variété, cle)
        if paire[1] == Variete[n]:
            break
    Variete_full = paire[0].lower()
    Contenu = ''
    if ObjetFormel[n] == 1 :
      if NomObjetFormel == 'Singleton_L' :
        Contenu = "%Graphe Formel d'un singleton inductif en variété " + Variete_full + "\n"
      if NomObjetFormel == 'Singleton_C' :
        Contenu = "%Graphe Formel d'un singleton capacitif en variété " + Variete_full + "\n"
    if ObjetFormel[n] == 2 :
      Contenu = "%Graphe Formel d'un pôle en variété " + Variete_full + "\n" # TODO utf-8
    if ObjetFormel[n] == 3 :
      Contenu = "%Graphe Formel d'un dipôle en variété " + Variete_full + "\n"
    if ObjetFormel[n] == 4 :
      Contenu = "%Graphe Formel d'un assemblage en variété " + Variete_full + "\n"
    Code_LaTex = '%LaTeX\n'
    Code_LaTex += Contenu
    Code_LaTex += '%Fichier produit avec Dessinateur de Graphes Formels version ' + version + '\n'
    Code_LaTex += '%Requiert le paquet \'formalgraph.sty\' disponible sur \"https://framagit.org/caudwell/FormalGraphs-latex\"\n'
    Code_LaTex += '\\documentclass{article}\n\\usepackage{formalgraphs}\n\\begin{document}\n'
    Code_LaTex += '  \\begin{formalgraph}\n'
    for item in Liste_elements: # Noeuds en premier
        Prefix_elem = item [:4]
        Element = item [4:]
        if Element.find('N_') == 0 : 
            Ligne = Implante_LaTex(self, Element, Prefix_elem, Variete_LaTex[Variete[n]])
            Code_LaTex += Ligne
    for item in Liste_elements: # Opérateurs en second
        Prefix_elem = item [:4]
        Element = item [4:]
        if Element.find('O_') == 0 : 
            Ligne = Implante_LaTex(self, Element,  Prefix_elem, Variete_LaTex[Variete[n]])
            if Ligne != "   \\timeX{}\n" or Code_LaTex.find(Ligne) < 0 :
                Code_LaTex += Ligne
    Code_LaTex += '  \\end{formalgraph}\n\\end{document}\n'
    #print(Code_LaTex) # Chaîne d'un seul paragraphe
    #print('-----------------------------')
    return Code_LaTex

def Implante_LaTex(self, Element, Prefix_elem, variete):    
    #Prefix_elem non utilisé provisoirement
    global Langue
    Ligne = ''
    if Element == 'N_Qte_Base':
        if Langue == 'F' : texte = var[n]['Nom_q']
        if Langue == 'E' :  texte = ind[n]['Nom_q']
        Ligne=  "   \\base{$\\" + variete + "Q$}\n   \\baselabel{" + texte + "}\n"
    if Element == 'N_EntiteQte_Base':
        Ligne=  ""
    elif Element == 'N_Effort':
        if Langue == 'F' : texte = var[n]['Nom_e']
        if Langue == 'E' :  texte = ind[n]['Nom_e']
        Ligne=  "   \\effort{$\\" + variete + "E$}\n   \\effortlabel{" + texte + "}\n"
    elif Element == 'N_Zero_Effort':
        Ligne=  ""
    elif Element == 'N_Impulsion':
        if Langue == 'F' : texte = var[n]['Nom_p']
        if Langue == 'E' :  texte = ind[n]['Nom_p']
        Ligne=  "   \\impulse{$\\" + variete + "I$}\n   \\impulselabel{" + texte + "}\n"
    elif Element == 'N_EntiteImpulsion':
        Ligne=  ""
    elif Element == 'N_Flot':
        if Langue == 'F' : texte = var[n]['Nom_f']
        if Langue == 'E' :  texte = ind[n]['Nom_f']
        Ligne=  "   \\flow{$\\" + variete + "F$}\n   \\flowlabel{" + texte + "}\n"
    elif Element == 'N_Zero_Flot':
        Ligne=  ""
    elif Element == 'N_Compteur_C':
        Ligne=  ""
    elif Element == 'N_Compteur_L':
        Ligne=  ""
    elif Element == 'O_Capacitance':
        Ligne=  "   \\capac{" +  oper['Capacitance'] + "}\n"
    elif Element == 'O_Elastance':
        Ligne=  "   \\elast{" + oper['Elastance'] + "}\n"
    elif Element == 'O_Inductance':
        Ligne=  "   \\induc{" + oper['Inductance'] + "}\n"
    elif Element == 'O_Reluctance':
        Ligne=  "   \\reluc{" + oper['Reluctance'] + "}\n"
    elif Element == 'O_Conductance' :
        Ligne=  "   \\conduc{" + oper['Conductance'] + "}\n"
    elif Element == 'O_Resistance':
        Ligne=  "   \\resist{" + oper['Resistance'] + "}\n"
    elif Element == 'O_Evolution_C':
        texte = "T-1"
        Ligne=  "   \\evoR{" + texte + "}\n"
    elif Element == 'O_Involution_C':
        texte = "T"
        Ligne=  "   \\invoR{" + texte + "}\n"
    elif Element == 'O_Evolution_L':
        texte = "T-1"
        Ligne=  "   \\evoL{" + texte + "}\n"
    elif Element == 'O_Involution_L':
        texte = "T"
        Ligne=  "   \\invoL{" + texte + "}\n"
    elif Element == 'O_Croix_D':
        Ligne=  "   \\timeX{}\n"
    elif Element == 'O_Croix_G':
        Ligne=  "   \\timeX{}\n"
    elif Element == 'O_Croix_C':
        Ligne=  "   \\timeX{}\n"
    elif Element == 'O_Croix_L':
        Ligne=  "   \\timeX{}\n"
    elif Element == 'C_Somme_E':
        Ligne = ""
    elif Element == 'C_Somme_F':
        Ligne = ""
    return Ligne


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())
