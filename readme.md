# Dessine_Graphe.py  

E.Vieil 06/01/2018 version 1.6

Logiciel de dessin d'un Graphe Formel global (2D) à partir d'une sélection à la souris de propriétés physiques du système.


## Intallation
Copier les fichiers de la distribution dans un répertoire. Lancer "Dessine_ Graphe.py" dans un interpréteur Python 3.

## Packages Python requis
PyQt5, xml.etree, QtSvg

## Paramétrisation
Les fichiers "GF_Data_varietes2.csv" et "GF_Data_proprietes2.csv" contiennent chacun un tableau des paramètres des graphes pour chaque variété d'énergie. 
Le nombre de ces variétés est déterminé par le nombre de lignes de ces tableaux (deux lignes par variété).

## Licence

Ce code est distribué sous licence [CeCILL](http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (compatible avec la licence GNU GPL).


## Documentation
La notice d'utilisation est placée dans le répertoire "Docs" en format HTML.
Les descriptions des variétés d'énergie sont dans ce répertoire en format HTML.
La carte des variétés d'énergie est acccessible sous différents formats dans ce répertoire.
Des exemples de fichiers de graphe sont donnés dans les sous-répetoires "GF_xxx".

## Evolutions

### Par rapport à la version précédente 1.5

Choix des unités SI ou SI-J (SI avec explicitation du joule, radian et corpuscule).

Fenêtre "A propos"

Commentaire de version dans les enregistrements

Nouvelle taille bitmap 450x300 pixels

Ajustement de la taille des graphes en SVG selon la hauteur bitmap

Bouton d'ajustement de la taille des symboles algébriques dans les noeuds



### Par rapport à la version précédente 1.4

Lecture et mémorisation complète de fichiers XML (y compris statique/dynamique).

Enregistrement/Lecture de description en XML dans les fichiers bitmap.

Représentation des singletons inductif et capacitif avec leurs paires conjuguées.

Inclusion des singletons dans les sorties XML et SVG. 

Centrage des labels (symboles et indices) dans les noeuds et les légendes.

Visionneuse SVG et Bitmap. 
Carte des variétés d'énergie. 

Fenêtre de description des variétés d'énergie.
